import { useState } from 'react'
//external state
export const useModalCarrousel = (initialValue = false) => {
  
  const [isOpen, setIsOpen] = useState(initialValue)
  const openModal = () => setIsOpen(true);
  const closeModal = () => setIsOpen(false);
  
  return [isOpen, openModal, closeModal];
}
