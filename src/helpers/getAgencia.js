export const getAgencia = (Empresa, Sucursal) => {
    if(Empresa === '1' && Sucursal === '1'){
        return `Mochis`
    }
    if(Empresa === '3' && Sucursal === '1'){
        return `Guasave`
    }
    if(Empresa === '5' && Sucursal === '1'){
        return `Culiacan`
    }
    if(Empresa === '7' && Sucursal === '1'){
        return `Cadillac`
    }
}

export const getAgenciaActualizado = (Empresa, Sucursal) => {
    if(Empresa === '1' && Sucursal === '1'){
        return `MOCHIS`
    }
    if(Empresa === '3' && Sucursal === '1'){
        return `GUASAVE`
    }
    if(Empresa === '5' && Sucursal === '1'){
        return `CULIACAN ZAP`
    }
    if(Empresa === '5' && Sucursal === '2'){
        return `CULIACAN AERO`
    }
    if(Empresa === '7' && Sucursal === '1'){
        return `CADILLAC`
    }
}