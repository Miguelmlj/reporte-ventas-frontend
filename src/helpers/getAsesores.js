import axios from "axios";
import { Apiurl } from "../services/Apirest";
import { getEmpresa, getSucursal } from "../helpers/getEmpresaSucursal";
import { activos, todos } from "../constantes/asesores";

export const getAsesores = async (status) => {
    const agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let endpoint = "";
    if (status === activos) endpoint = "api/asesores/getactivos"
    if (status === todos) endpoint = "api/asesores/get"

    let result = []
    const url = Apiurl + endpoint;
    await axios.post(url, agencia)
        .then( response => {
            result = response['data'];
        })
        .catch(err => {
            result = []
        })

    return result;
}

export const getAsesoresPorAgencia = async (Empresa, Sucursal) => {
    const agencia = { Empresa: Empresa, Sucursal: Sucursal }
    let endpoint = "api/asesores/get";

    let result = []
    const url = Apiurl + endpoint;
    await axios.post(url, agencia)
        .then( response => {
            result = response['data'];
        })
        .catch(err => {
            result = []
        })

    return result;
}