export const getEmpresa = () => {
    return window.sessionStorage.getItem("empresa")
} 

export const getSucursal = () => {
    return window.sessionStorage.getItem("sucursal")
} 

export const getNombreAsesor = () => {
    return window.sessionStorage.getItem("nombre")
}