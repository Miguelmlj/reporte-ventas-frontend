export const formatDecimalNumber = (decimalNumber) => {
    if(countDecimals(decimalNumber) === 0) return `${decimalNumber}.00`
    if(countDecimals(decimalNumber) === 1) return `${decimalNumber}0`
    if(countDecimals(decimalNumber) === 2) return decimalNumber
}

const countDecimals = function (value) {
    if ((value % 1) != 0)
        return value.toString().split(".")[1].length;
    return 0;
};