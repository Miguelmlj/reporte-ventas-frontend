export const capitalize = (word) => {
    if (word.length >= 2) return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase().trim();
    else return word;
}