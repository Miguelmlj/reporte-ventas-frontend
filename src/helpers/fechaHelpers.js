export const year_and_month = () => {
    let date = new Date();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    return `${year}-${month.toString().length > 1 ? month : '0' + month}`

}

export const get_month_name = (value) => {
    let month = value.substring(5,7);
    const Months = [
        "",
        "ENERO",
        "FEBRERO",
        "MARZO",
        "ABRIL",
        "MAYO",
        "JUNIO",
        "JULIO",
        "AGOSTO",
        "SEPTIEMBRE",
        "OCTUBRE",
        "NOVIEMBRE",
        "DICIEMBRE",
      ];

      /* console.log('value',value);
      console.log('subtring',value.substring(5,6)); */
    if (value.substring(5,6) === "0") {
        month = value.substring(6,7);
        return Months[month];
    }
    return Months[month];
    
    

    // let number = Number(value);

     
}