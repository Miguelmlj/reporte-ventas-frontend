import React, { useEffect, useState } from 'react'

export const ModalIndicadorLeadsJDPower = (props) => {
    const [leadsJDPower, setLeadsJDPower] = useState({})

    useEffect ( ( ) => {
        setLeadsJDPower ( props.data )
    }, [props.data] )


    const onChange = ( e ) => {
        setLeadsJDPower({
            ...leadsJDPower,
            [e.target.name]: e.target.value
        })
    }

    const resetFormFields = ( ) => {
        setLeadsJDPower({
            Fecha: "",
            Cantidad_encuestas: ""
        })
    }

    return (
        <div style={{backgroundColor: "#FFFFFF"}}>
            <h5 className='text-center text-dark'>JD POWER</h5>

            <div className='row'>

                <div className="container col-12 mt-4">
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={leadsJDPower.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Cantidad de Encuestas</label>
                        <input type="number" min="0" className="form-control" tabIndex={1} value={leadsJDPower.Cantidad_encuestas} onChange={onChange} name="Cantidad_encuestas" autoComplete='off' />
                        <br />

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                props.onSubmit(leadsJDPower)
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={9}>
                        { props.editMode ? "Editar": "Registrar"}
                    </button>


                </div>
            </div>

        </div>
    )
}
