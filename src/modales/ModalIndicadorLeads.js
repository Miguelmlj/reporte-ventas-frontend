import React, { useEffect, useState } from 'react'

export const ModalIndicadorLeads = (props) => {
  const [indicadorLeads, setIndicadorLeads] = useState({})

  useEffect(() => {
    if (props.isEditMode) {
      setIndicadorLeads(props.data)
      return
    }
    setIndicadorLeads(props.data)
  }, [props.data])


  const onChange = (e) => {
    setIndicadorLeads({
      ...indicadorLeads,
      [e.target.name]: e.target.value
    })

  }

  const resetFormFields = () => {
    setIndicadorLeads({
      Fecha: "",
      Oport_registradasPeriodo: "",
      Oport_contactadas: "",
      Oport_contactadasATiempo: "",
      Porcen_contactadosATiempo: ""
    })
  }

  return (
    <div style={{backgroundColor: "#FFFFFF"}}>
      <h5 className='text-center text-dark'>INDICADOR LEADS</h5>

      <div className='row'>

        <div className="container col-12 mt-4">
          <form id="form">

            <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
            <input type="date" className="form-control" tabIndex={1} value={indicadorLeads.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
            <br />
            <label className="bg-secondary input-group-text text-light font-weight-normal"># Oportunidades Registradas</label>
            <input type="number" min="0" className="form-control" tabIndex={1} value={indicadorLeads.Oport_registradasPeriodo} onChange={onChange} name="Oport_registradasPeriodo" autoComplete='off' />
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal"># Oportunidades Contactadas</label>
            <input type="number" min="0" className="form-control" tabIndex={1} value={indicadorLeads.Oport_contactadas} onChange={onChange} name="Oport_contactadas" autoComplete='off' />
            <br />
            <label className="bg-secondary input-group-text text-light font-weight-normal"># Oportunidades Contactadas A Tiempo</label>
            <input type="number" min="0" className="form-control" tabIndex={1} value={indicadorLeads.Oport_contactadasATiempo} onChange={onChange} name="Oport_contactadasATiempo" autoComplete='off' />
            <br />
            <label className="bg-secondary input-group-text text-light font-weight-normal">% Contactados A Tiempo</label>
            <input type="number" min="0" className="form-control" tabIndex={1} value={indicadorLeads.Porcen_contactadosATiempo} onChange={onChange} name="Porcen_contactadosATiempo" autoComplete='off' />
            <br />

          </form>
        </div>



      </div>

      <div className='row justify-content-center'>
        <div className='col-auto'>
          <br />
          
          <button
            className='btn btn-info text-dark font-italic'
            onClick={
              () => {
                props.onSubmit(indicadorLeads)
                resetFormFields();
              }
            }
            type='button'
            tabIndex={9}>
             {props.editMode ? "Editar": "Registrar"} 
          </button>


        </div>
      </div>

    </div>
  )
}
