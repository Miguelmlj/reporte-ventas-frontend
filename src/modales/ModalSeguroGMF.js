import React, { useState, useEffect } from 'react'
import { removerComas } from '../helpers/formatoMoneda'
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../helpers/validarInput'

export const ModalSeguroGMF = ({ data, editMode, onSubmit, asesores }) => {
    const [seguroGMF, setseguroGMF] = useState({})
    const [cambiaDimension, setCambiaDimension] = useState(false)

    useEffect(() => { setseguroGMF(data) }, [data])

    const validateInput = (e, importe) => {

        if (isNumber(e.target.value)) {
            calcularValores(e.target.value);
            return;
        }

        if (!isNumber(e.target.value)) {
            if (isADotValue(e.target.value)) {
                (!hasPointTheInputValue(importe))
                    ? calcularValores(e.target.value)
                    : (getTotalPoints(e.target.value) !== 2) && calcularValores(importe.substring(0, importe.toString().length - 1))
            }
        }

    }

    const onChange = (e) => {
        if (e.target.name === "Importe_Seguro") {
            // calcularValores(e.target.value);
            validateInput(e, seguroGMF.Importe_Seguro)
            return;
        }

        setseguroGMF({
            ...seguroGMF,
            [e.target.name]: e.target.value
        })
    }

    const onBlur = (e) => {
        setseguroGMF({
            ...seguroGMF,
            [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value)
        })
    }

    const onFocus = (e) => {
        setseguroGMF({
            ...seguroGMF,
            [e.target.name]: removerComas(e.target.value)
        })
    }

    const calcularValores = (valor) => {

        //Al valor restar el iva 16% == SUBTOTAL.
        // let Utilidad = valor * 0.13;
        let Utilidad = (valor / 1.16).toFixed(2) * 0.13;
        let ComisionAsesor = Utilidad * 0.2;
        let UtilidadNeta = Utilidad - ComisionAsesor;

        setseguroGMF({
            ...seguroGMF,
            ['Importe_Seguro']: valor,
            ['Utilidad']: new Intl.NumberFormat('es-MX').format(Utilidad),
            ['Comision_Asesor']: new Intl.NumberFormat('es-MX').format(ComisionAsesor),
            ['Utilidad_Neta']: new Intl.NumberFormat('es-MX').format(UtilidadNeta)

        })

    }

    const resetFormFields = () => {
        setseguroGMF({
            Fecha: "",
            Importe_Seguro: "",
            Asesor: "",
            Nombre_Cliente: "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })
    }

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }

    }

    // const onTest = (seg) => {console.log(seg)}

    window.addEventListener('resize', RedimensionarInputs);

    return (
        <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'>SEGURO GMF</h5>

            <div className='row'>

                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={seguroGMF.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Asesor</label>
                        {/* <input 
                        autoComplete='off' 
                        className="form-control" 
                        name="Asesor" 
                        onChange={onChange} 
                        tabIndex={3} 
                        type="text" 
                        value={seguroGMF.Asesor} 
                        />
                        <br /> */}

                        <select name='Asesor' className='form-select' tabIndex={3} onChange={onChange}>
                            {
                                asesores.map((asesor) => {
                                    return (
                                        <option value={asesor.Nombre_Asesor} selected={asesor.Nombre_Asesor === seguroGMF.Asesor}>{asesor.Nombre_Asesor}</option>
                                    )
                                })
                            }
                        </select>
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad</label>
                        <input type="text" className="form-control" readOnly value={seguroGMF.Utilidad} name="Utilidad" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad Neta</label>
                        <input type="text" className="form-control" readOnly value={seguroGMF.Utilidad_Neta} name="Utilidad_Neta" autoComplete='off' />
                        <br />

                    </form>
                </div>

                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                    <form id="form2">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Importe Seguro</label>
                        <input
                            autoComplete='off'
                            className="form-control"
                            name="Importe_Seguro"
                            onBlur={onBlur}
                            onChange={onChange}
                            onFocus={onFocus}
                            tabIndex={2}
                            type="text"
                            value={seguroGMF.Importe_Seguro}
                        />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Cliente</label>
                        <input type="text" className="form-control" tabIndex={4} value={seguroGMF.Nombre_Cliente} onChange={onChange} name="Nombre_Cliente" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Comisión Asesor</label>
                        <input type="text" className="form-control" readOnly value={seguroGMF.Comision_Asesor} name="Comision_Asesor" autoComplete='off' />
                        <br />

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                onSubmit(seguroGMF)
                                resetFormFields();
                                // onTest(seguroGMF);
                            }
                        }
                        type='button'
                        tabIndex={9}>
                        {editMode ? "Editar" : "Registrar"}
                    </button>


                </div>
            </div>

        </div>
    )
}
