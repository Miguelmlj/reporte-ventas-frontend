import React, { useState } from 'react'

import { FormGroup, Label, Input } from "reactstrap";


export const ModalUsuarios = (props) => {
    const [usuario, setUsuario] = useState({
        nombre_usuario: '',
        clave: '',
        responsable: '2',
        tipo: '2'
    })

    const [rol, setRol] = useState(1)
    const [vehiculo, setVehiculo] = useState(1)

    const cambioRadioRol = e => {
        setRol(e.target.value)
    }

    const cambioRadioVehiculo = e => {
        setVehiculo(e.target.value)
    }

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })

        // console.log(usuario)
    }


    return (
        <div className='bg-light'>
            <h5 className='text-center text-dark'>Registrar Usuario</h5>

            <div className='row'>
                <div className='container col-12 pt-2'>
                    <form action="form">

                        <label className="input-group-text text-dark font-weight-normal">Usuario</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} name="nombre_usuario" autoComplete='off' />

                        <label className="input-group-text text-dark font-weight-normal mt-4">Contraseña</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} name="clave" autoComplete='off' />


                        <label className="input-group-text text-dark font-weight-normal mt-4">Rol</label>


                        <div className='mt-2'>

                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="adminRadio1" id="adminRadio1" value="1"
                                    checked={rol == 1 ? true : false} onChange={cambioRadioRol} />
                                <label className="form-check-label" for="adminRadio1">
                                    Admin
                                </label>
                            </div>

                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="hostessRadio2" id="hostessRadio2" value="2"
                                    checked={rol == 2 ? true : false} onChange={cambioRadioRol} />
                                <label className="form-check-label" for="hostessRadio2">
                                    Hostess
                                </label>
                            </div>

                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="bdcRadio3" id="bdcRadio3" value="3"
                                    checked={rol == 3 ? true : false} onChange={cambioRadioRol} />
                                <label className="form-check-label" for="bdcRadio3">
                                    BDC
                                </label>
                            </div>

                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="fyiRadio4" id="fyiRadio4" value="4"
                                    checked={rol == 4 ? true : false} onChange={cambioRadioRol} />
                                <label className="form-check-label" for="fyiRadio4">
                                    F&I
                                </label>
                            </div>

                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="asesorRadio5" id="asesorRadio5" value="5"
                                    checked={rol == 5 ? true : false} onChange={cambioRadioRol} />
                                <label className="form-check-label" for="asesorRadio5">
                                    Asesor Ventas
                                </label>
                            </div>

                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="contadorRadio6" id="contadorRadio6" value="6"
                                    checked={rol == 6 ? true : false} onChange={cambioRadioRol} />
                                <label className="form-check-label" for="contadorRadio6">
                                    Contador
                                </label>
                            </div>

                            <div className="form-check form-check-inline m-2">

                                <input className="form-check-input" type="radio"
                                    name="tiRadio7" id="tiRadio7" value="7"
                                    checked={rol == 7 ? true : false} onChange={cambioRadioRol} />
                                <label className="form-check-label" for="tiRadio7">
                                    TI
                                </label>

                            </div>
                        </div>


                        {/* <select name="responsable" className="form-select form-select-mb" tabIndex={1} onChange={onChange}>

                            <option value="2"  >Hostess</option>
                            <option value="3"  >BDC</option>
                            <option value="4"  >FYI</option>
                            <option value="1"  >Administrador</option>

                        </select> */}


                        <label className="input-group-text text-dark font-weight-normal mt-4">Registro Vehículos</label>
                        <div className='mt-2'>
                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="nuevosRadio1" id="nuevosRadio1" value="1"
                                    checked={vehiculo == 1 ? true : false} onChange={cambioRadioVehiculo} />
                                <label className="form-check-label" for="nuevosRadio1">
                                    Nuevos
                                </label>
                            </div>

                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="seminuevosRadio2" id="seminuevosRadio2" value="2"
                                    checked={vehiculo == 2 ? true : false} onChange={cambioRadioVehiculo} />
                                <label className="form-check-label" for="seminuevosRadio2">
                                    Seminuevos
                                </label>
                            </div>

                            <div className="form-check form-check-inline m-2">
                                <input className="form-check-input" type="radio"
                                    name="ambosRadio3" id="ambosRadio3" value="3"
                                    checked={vehiculo == 3 ? true : false} onChange={cambioRadioVehiculo} />
                                <label className="form-check-label" for="ambosRadio3">
                                    Ambos
                                </label>
                            </div>

                        </div>
                        <label className="input-group-text text-dark font-weight-normal mt-4">Permisos</label>

                    </form>
                </div>

                <br />
                <p>Rol {rol}</p>
                <br />
                <p>Vehiculos {vehiculo}</p>
            </div>

            {/* botones */}
            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />
                    <button className='btn btn-info text-dark mt-5 font-italic' type='button' tabIndex={9} onClick={() => props.onSubmit(usuario)}>
                        Registrar
                    </button>

                </div>
            </div>
        </div>
    )
}
