import React, { useState, useEffect } from 'react'
import { removerComas } from '../helpers/formatoMoneda';
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../helpers/validarInput'

export const ModalCtrlOper_GarantiPlus = ({ data, editMode, onSubmit, asesores }) => {
    const [GarantiPlus, setGarantiPlus] = useState({});
    const [cambiaDimension, setCambiaDimension] = useState(false)

    useEffect(() => { setGarantiPlus(data) }, [data])

    const validateInput = (e, importe) => {
        if ( isNumber( e.target.value )) {
            calcularValores( e.target.name, e.target.value );
            return;
        }

        if (!isNumber(e.target.value)) {
            if (isADotValue(e.target.value)) {
                (!hasPointTheInputValue( importe ))
                ? calcularValores(e.target.name, e.target.value)
                : (getTotalPoints(e.target.value) !== 2) && calcularValores(e.target.name, importe.substring(0, importe.toString().length - 1))
            }
        }

    }

    const onChange = (e) => {

        if (e.target.name === "Importe_GarantiPlus") {
            // calcularValores(e.target.name, e.target.value);
            validateInput(e, GarantiPlus.Importe_GarantiPlus)
            return;
        }
        
        if (e.target.name === "Costo_GarantiPlus") {
            // calcularValores(e.target.name, e.target.value);
            validateInput(e, GarantiPlus.Costo_GarantiPlus)
            return;
        }

        setGarantiPlus({
            ...GarantiPlus,
            [e.target.name]: e.target.value
        })
    }

    const onBlur = (e) => {
        setGarantiPlus({
            ...GarantiPlus,
            [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value)
        })
    }

    const onFocus =(e) => {
        setGarantiPlus({
            ...GarantiPlus,
            [e.target.name]: removerComas(e.target.value)
        })
    }

    const calcularValores = (propiedad, valor) => {
        let Utilidad;
        let ComisionAsesor;
        let UtilidadNeta;

        if (propiedad === "Importe_GarantiPlus") {
            Utilidad = valor - removerComas(GarantiPlus.Costo_GarantiPlus);
            ComisionAsesor = Utilidad * 0.20;
            UtilidadNeta = Utilidad - ComisionAsesor;

            setGarantiPlus({
                ...GarantiPlus,
                ['Importe_GarantiPlus'] : valor,
                ['Utilidad']            : new Intl.NumberFormat('es-MX').format(Utilidad),
                ['Comision_Asesor']     : new Intl.NumberFormat('es-MX').format(ComisionAsesor),
                ['Utilidad_Neta']       : new Intl.NumberFormat('es-MX').format(UtilidadNeta)
            })
        }

        if (propiedad === "Costo_GarantiPlus") {
            Utilidad = removerComas(GarantiPlus.Importe_GarantiPlus) - valor;
            ComisionAsesor = Utilidad * 0.20;
            UtilidadNeta = Utilidad - ComisionAsesor;

            setGarantiPlus({
                ...GarantiPlus,
                ['Costo_GarantiPlus']   : valor,
                ['Utilidad']            : new Intl.NumberFormat('es-MX').format(Utilidad),
                ['Comision_Asesor']     : new Intl.NumberFormat('es-MX').format(ComisionAsesor),
                ['Utilidad_Neta']       : new Intl.NumberFormat('es-MX').format(UtilidadNeta)
            })
        }

    }

    const resetFormFields = () => {
        setGarantiPlus({
            Fecha: "",
            Importe_GarantiPlus: "",
            Costo_GarantiPlus: "",
            Asesor: "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })
    }

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }
    }

    window.addEventListener('resize', RedimensionarInputs);

  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'> GARANTI PLUS </h5>

            <div className='row'>

                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={GarantiPlus.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Costo Garanti Plus</label>
                        <input 
                        autoComplete='off' 
                        className="form-control" 
                        name="Costo_GarantiPlus" 
                        onBlur={onBlur}
                        onChange={onChange}
                        onFocus={onFocus} 
                        tabIndex={3} 
                        type="text" 
                        value={GarantiPlus.Costo_GarantiPlus} 
                        />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad</label>
                        <input type="text" className="form-control" value={GarantiPlus.Utilidad} readOnly name="Utilidad" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad Neta</label>
                        <input type="text" className="form-control" value={GarantiPlus.Utilidad_Neta} readOnly name="Utilidad_Neta" autoComplete='off' />
                        <br />

                    </form>
                </div>

                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                    <form id="form2">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Importe Garanti Plus</label>
                        <input 
                        autoComplete='off' 
                        className="form-control" 
                        name="Importe_GarantiPlus"
                        onBlur={onBlur} 
                        onChange={onChange}
                        onFocus={onFocus} 
                        tabIndex={2} 
                        type="text" 
                        value={GarantiPlus.Importe_GarantiPlus} 
                        />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Asesor</label>
                        {/* <input type="text" className="form-control" tabIndex={4} value={GarantiPlus.Asesor} onChange={onChange} name="Asesor" autoComplete='off' /> */}
                        <select name='Asesor' className='form-select' tabIndex={3} onChange={onChange}>
                            {
                                asesores.map((asesor) => {
                                    return(
                                        <option value={asesor.Nombre_Asesor} selected={asesor.Nombre_Asesor === GarantiPlus.Asesor}>{asesor.Nombre_Asesor}</option>
                                    )
                                })
                            }
                        </select>
                        <br />


                        <label className="bg-secondary input-group-text text-light font-weight-normal">Comisión Asesor</label>
                        <input type="text" className="form-control" value={GarantiPlus.Comision_Asesor} readOnly name="Comision_Asesor" autoComplete='off' />
                        <br />

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                onSubmit(GarantiPlus);
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={5}>
                        {editMode ? "Editar" : "Registrar"}
                    </button>


                </div>
            </div>

        </div>
  )
}
