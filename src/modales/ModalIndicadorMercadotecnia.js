import React,{useState, useEffect} from 'react'

export const ModalIndicadorMercadotecnia = (props) => {
  const [indicadorMkt, setIndicadorMkt] = useState({})

  useEffect(() => { setIndicadorMkt( props.data )},[props.data])

  const onChange = ( e ) => {
    setIndicadorMkt({
      ...indicadorMkt,
      [e.target.name]: e.target.value
    })
  }

  const  resetFormFields = ( ) => {
    setIndicadorMkt({
      Fecha: "",
      Lead_GrupoFelix: "",
      Lead_PaginaLocal: ""
    })
  }


  return (
    <div className='' style={{backgroundColor: "#FFFFFF"}}>
            <h5 className='text-center text-dark'>MERCADOTECNIA</h5>

            <div className='row'>

                <div className="container col-12 mt-4">
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={indicadorMkt.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Cantidad Lead Grupo Félix</label>
                        <input type="number" min="0" className="form-control" tabIndex={1} value={indicadorMkt.Lead_GrupoFelix} onChange={onChange} name="Lead_GrupoFelix" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Cantidad Lead Página Local</label>
                        <input type="number" min="0" className="form-control" tabIndex={1} value={indicadorMkt.Lead_PaginaLocal} onChange={onChange} name="Lead_PaginaLocal" autoComplete='off' />
                        <br />

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                props.onSubmit(indicadorMkt)
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={9}>
                        { props.editMode ? "Editar": "Registrar"}
                    </button>


                </div>
            </div>

        </div>
  )
}
