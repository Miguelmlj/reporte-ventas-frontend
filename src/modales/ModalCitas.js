import React, { useEffect, useState } from 'react'

import "../css/Modal.css";

export const ModalCitas = ({ onSubmit, editMode, data }) => {
    const [citas, setCitas] = useState({})
    
    useEffect(() => { setCitas(data) }, [data])

    useEffect(() => {
        RedimensionarInputs();
    }, [])

    const onChange = (e) => {
        setCitas({
            ...citas,
            [e.target.name]: e.target.value
        })
    }

    const [cambiaDimension, setCambiaDimension] = useState(false)

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }

    }

    const resetFormFields = () => {
        setCitas({
            id_fecha: "",
            cit_agendadas_n: "",
            cit_cumplidas_n: ""
        })
    }

    window.addEventListener('resize', RedimensionarInputs);

    return (
        <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'>CITAS</h5>

            <div className='row'>
                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form">

                        {
                            !editMode
                                ? <><label className="input-group-text text-dark font-weight-normal">Fecha</label><input type="date" className="form-control from-control-sm" tabIndex={1} onChange={onChange} value={citas.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                                : <><label className="input-group-text text-dark font-weight-normal">Fecha</label><input readOnly type="date" className="form-control from-control-sm" tabIndex={1} onChange={onChange} value={citas.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                        }

                        <label className="input-group-text text-dark font-weight-normal">Agendadas</label>
                        <input type="number" className="form-control font-weight-normal" tabIndex={1} onChange={onChange} value={citas.cit_agendadas_n} name="cit_agendadas_n" autoComplete='off' />
                        <br />
                        <label className="input-group-text text-dark font-weight-normal">Cumplidas</label>
                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={citas.cit_cumplidas_n} name="cit_cumplidas_n" autoComplete="off" />
                        <br />

                    </form>
                </div>
                {/* <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form2">
                    <label className="input-group-text text-dark font-weight-normal">1 Cita</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} name="cit_primera_cita_n" autoComplete='off' />
                        <br />
                        <label className="input-group-text text-dark font-weight-normal">Digitales</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} name="cit_digitales_n" autoComplete='off' />

                    </form>
                </div> */}
            </div>

            {/* botones */}
            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />
                    <button
                        className='btn btn-info text-dark mr-2 mt-5 font-italic'
                        type='button'
                        tabIndex={9}
                        onClick={
                            () => {
                                onSubmit(citas);
                                resetFormFields();
                            }}>
                        {editMode ? "Editar" : "Registrar"}
                    </button>

                    {/* <button className="btn btn-secondary text-dark mt-5 font-italic" onClick={borrar} type="reset">Limpiar</button> */}
                </div>
            </div>
        </div>


    )
}
