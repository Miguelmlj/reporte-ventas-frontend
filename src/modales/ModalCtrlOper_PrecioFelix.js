import React, { useState, useEffect } from 'react'
import { removerComas } from '../helpers/formatoMoneda'
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../helpers/validarInput'

export const ModalCtrlOper_PrecioFelix = ({ data, editMode, onSubmit, asesores }) => {
    const [PrecioFelix, setPrecioFelix] = useState({});
    const [cambiaDimension, setCambiaDimension] = useState(false)

    useEffect(() => { setPrecioFelix(data) }, [data])

    const validateInput = (e, importe) => {
        if (isNumber(e.target.value)) {
            calcularValores(e.target.value);
            return;
        }

        if (!isNumber(e.target.value)) {
            if (isADotValue(e.target.value)) {
                (!hasPointTheInputValue(importe))
                    ? calcularValores(e.target.value)
                    : (getTotalPoints(e.target.value) !== 2) && calcularValores(importe.substring(0, importe.toString().length - 1))
            }
        }

    }

    const onChange = (e) => {
        if (e.target.name === "Importe_PrecioFelix") {
            // calcularValores(e.target.value);
            validateInput(e, PrecioFelix.Importe_PrecioFelix)
            return;
        }

        setPrecioFelix({
            ...PrecioFelix,
            [e.target.name]: e.target.value
        })
    }

    const onBlur = (e) => {
        setPrecioFelix({
            ...PrecioFelix,
            [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value) 
        })
    }

    const onFocus = (e) => {
        setPrecioFelix({
            ...PrecioFelix,
            [e.target.name]: removerComas(e.target.value) 
        })
    }

    const calcularValores = (valor) => {
        let Utilidad = valor;
        let ComisionAsesor = Utilidad * 0.1;
        let UtilidadNeta = Utilidad - ComisionAsesor;

        setPrecioFelix({
            ...PrecioFelix,
            ['Importe_PrecioFelix']: valor,
            ['Utilidad']: new Intl.NumberFormat('es-MX').format(Utilidad),
            ['Comision_Asesor']: new Intl.NumberFormat('es-MX').format(ComisionAsesor),
            ['Utilidad_Neta']: new Intl.NumberFormat('es-MX').format(UtilidadNeta)

        })

    }

    const resetFormFields = () => {
        setPrecioFelix({
            Fecha: "",
            Importe_PrecioFelix: "",
            Asesor: "",
            Nombre_Cliente: "",
            Factura_PrecioFelix: "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })
    }

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }
    }

    window.addEventListener('resize', RedimensionarInputs);

    return (
        <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'> PRECIO FÉLIX </h5>

            <div className='row'>

                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={PrecioFelix.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Asesor</label>
                        {/* <input type="text" className="form-control" tabIndex={3} value={PrecioFelix.Asesor} onChange={onChange} name="Asesor" autoComplete='off' /> */}
                        <select name='Asesor' className='form-select' tabIndex={3} onChange={onChange}>
                            {
                                asesores.map((asesor) => {
                                    return (
                                        <option value={asesor.Nombre_Asesor} selected={asesor.Nombre_Asesor === PrecioFelix.Asesor}>{asesor.Nombre_Asesor}</option>
                                    )
                                })
                            }
                        </select>
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Factura Precio Félix</label>
                        <input type="text"  className="form-control" tabIndex={5} value={PrecioFelix.Factura_PrecioFelix} onChange={onChange} name="Factura_PrecioFelix" autoComplete='off' />
                        <br />
                        
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Comisión Asesor</label>
                        <input type="text" className="form-control" value={PrecioFelix.Comision_Asesor} readOnly name="Comision_Asesor" autoComplete='off' />
                        <br />

                        

                    </form>
                </div>

                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                    <form id="form2">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Importe Precio Félix</label>
                        <input 
                        autoComplete='off' 
                        className="form-control" 
                        name="Importe_PrecioFelix"
                        onBlur={onBlur} 
                        onChange={onChange}
                        onFocus={onFocus} 
                        tabIndex={2} 
                        type="text" 
                        value={PrecioFelix.Importe_PrecioFelix} 
                        />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Cliente</label>
                        <input type="text" className="form-control" tabIndex={4} value={PrecioFelix.Nombre_Cliente} onChange={onChange} name="Nombre_Cliente" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad</label>
                        <input type="text" className="form-control" value={PrecioFelix.Utilidad} readOnly name="Utilidad" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad Neta</label>
                        <input type="text" className="form-control" value={PrecioFelix.Utilidad_Neta} readOnly name="Utilidad_Neta" autoComplete='off' />
                        <br />

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                onSubmit(PrecioFelix);
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={6}>
                        {editMode ? "Editar" : "Registrar"}
                    </button>


                </div>
            </div>

        </div>
    )
}
