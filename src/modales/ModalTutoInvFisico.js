import React,{ useState } from 'react'
import AwesomeSlider from 'react-awesome-slider'
import 'react-awesome-slider/dist/styles.css';
import '../css/ModalTutoInvFisico.css'
import first from '../assets/images/tutorial_carrousel/first.PNG'
import second from '../assets/images/tutorial_carrousel/second.PNG'
import third from '../assets/images/tutorial_carrousel/third.PNG'
import fourth from '../assets/images/tutorial_carrousel/fourth.PNG'
import fifth from '../assets/images/tutorial_carrousel/fifth.PNG'

export const ModalTutoInvFisico = () => {
    // https://www.npmjs.com/package/react-awesome-slider

    let titleSlideOne = "Consulta de Inventario"
    let titleSlideTwo = "Exportar Inventario a Excel"
    let titleSlideThree = "Exportando a Excel"
    let titleSlideFour = "Imprimir Inventario"
    let titleSlideFive = "Imprimiendo"
    const title = [titleSlideOne, titleSlideTwo, titleSlideThree, titleSlideFour, titleSlideFive]
    const [indexSlide, setIndexSlide] = useState(0)

    const changeSlideTitle = (e) => {
        setIndexSlide(e.currentIndex)
    }

  return (
    <div className='carrousel__container'>
            <h5 className='text-center text-dark'>{title[indexSlide]}</h5>
            <div className='row'>
                <AwesomeSlider 
                onTransitionEnd={changeSlideTitle}
                >
                    <div data-src={first}/>
                    <div data-src={second}/>
                    <div data-src={third}/>
                    <div data-src={fourth}/>
                    <div data-src={fifth}/>
                </AwesomeSlider>
            </div>
        </div>

  )
}
