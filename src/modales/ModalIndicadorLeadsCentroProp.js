import React, { useEffect, useState } from 'react'

export const ModalIndicadorLeadsCentroProp = (props) => {
    const [leadsCentroProp, setLeadsCentroProp] = useState({})

    useEffect(() => {
        if (props.isEditMode) {
            setLeadsCentroProp(props.data)
            return
          }
          setLeadsCentroProp(props.data)
    }, [props.data])
    
    const onChange = (e) => {
        setLeadsCentroProp({
          ...leadsCentroProp,
          [e.target.name]: e.target.value
        })

    }

    const resetFormFields = () => {
        setLeadsCentroProp({
          Fecha: "",
          Encuesta_pulso:""
        })
      }


    return (
        <div style={{backgroundColor: "#FFFFFF"}}>
            <h5 className='text-center text-dark'>CENTRO DE PROPIETARIO</h5>

            <div className='row'>

                <div className="container col-12 mt-4">
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={leadsCentroProp.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal"># Encuesta de Pulso</label>
                        <input type="number" min="0" className="form-control" tabIndex={1} value={leadsCentroProp.Encuesta_pulso} onChange={onChange} name="Encuesta_pulso" autoComplete='off' />
                        <br />

                    </form>
                </div>



            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                props.onSubmit(leadsCentroProp)
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={9}>
                       { props.editMode ? "Editar": "Registrar" }
                        
                    </button>


                </div>
            </div>

        </div>
    )
}
