import React, { useState, useEffect } from 'react'
import { removerComas } from '../helpers/formatoMoneda'
import { getTotalPoints, isNumber, hasPointTheInputValue, isADotValue } from '../helpers/validarInput'

export const ModalCtrlOper_onStar = ({ data, editMode, onSubmit, asesores }) => {
  const [OnStar, setOnStar] = useState({})
  const [cambiaDimension, setCambiaDimension] = useState(false)

  useEffect(() => { setOnStar(data) }, [data])

  const validateInput = (e, importe) => {
    if ( isNumber( e.target.value ) ) {
      calcularValores( e.target.value )
      return;
    }

    if (!isNumber(e.target.value)) {
      if (isADotValue(e.target.value)) {
        (!hasPointTheInputValue( importe ))
          ? calcularValores( e.target.value )
          : (getTotalPoints(e.target.value) !== 2) && calcularValores(importe.substring(0, importe.toString().length - 1))
      }

    }

  }

  const onChange = (e) => {
    
    if ( e.target.name === "Importe_OnStar" ) {
      // calcularValores(e.target.value);
      validateInput(e, OnStar.Importe_OnStar)
      return;
    }

    setOnStar({
      ...OnStar,
      [e.target.name]: e.target.value
    })


  }

  const onBlur = (e) => {
    setOnStar({
      ...OnStar,
      [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value)
    })
  }
  
  const onFocus = (e) => {
    setOnStar({
      ...OnStar,
      [e.target.name]: removerComas( e.target.value )
    })
  }

  const calcularValores = (valor) => {
    let Utilidad = valor * 0.2;
    let ComisionAsesor = Utilidad * 0.2;
    let UtilidadNeta = Utilidad - ComisionAsesor;

    setOnStar({
      ...OnStar,
      ['Importe_OnStar']  : valor,
      ['Utilidad']        : new Intl.NumberFormat('es-MX').format(Utilidad),
      ['Comision_Asesor'] : new Intl.NumberFormat('es-MX').format(ComisionAsesor),
      ['Utilidad_Neta']   : new Intl.NumberFormat('es-MX').format(UtilidadNeta)
      
    })
    
  }

  const resetFormFields = () => {
    setOnStar({
      Fecha: "",
      Importe_OnStar: "",
      Asesor: "",
      Nombre_Cliente: "",
      Utilidad: "",
      Comision_Asesor: "",
      Utilidad_Neta: ""
    })
  }

  const RedimensionarInputs = () => {
    if (window.innerWidth <= 500) {
      setCambiaDimension(true)
    } else {
      setCambiaDimension(false)
    }

  }

  window.addEventListener('resize', RedimensionarInputs);

  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
      <h5 className='text-center text-dark'> ON STAR </h5>

      <div className='row'>

        <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
          <form id="form">

            <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
            <input type="date" className="form-control" tabIndex={1} value={OnStar.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Asesor</label>
            {/* <input type="text" className="form-control" tabIndex={3} value={OnStar.Asesor} onChange={onChange} name="Asesor" autoComplete='off' /> */}
            <select name='Asesor' className='form-select' tabIndex={3} onChange={onChange}>
              {
                asesores.map((asesor) => {
                  return (
                      <option value={asesor.Nombre_Asesor} selected={asesor.Nombre_Asesor === OnStar.Asesor}>{asesor.Nombre_Asesor}</option>
                  )
              })
              }
            </select>
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad</label>
            {/* <input type="number" min="0" className="form-control" tabIndex={5} value={OnStar.Utilidad} onChange={onChange} name="Utilidad" autoComplete='off' /> */}
            <input type="text" className="form-control"  value={OnStar.Utilidad} readOnly name="Utilidad" autoComplete='off' />
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad Neta</label>
            {/* <input type="number" min="0" className="form-control" tabIndex={7} value={OnStar.Utilidad_Neta} onChange={onChange} name="Utilidad_Neta" autoComplete='off' /> */}
            <input type="text" className="form-control" value={OnStar.Utilidad_Neta} readOnly name="Utilidad_Neta" autoComplete='off' />
            <br />

          </form>
        </div>

        <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
          <form id="form2">

            <label className="bg-secondary input-group-text text-light font-weight-normal">Importe OnStar</label>
            <input 
            autoComplete='off' 
            className="form-control" 
            name="Importe_OnStar"
            onBlur={onBlur} 
            onChange={onChange}
            onFocus={onFocus} 
            tabIndex={2} 
            type="text" 
            value={OnStar.Importe_OnStar} 
            />
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Cliente</label>
            <input type="text" className="form-control" tabIndex={4} value={OnStar.Nombre_Cliente} onChange={onChange} name="Nombre_Cliente" autoComplete='off' />
            <br />


            <label className="bg-secondary input-group-text text-light font-weight-normal">Comisión Asesor</label>
            {/* <input type="number" min="0" className="form-control" tabIndex={6} value={OnStar.Comision_Asesor} onChange={onChange} name="Comision_Asesor" autoComplete='off' /> */}
            <input type="text" className="form-control"  value={OnStar.Comision_Asesor} readOnly name="Comision_Asesor" autoComplete='off' />
            <br />

          </form>
        </div>

      </div>

      <div className='row justify-content-center'>
        <div className='col-auto'>
          <br />

          <button
            className='btn btn-info text-dark font-italic'
            onClick={
              () => {
                onSubmit(OnStar)
                resetFormFields();
              }
            }
            type='button'
            tabIndex={9}>
            {editMode ? "Editar" : "Registrar"}
          </button>


        </div>
      </div>

    </div>
  )

}
