import React, { useState, useEffect } from 'react'
import { removerComas } from '../helpers/formatoMoneda';
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../helpers/validarInput'

export const ModalCtrlOper_Accesorios = ({ data, editMode, onSubmit, asesores }) => {
    const [Accesorio, setAccesorio] = useState({});
    const [cambiaDimension, setCambiaDimension] = useState(false)

    useEffect(() => { setAccesorio(data) }, [data])

    const validateInput = (e, importe) => {
        if (isNumber(e.target.value)) {
            calcularValores(e.target.name, e.target.value)
            return;
        }

        if (!isNumber(e.target.value)) {
            if (isADotValue(e.target.value)) {
                (!hasPointTheInputValue( importe ))
                ? calcularValores(e.target.name, e.target.value)
                : (getTotalPoints(e.target.value) !== 2) && calcularValores(e.target.name, importe.substring(0, importe.toString().length - 1))
            }
        }

    }

    const onChange = (e) => {

        if (e.target.name === "Importe_Accesorio") {
            // calcularValores(e.target.name, e.target.value);
            validateInput(e, Accesorio.Importe_Accesorio)
            return;
        }

        if (e.target.name === "Costo_Accesorio") {
            // calcularValores(e.target.name, e.target.value);
            validateInput(e, Accesorio.Costo_Accesorio)
            return;
        }

        setAccesorio({
            ...Accesorio,
            [e.target.name]: e.target.value
        })

    }

    const onBlur = (e) => {
        setAccesorio({
            ...Accesorio,
            [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value)
        })
    }

    const onFocus = (e) => {
        setAccesorio({
            ...Accesorio,
            [e.target.name]: removerComas(e.target.value)
        })

    }

    const calcularValores = (propiedad, valor) => {
        let Utilidad;
        let ComisionAsesor;
        let UtilidadNeta;

        if (propiedad === "Importe_Accesorio") {
            Utilidad = valor - removerComas(Accesorio.Costo_Accesorio);
            ComisionAsesor = (Utilidad * 0.20).toFixed(2);
            UtilidadNeta = (Utilidad - ComisionAsesor).toFixed(2);

            setAccesorio({
                ...Accesorio,
                ['Importe_Accesorio']  : valor,
                ['Utilidad']           : new Intl.NumberFormat('es-MX').format(Utilidad),
                ['Comision_Asesor']    : new Intl.NumberFormat('es-MX').format(ComisionAsesor),
                ['Utilidad_Neta']      : new Intl.NumberFormat('es-MX').format(UtilidadNeta)
            })

        }

        if (propiedad === "Costo_Accesorio") {
            Utilidad = removerComas(Accesorio.Importe_Accesorio) - valor;
            ComisionAsesor = (Utilidad * 0.20).toFixed(2);
            UtilidadNeta = (Utilidad - ComisionAsesor).toFixed(2);

            setAccesorio({
                ...Accesorio,
                ['Costo_Accesorio']    : valor,
                ['Utilidad']           : new Intl.NumberFormat('es-MX').format(Utilidad),
                ['Comision_Asesor']    : new Intl.NumberFormat('es-MX').format(ComisionAsesor),
                ['Utilidad_Neta']      : new Intl.NumberFormat('es-MX').format(UtilidadNeta)
            })
        }


    }

    const resetFormFields = () => {
        setAccesorio({
            Fecha: "",
            Importe_Accesorio: "",
            Descrip_Accesorio: "",
            Codigo_Accesorio: "",
            Costo_Accesorio: "",
            Asesor: "",
            Nombre_Cliente: "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })
    }

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }
    }

    window.addEventListener('resize', RedimensionarInputs);

    return (
        <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'> ACCESORIO </h5>

            <div className='row'>

                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-4"}>
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={Accesorio.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Descripción Accesorio</label>
                        <input type="text" className="form-control" tabIndex={3} value={Accesorio.Descrip_Accesorio} onChange={onChange} name="Descrip_Accesorio" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Importe Accesorio</label>
                        <input
                            autoComplete='off'
                            className="form-control"
                            name="Importe_Accesorio"
                            onBlur={onBlur}
                            onChange={onChange}
                            onFocus={onFocus}
                            tabIndex={5}
                            type="text"
                            value={Accesorio.Importe_Accesorio}
                        />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Cliente</label>
                        <input type="text" className="form-control" tabIndex={7} value={Accesorio.Nombre_Cliente} onChange={onChange} name="Nombre_Cliente" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Comisión Asesor</label>
                        <input type="text" className="form-control" value={Accesorio.Comision_Asesor} readOnly name="Comision_Asesor" autoComplete='off' />
                        <br />

                    </form>
                </div>

                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-4"}>
                    <form id="form2">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Código Accesorio</label>
                        <input type="text" className="form-control" tabIndex={2} value={Accesorio.Codigo_Accesorio} onChange={onChange} name="Codigo_Accesorio" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Costo Accesorio</label>
                        <input
                            autoComplete='off'
                            className="form-control"
                            name="Costo_Accesorio"
                            onBlur={onBlur}
                            onChange={onChange}
                            onFocus={onFocus}
                            tabIndex={4}
                            type="text"
                            value={Accesorio.Costo_Accesorio}
                        />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Asesor</label>
                        {/* <input type="text" className="form-control" tabIndex={6} value={Accesorio.Asesor} onChange={onChange} name="Asesor" autoComplete='off' /> */}
                        
                            <select name='Asesor' className='form-select' tabIndex={6} onChange={onChange}>
                                {
                                    asesores.map((asesor) => {
                                        return (
                                            <option value={asesor.Nombre_Asesor} selected={asesor.Nombre_Asesor === Accesorio.Asesor}>{asesor.Nombre_Asesor}</option>
                                        )
                                    })
                                }
                            </select>    
                        
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad</label>
                        <input type="text" className="form-control" value={Accesorio.Utilidad} readOnly name="Utilidad" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad Neta</label>
                        <input type="text" className="form-control" value={Accesorio.Utilidad_Neta} readOnly name="Utilidad_Neta" autoComplete='off' />
                        <br />
                    </form>

                </div>

            </div>




            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                onSubmit(Accesorio);
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={8}>
                        {editMode ? "Editar" : "Registrar"}
                    </button>


                </div>
            </div>

        </div>
    )
}
