import React, { useEffect, useState } from 'react'
import "../css/Modal.css";

export const ModalAfluencia = ({ onSubmit, editMode, data }) => {

    //POSIBLES USUARIOS A ACCEDER A MODULO AFLUENCIA. INICIALIZADOS EN FALSE.
    const [hostessNuevos, setHostessNuevos] = useState(false)
    const [hostessSeminuevos, setHostessSeminuevos] = useState(false)
    const [administrador, setAdministrador] = useState(false)
    const [afluencia, setAfluencia] = useState({})

    useEffect(() => { setAfluencia(data) }, [data])

    useEffect(() => {
        asignaTipoUsuarioLogueado();
        RedimensionarInputs();
    }, [])

    const asignaTipoUsuarioLogueado = () => {
        //1.- Administrador, 2.-Nuevos, 3.- Seminuevos
        if (window.sessionStorage.getItem('tipo_usuario') === '1') {
            setAdministrador(true)
            return
        }

        if (window.sessionStorage.getItem('tipo_usuario') === '2') {
            setHostessNuevos(true)

        } else if (window.sessionStorage.getItem('tipo_usuario') === '3') {
            setHostessSeminuevos(true)
        }

    }

    const onChange = (e) => {
        setAfluencia({
            ...afluencia,
            [e.target.name]: e.target.value
        })
    }

    const [cambiaDimension, setCambiaDimension] = useState(false)

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }

    }

    const resetFormFields = () => {
        setAfluencia({
            id_fecha: "",
            af_fresh_n: "",
            af_bback_n: "",
            af_primera_cita_n: "",
            af_digitales_n: "",

            af_fresh_s: "",
            af_bback_s: "",
            af_primera_cita_s: "",
            af_digitales_s: ""
        })
    }

    window.addEventListener('resize', RedimensionarInputs);

    return (
        <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className="text-center text-dark">{administrador ? 'AFLUENCIA' : hostessNuevos ? 'AFLUENCIA NUEVOS' : 'AFLUENCIA SEMINUEVOS'}</h5>
            <div className="row">
                { //si el usuario es hostess nuevos
                    hostessNuevos ?
                        <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                            <form id="form">
                                {/* <label className="bg-secondary input-group-text text-dark font-italic">NUEVOS</label> */}
                                <br />
                                {
                                    !editMode
                                        ? <><label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label><input type="date" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                                        : <><label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label><input readOnly type="date" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                                }
                                <label className="bg-secondary input-group-text text-light font-weight-normal">Fresh</label>
                                <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_fresh_n} name="af_fresh_n" autoComplete='off' />
                                <br />
                                <label className="bg-secondary input-group-text text-light font-weight-normal">Bback</label>
                                <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_bback_n} name="af_bback_n" autoComplete='off' />
                                <br />
                                <label className="bg-secondary input-group-text text-light font-weight-normal">1 cita</label>
                                <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_primera_cita_n} name="af_primera_cita_n" autoComplete='off' />
                                <br />
                                <label className="bg-secondary input-group-text text-light font-weight-normal">Digital</label>
                                <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_digitales_n} name="af_digitales_n" autoComplete='off' />



                            </form>

                        </div>
                        //si el usuario es hostess seminuevos
                        : hostessSeminuevos ?
                            <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                                <form id="form2">
                                    {/* <label className=" input-group-text text-dark font-italic">SEMINUEVOS</label> */}
                                    <br />
                                    {
                                        !editMode
                                            ? <><label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label><input type="date" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                                            : <><label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label><input readOnly type="date" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                                    }
                                    <label className="bg-secondary input-group-text text-light font-weight-normal">Fresh</label>
                                    <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_fresh_s} name="af_fresh_s" autoComplete='off' />
                                    <br />
                                    <label className="bg-secondary input-group-text text-light font-weight-normal">Bback</label>
                                    <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_bback_s} name="af_bback_s" autoComplete='off' />
                                    <br />
                                    <label className="bg-secondary input-group-text text-light font-weight-normal">1 cita</label>
                                    <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_primera_cita_s} name="af_primera_cita_s" autoComplete='off' />
                                    <br />
                                    <label className="bg-secondary input-group-text text-light font-weight-normal">Digital</label>
                                    <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_digitales_s} name="af_digitales_s" autoComplete='off' />
                                    <br />


                                </form>

                            </div>

                            :


                            //hasta este punto el usuario será administrador, no hoster


                            <>
                                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                                    <form id="form">
                                        <label className="bg-secondary input-group-text text-light font-italic">NUEVOS</label>
                                        <br />
                                        {
                                            !editMode
                                                ? <><label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label><input type="date" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                                                : <><label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label><input readOnly type="date" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                                        }
                                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fresh</label>
                                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_fresh_n} name="af_fresh_n" autoComplete='off' />
                                        <br />
                                        <label className="bg-secondary input-group-text text-light font-weight-normal">Bback</label>
                                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_bback_n} name="af_bback_n" autoComplete='off' />
                                        <br />
                                        <label className="bg-secondary input-group-text text-light font-weight-normal">1 cita</label>
                                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_primera_cita_n} name="af_primera_cita_n" autoComplete='off' />
                                        <br />
                                        <label className="bg-secondary input-group-text text-light font-weight-normal">Digital</label>
                                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_digitales_n} name="af_digitales_n" autoComplete='off' />



                                    </form>

                                </div>
                                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                                    <form id="form2">
                                        <label className=" input-group-text text-dark font-italic">SEMINUEVOS</label>
                                        <br />

                                        <label className=" input-group-text text-dark font-weight-normal">Fresh</label>
                                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_fresh_s} name="af_fresh_s" autoComplete='off' />
                                        <br />
                                        <label className=" input-group-text text-dark font-weight-normal">Bback</label>
                                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_bback_s} name="af_bback_s" autoComplete='off' />
                                        <br />
                                        <label className=" input-group-text text-dark font-weight-normal">1 cita</label>
                                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_primera_cita_s} name="af_primera_cita_s" autoComplete='off' />
                                        <br />
                                        <label className=" input-group-text text-dark font-weight-normal">Digital</label>
                                        <input type="number" className="form-control" tabIndex={1} onChange={onChange} value={afluencia.af_digitales_s} name="af_digitales_s" autoComplete='off' />
                                        <br />


                                    </form>

                                </div>

                            </>



                }
            </div>
            {/* BOTONES */}
            <div className="row justify-content-center">
                <div className="col-auto">
                    <br />
                    <button
                        className="btn btn-info text-dark mr-2 font-italic"
                        type="button"
                        tabIndex={9}
                        onClick={() => {
                            onSubmit(afluencia);
                            resetFormFields();
                        }}

                    >
                        {editMode ? "Editar" : "Registrar"}
                    </button>

                </div>

            </div>

        </div>



    )
}
