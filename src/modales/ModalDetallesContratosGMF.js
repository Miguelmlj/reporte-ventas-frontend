import React, { useState, useEffect } from 'react'
import { removerComas } from '../helpers/formatoMoneda'
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../helpers/validarInput'

export const ModalDetallesContratosGMF = ({ data, editMode, onSubmit }) => {
  const [DetallesContratosGMF, setDetallesContratosGMF] = useState({})

  useEffect(() => { setDetallesContratosGMF(data) }, [data])

  const validateInput = (e, importe) => {
    if (isNumber(e.target.value)) {
      calcularValores(e.target.value);
      return;
    }

    if (!isNumber(e.target.value)) {
      if (isADotValue(e.target.value)) {
          (!hasPointTheInputValue(importe))
              ? calcularValores(e.target.value)
              : (getTotalPoints(e.target.value) !== 2) && calcularValores(importe.substring(0, importe.toString().length - 1))
      }
  }

  }

  const onChange = (e) => {
    if (e.target.name === "Monto_ContratoAFinanciar") {
      // calcularValores(e.target.value);
      validateInput(e, DetallesContratosGMF.Monto_ContratoAFinanciar)
      return;
    }

    setDetallesContratosGMF({
      ...DetallesContratosGMF,
      [e.target.name]: e.target.value
    })

  }

  const onBlur = (e) => {
    setDetallesContratosGMF({
      ...DetallesContratosGMF,
      [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value)
    })
  }

  const onFocus = (e) => {
    setDetallesContratosGMF({
      ...DetallesContratosGMF,
      [e.target.name]: removerComas(e.target.value)
    })
  }

  const calcularValores = (monto) => {
    let Utilidad = monto * 0.022;

    setDetallesContratosGMF({
      ...DetallesContratosGMF,
      ['Monto_ContratoAFinanciar']  : monto,
      ['Utilidad']                  : new Intl.NumberFormat('es-MX').format(Utilidad),

    })
  }

  const resetFormFields = () => {
    setDetallesContratosGMF({
      Fecha: "",
      Folio_Contrato: "",
      Monto_ContratoAFinanciar: "",
      Nombre_Cliente: "",
      Utilidad: ""
    })
  }


  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
      <h5 className='text-center text-dark'> DETALLES CONTRATOS GMF </h5>

      <div className='row'>

        <div className="container col-12 mt-4">
          <form id="form">

            <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
            <input type="date" className="form-control" tabIndex={1} value={DetallesContratosGMF.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal">Folio Contrato</label>
            <input type="text" className="form-control" tabIndex={2} value={DetallesContratosGMF.Folio_Contrato} onChange={onChange} name="Folio_Contrato" autoComplete='off' />
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal">Monto Contrato A Financiar</label>
            <input
              autoComplete='off'
              className="form-control"
              name="Monto_ContratoAFinanciar"
              onBlur={onBlur}
              onChange={onChange}
              onFocus={onFocus}
              tabIndex={3}
              type="text"
              value={DetallesContratosGMF.Monto_ContratoAFinanciar}
            />
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Cliente</label>
            <input type="text" className="form-control" tabIndex={4} value={DetallesContratosGMF.Nombre_Cliente} onChange={onChange} name="Nombre_Cliente" autoComplete='off' />
            <br />

            <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad</label>
            <input
              autoComplete='off'
              className="form-control"
              readOnly name="Utilidad"
              type="text"
              value={DetallesContratosGMF.Utilidad}
            />
            <br />



          </form>
        </div>



      </div>

      <div className='row justify-content-center'>
        <div className='col-auto'>
          <br />

          <button
            className='btn btn-info text-dark font-italic'
            onClick={
              () => {
                onSubmit(DetallesContratosGMF);
                resetFormFields();
              }
            }
            type='button'
            tabIndex={5}>
            {editMode ? "Editar" : "Registrar"}
          </button>


        </div>
      </div>

    </div>
  )
}
