// import { isNaN } from 'lodash'
import React, { useEffect, useState } from 'react'
import { removerComas } from '../helpers/formatoMoneda'
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../helpers/validarInput'

export const ModalIndicadorFYI = (props) => {
    const [indicadorFYI, setIndicadorFYI] = useState({})

    const [cambiaDimension, setCambiaDimension] = useState(false)

    useEffect(() => {
        setIndicadorFYI(props.data)
    }, [props.data])

    useEffect(() => {
        RedimensionarInputs()
    }, [])

    const validateInputImporteAccesorios =(e, importe) => {

        if(isNumber(e.target.value)) {
                setIndicadorFYI({
                    ...indicadorFYI,
                    [e.target.name]: e.target.value
                })
            return;
        }

        if (!isNumber(e.target.value)) {
            //si el valor es punto
            if (isADotValue(e.target.value)) {
                //si el input aun no tiene el punto
                if (!hasPointTheInputValue(importe)) {
                    setIndicadorFYI({
                        ...indicadorFYI,
                        [e.target.name]: e.target.value
                    })

                } else {

                    if (getTotalPoints(e.target.value) !== 2) {
                        setIndicadorFYI({
                            ...indicadorFYI,
                            [e.target.name]: importe.substring(0, importe.toString().length - 1)
                        })
                    }

                }

            }

        } 
    }

    const onChange = (e) => {

        if (e.target.name === "ImporteAccesorios") {
            validateInputImporteAccesorios(e, indicadorFYI.ImporteAccesorios)
            return;
        }

        setIndicadorFYI({
            ...indicadorFYI,
            [e.target.name]: e.target.value
        })

    }

    const onBlur = (e) => {
            setIndicadorFYI({
                ...indicadorFYI,
                [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value)
            })
    }

    const onFocus = (e) => {
        setIndicadorFYI({
            ...indicadorFYI,
            [e.target.name]: removerComas(e.target.value)
        })
    }

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }

    }

    const resetFormFields = () => {

        setIndicadorFYI({
            Fecha: "",
            GarExtendidas: "",
            Seguros: "",
            OnStart: "",
            ProgValorFactura: "",
            ImporteAccesorios: "",

        })

    }

    window.addEventListener('resize', RedimensionarInputs);


    return (
        <div className='' style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'>INDICADOR F&I</h5>

            <div className='row'>

                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={indicadorFYI.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        {/* <input type="text" className="form-control" tabIndex={1} value={FechaDeHoyFormato()} onChange={onChange} disabled="true" name="fecha" autoComplete='off' /> */}
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Seguros</label>
                        <input type="number" min="0" className="form-control" tabIndex={3} value={indicadorFYI.Seguros} onChange={onChange} name="Seguros" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">GAP: P. de Valor Factura</label>
                        <input type="number" min="0" className="form-control" tabIndex={5} value={indicadorFYI.ProgValorFactura} onChange={onChange} name="ProgValorFactura" autoComplete='off' />
                        <br />

                    </form>
                </div>

                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                    <form id="form2">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Garantías Extendidas</label>
                        <input type="number" min="0" className="form-control" tabIndex={2} value={indicadorFYI.GarExtendidas} onChange={onChange} name="GarExtendidas" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">OnStar</label>
                        <input type="number" min="0" className="form-control" tabIndex={4} value={indicadorFYI.OnStart} onChange={onChange} name="OnStart" autoComplete='off' />
                        <br />


                        <label className="bg-secondary input-group-text text-light font-weight-normal">Importe en Accesorios</label>
                        <input 
                        autoComplete='off' 
                        className="form-control" 
                        name="ImporteAccesorios" 
                        onBlur={onBlur} 
                        onChange={onChange}
                        onFocus={onFocus} 
                        tabIndex={6} 
                        type="text" 
                        value={indicadorFYI.ImporteAccesorios} 
                        />
                        <br />

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />
                    {/* <button className='btn btn-info text-dark font-italic' type='button' tabIndex={9} onClick={imprimir}> */}
                    {/* <button className='btn btn-info text-dark font-italic' type='button' tabIndex={9} onClick={() => props.onSubmit(reporteAnticipo)}> */}
                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                props.onSubmit(indicadorFYI)
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={9}>
                        {props.editMode ? "Editar" : "Registrar"}
                    </button>


                </div>
            </div>

        </div>
    )
}
