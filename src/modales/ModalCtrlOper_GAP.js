import React, { useState, useEffect } from 'react'
import { removerComas } from '../helpers/formatoMoneda'
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../helpers/validarInput'

export const ModalCtrlOper_GAP = ({ data, editMode, onSubmit, asesores }) => {
    const [GAP, setGAP] = useState({});
    const [cambiaDimension, setCambiaDimension] = useState(false)

    useEffect(() => { setGAP(data) }, [data])

    const validarInput = (e, importe) => {
        if (isNumber(e.target.value)) {
            calcularValores(e.target.value);
            return;
        }

        if (!isNumber(e.target.value)) {
            if (isADotValue(e.target.value)) {
                (!hasPointTheInputValue(importe))
                    ? calcularValores(e.target.value)
                    : (getTotalPoints(e.target.value) !== 2) && calcularValores(importe.substring(0, importe.toString().length - 1))
            }
        }

    }

    const onChange = (e) => {
        if (e.target.name === "Importe_GAP") {
            // calcularValores(e.target.value);
            validarInput(e, GAP.Importe_GAP)
            return;
        }

        setGAP({
            ...GAP,
            [e.target.name]: e.target.value
        })
    }

    const onBlur = (e) => {
        setGAP({
            ...GAP,
            [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value)
        })
    }

    const onFocus = (e) => {
        setGAP({
            ...GAP,
            [e.target.name]: removerComas(e.target.value)
        })
    }

    const calcularValores = (valor) => {
        
        let Utilidad = valor * 0.2;
        let ComisionAsesor = Utilidad * 0.2;
        let UtilidadNeta = Utilidad - ComisionAsesor;

        setGAP({
            ...GAP,
            ['Importe_GAP']     : valor,
            ['Utilidad']        : new Intl.NumberFormat('es-MX').format(Utilidad),
            ['Comision_Asesor'] : new Intl.NumberFormat('es-MX').format(ComisionAsesor),
            ['Utilidad_Neta']   : new Intl.NumberFormat('es-MX').format(UtilidadNeta)

        })
    }

    const resetFormFields = () => {
        setGAP({
            Fecha: "",
            Importe_GAP: "",
            Asesor: "",
            Nombre_Cliente: "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })
    }

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }
    }

    window.addEventListener('resize', RedimensionarInputs);

    return (
        <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'> GAP </h5>

            <div className='row'>

                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={GAP.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Asesor</label>
                        {/* <input type="text" className="form-control" tabIndex={3} value={GAP.Asesor} onChange={onChange} name="Asesor" autoComplete='off' /> */}
                        <select name='Asesor' className='form-select' tabIndex={3} onChange={onChange}>
                            {
                                asesores.map((asesor) => {
                                    return(
                                        <option value={asesor.Nombre_Asesor} selected={asesor.Nombre_Asesor === GAP.Asesor}>{asesor.Nombre_Asesor}</option>
                                    )
                                })
                            }
                        </select>
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad</label>
                        <input type="text" className="form-control" value={GAP.Utilidad} readOnly name="Utilidad" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Utilidad Neta</label>
                        <input type="text" className="form-control" value={GAP.Utilidad_Neta} readOnly name="Utilidad_Neta" autoComplete='off' />
                        <br />

                    </form>
                </div>

                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                    <form id="form2">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Importe GAP</label>
                        <input 
                        autoComplete='off' 
                        className="form-control" 
                        name="Importe_GAP"
                        onBlur={onBlur} 
                        onChange={onChange}
                        onFocus={onFocus} 
                        tabIndex={2} 
                        type="text" 
                        value={GAP.Importe_GAP} 
                        />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Cliente</label>
                        <input type="text" className="form-control" tabIndex={4} value={GAP.Nombre_Cliente} onChange={onChange} name="Nombre_Cliente" autoComplete='off' />
                        <br />


                        <label className="bg-secondary input-group-text text-light font-weight-normal">Comisión Asesor</label>
                        <input type="text" className="form-control" value={GAP.Comision_Asesor} readOnly name="Comision_Asesor" autoComplete='off' />
                        <br />

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                onSubmit(GAP);
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={5}>
                        {editMode ? "Editar" : "Registrar"}
                    </button>


                </div>
            </div>

        </div>
    )
}
