import React, { useEffect, useState } from 'react'
import "../css/Modal.css";

export const ModalSolicitudes = ({ onSubmit, editMode, data }) => {
    const [solicitudes, setSolicitudes] = useState({})
    
    useEffect(() => { setSolicitudes(data) }, [data])

    useEffect(() => {
        RedimensionarInputs();
    }, [])

    const onChange = (e) => {
        // console.log(solicitudes)
        setSolicitudes({
            ...solicitudes,
            [e.target.name]: e.target.value
        })

    }

    const [cambiaDimension, setCambiaDimension] = useState(false)

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }
       
    }
    
    const resetFormFields = () => {
        setSolicitudes({
            id_fecha: "",
            //solicitudes nuevos
            sol_gmf_n: "",
            sol_bancos_n: "",
            sol_aprobaciones_gmf_n: "",
            sol_contratos_comprados_n: "",
            //solicitudes seminuevos
            sol_gmf_s: "",
            sol_bancos_s: "",
            sol_aprobaciones_gmf_s: "",
            sol_contratos_comprados_s: "",
        })
    }

    window.addEventListener('resize', RedimensionarInputs);

  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'>SOLICITUDES</h5>
            
            <div className='row'>
                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form">
                    <label className="bg-secondary input-group-text text-light font-italic">NUEVOS</label>
                    <br />
                    {
                        !editMode
                        ?<><label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label><input type="date" className="form-control from-control-sm" tabIndex={1} onChange={onChange} value={solicitudes.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                        :<><label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label><input readOnly type="date" className="form-control from-control-sm" tabIndex={1} onChange={onChange} value={solicitudes.id_fecha} name="id_fecha" autoComplete='off' /><br /></>
                    }
                    <label className="bg-secondary input-group-text text-light font-weight-normal">Solicitudes GMF</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_gmf_n} name="sol_gmf_n" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Bancos</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_bancos_n} name="sol_bancos_n" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Aprobaciones GMF</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_aprobaciones_gmf_n} name="sol_aprobaciones_gmf_n" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Contratos Comprados</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_contratos_comprados_n} name="sol_contratos_comprados_n" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Contratos Leasing</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_contratos_comprados_leasing} name="sol_contratos_comprados_leasing" autoComplete='off' />

                    </form>
                </div>
                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                    <form id="form2">
                    <label className=" input-group-text text-dark font-italic">SEMINUEVOS</label>
                    <br />
                    <label className="input-group-text text-dark font-weight-normal">Solicitudes GMF</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_gmf_s} name="sol_gmf_s" autoComplete='off' />
                        <br />
                        <label className="input-group-text text-dark font-weight-normal">Bancos</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_bancos_s} name="sol_bancos_s" autoComplete='off' />
                        <br />
                        <label className="input-group-text text-dark font-weight-normal">Aprobaciones GMF</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_aprobaciones_gmf_s} name="sol_aprobaciones_gmf_s" autoComplete='off' />
                        <br />
                        <label className="input-group-text text-dark font-weight-normal">Contratos Comprados</label>
                        <input type="text" className="form-control" tabIndex={1} onChange={onChange} value={solicitudes.sol_contratos_comprados_s} name="sol_contratos_comprados_s" autoComplete='off' />

                    </form>
                </div>
            </div>

            {/* botones */}
            <div className='row justify-content-center'>
                <div className='col-auto'>
                <br />
                <button 
                className='btn btn-info text-dark font-italic' 
                type='button' 
                tabIndex={9} 
                onClick={() => {
                    onSubmit(solicitudes);
                    resetFormFields();
                }}
                >
                    {editMode ? "Editar" : "Registrar"}
                </button>
                {/* &nbsp;&nbsp; */}
                {/* <button className="btn btn-secondary text-dark font-italic" onClick={borrar} type="reset">Limpiar</button> */}
                </div>
            </div>
        </div>
  )
}
