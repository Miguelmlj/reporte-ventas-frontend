import React, { useState, useEffect } from 'react'
import { removerComas } from '../helpers/formatoMoneda'
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../helpers/validarInput'

export const ModalCtrlOper_PrecioFelixContado = ({ data, editMode, onSubmit, asesores }) => {
    const [PrecioFelixContado, setPrecioFelixContado] = useState({});

    useEffect(() => { setPrecioFelixContado(data) }, [data])

    const validateInput = ( e, importe) => {
        if (isNumber(e.target.value)) {
            calcularValores(e.target.value);
            return;
        }

        if (!isNumber(e.target.value)) {
            if (isADotValue(e.target.value)) {
                (!hasPointTheInputValue(importe))
                    ? calcularValores(e.target.value)
                    : (getTotalPoints(e.target.value) !== 2) && calcularValores(importe.substring(0, importe.toString().length - 1))
            }
        }
    }

    const onChange = (e) => {
        if (e.target.name === "Importe_PrecioFelixContado") {
            // calcularValores(e.target.value);
            validateInput( e, PrecioFelixContado.Importe_PrecioFelixContado )
            return;
        }

        setPrecioFelixContado({
            ...PrecioFelixContado,
            [e.target.name]: e.target.value
        })
    }

    const onBlur = (e) => {
        setPrecioFelixContado({
            ...PrecioFelixContado,
            [e.target.name]: new Intl.NumberFormat('es-MX').format(e.target.value)
        })
    }

    const onFocus = (e) => {
        setPrecioFelixContado({
            ...PrecioFelixContado,
            [e.target.name]: removerComas(e.target.value)
        })
    }

    const calcularValores = (importePrecioFelixCont) => {
        let ComisionAsesor = importePrecioFelixCont * 0.1;

        setPrecioFelixContado({
            ...PrecioFelixContado,
            ['Importe_PrecioFelixContado']  : importePrecioFelixCont,
            ['Comision_Asesor']             : new Intl.NumberFormat('es-MX').format(ComisionAsesor),

        })
    }

    const resetFormFields = () => {
        setPrecioFelixContado({
            Fecha: "",
            Importe_PrecioFelixContado: "",
            Asesor: "",
            Comision_Asesor: "",
        })
    }


  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'> PRECIO FÉLIX CONTADO </h5>

            <div className='row'>

                <div className="container col-12 mt-4">
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={PrecioFelixContado.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Importe Precio Félix Contado</label>
                        <input 
                        autoComplete='off' 
                        className="form-control" 
                        name="Importe_PrecioFelixContado" 
                        onBlur={onBlur}
                        onChange={onChange} 
                        onFocus={onFocus}
                        tabIndex={2} 
                        type="text" 
                        value={PrecioFelixContado.Importe_PrecioFelixContado} 
                        />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Asesor</label>
                        {/* <input type="text" className="form-control" tabIndex={3} value={PrecioFelixContado.Asesor} onChange={onChange} name="Asesor" autoComplete='off' /> */}
                        <select name='Asesor' className='form-select' tabIndex={3} onChange={onChange}>                        
                        {
                            asesores.map((asesor) => {
                                return (
                                    <option value={asesor.Nombre_Asesor} selected={asesor.Nombre_Asesor === PrecioFelixContado.Asesor}>{asesor.Nombre_Asesor}</option>    
                                )
                            })
                        }
                        </select>
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Comisión Asesor</label>
                        <input type="text" className="form-control" value={PrecioFelixContado.Comision_Asesor} readOnly name="Comision_Asesor" autoComplete='off' />
                        <br />
                       

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                onSubmit(PrecioFelixContado);
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={5}>
                        {editMode ? "Editar" : "Registrar"}
                    </button>


                </div>
            </div>

        </div>
  )
}
