import React, { useState , useEffect } from 'react'

export const ModalIndicadorBDC = (props) => {
    const [indicadorBDC, setIndicadorBDC] = useState({})

    useEffect(() => {
      setIndicadorBDC ( props.data )
    }, [props.data])
    

    const onChange = ( e ) => {
        setIndicadorBDC({
            ...indicadorBDC,
            [e.target.name]: e.target.value
        })
    }

    const  resetFormFields = ( ) => {
        setIndicadorBDC({
            Fecha: "",
            Llamadas_entrantesVehNvo: ""
        })
    }

  return (
    <div style={{backgroundColor: "#FFFFFF"}}>
            <h5 className='text-center text-dark'>INDICADOR BDC</h5>

            <div className='row'>

                <div className="container col-12 mt-4">
                    <form id="form">

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="date" className="form-control" tabIndex={1} value={indicadorBDC.Fecha} onChange={onChange} name="Fecha" autoComplete='off' />
                        <br />
                        <label className="bg-secondary input-group-text text-light font-weight-normal">LLamadas Entrantes Vehículos Nuevos</label>
                        <input type="number" min="0" className="form-control" tabIndex={1} value={indicadorBDC.Llamadas_entrantesVehNvo} onChange={onChange} name="Llamadas_entrantesVehNvo" autoComplete='off' />
                        <br />

                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />

                    <button
                        className='btn btn-info text-dark font-italic'
                        onClick={
                            () => {
                                props.onSubmit(indicadorBDC)
                                resetFormFields();
                            }
                        }
                        type='button'
                        tabIndex={9}>
                        { props.editMode ? "Editar": "Registrar"}
                    </button>


                </div>
            </div>

        </div>
  )
}
