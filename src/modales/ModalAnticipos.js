import React, { useEffect, useState } from 'react'
import "../css/Modal.css";
import { FechaDeHoyFormato, FechaDeHoy } from "../componentes/FechaFormatoHoy"
import { getAgenciaActualizado } from '../helpers/getAgencia';
import { capitalize } from '../helpers/capitalizarPalabra';

export const ModalAnticipos = ({data, nombreAsesor, nombreAgencia, idEmpresa, idSucursal, onSubmit}) => {
    const rol = window.sessionStorage.getItem('responsable');
    const agency = 'Agencia'
    const empresa = 'Empresa'
    const sucursal = 'Sucursal'
    const [cambiaDimension, setCambiaDimension] = useState(false)
    const [reporteAnticipo, setReporteAnticipo] = useState({
        /* empresa: idEmpresa,
        sucursal: idSucursal,
        folio_anticipo: "",
        fecha: FechaDeHoy(), //CREAR FUNCIÓN PARA OBTENER FECHA POR DEFAULT (esta hecha en backend)
        // importe: -1,
        nombre_cliente: "",
        unidad: "",
        paquete: "",
        color: "",
        tipo_compra: "",
        nombre_asesor: nombreAsesor.toUpperCase(),
        agencia: getAgenciaActualizado(idEmpresa, idSucursal) */
    })

    useEffect(() => {setReporteAnticipo(data)}, [data])

    const onChange = (e) => {
        // console.log(e.target.value)
        if ( e.target.name === agency ) {
            const emp = e.target.value.split('-');
            setReporteAnticipo({
                ...reporteAnticipo,
                [e.target.name]: getAgenciaActualizado(emp[0],emp[1]),
                [empresa]: emp[0],
                [sucursal]: emp[1],
            })

            // console.log("reporteAnticipo", reporteAnticipo)
            
            return;
        }
        
        setReporteAnticipo({
            ...reporteAnticipo,
            [e.target.name]: e.target.value.toUpperCase()
        })

        // console.log("reporteAnticipo", reporteAnticipo)
    }

    const borrar = (e) => {
        document.getElementById("form").reset();
        document.getElementById("form2").reset();

        setReporteAnticipo({
        empresa: 5,
        sucursal: 1,
        folio_anticipo: "",
        fecha: FechaDeHoy(),
        importe: -1,
        nombre_cliente: "",
        unidad: "",
        paquete: "",
        color: "",
        tipo_compra: "",
        nombre_asesor: "",
        agencia: ""
        })
    }

    const selectAgenciaDisabled = () => {
        if (rol === '7') return false;
        return true;
    }

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }
       
    } 

    const resetFormFields = () => {
        setReporteAnticipo({
            Empresa: "",
            Sucursal: "",
            Id_anticipo:"",
            Folio_anticipo: "",
            Fecha: "", 
            Nombre_cliente: "",
            Unidad: "",
            Paquete: "",
            Color: "",
            Tipo_compra: "",
            Nombre_asesor: "",
            Agencia: ""
        })
    }

    const imprimir = () => {
        console.log(reporteAnticipo)
    }

    window.addEventListener('resize', RedimensionarInputs);
    
    return (
        <div style={{ backgroundColor: "#FFFFFF" }}>
            <h5 className='text-center text-dark'>ANTICIPOS</h5>

            <div className='row'>

                <div className={cambiaDimension ? "container col-12 mt-1" : "container col-6 mt-2"}>
                    <form id="form">
                    <label className="bg-secondary input-group-text text-light font-weight-normal">Fecha</label>
                        <input type="text" className="form-control" tabIndex={1} value={reporteAnticipo.Fecha} onChange={onChange} disabled="true" name="Fecha" autoComplete='off' />
                        <br />
                    <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Cliente</label>
                        <input type="text" className="form-control" tabIndex={3} value={reporteAnticipo.Nombre_cliente} onChange={onChange} name="Nombre_cliente" autoComplete='off' />
                        <br />
                    
                    <label className="bg-secondary input-group-text text-light font-weight-normal">Paquete</label>
                        <input type="text" className="form-control" tabIndex={5} value={reporteAnticipo.Paquete} onChange={onChange} name="Paquete" autoComplete='off' />
                        <br />
                   
                    <label className="bg-secondary input-group-text text-light font-weight-normal">Color</label>
                        <input type="text" className="form-control" tabIndex={7} value={reporteAnticipo.Color} onChange={onChange} name="Color" autoComplete='off' />
                        <br />

                    <label className="bg-secondary input-group-text text-light font-weight-normal">Nombre Asesor</label>
                        <input type="text" className="form-control" tabIndex={9} value={reporteAnticipo.Nombre_asesor} onChange={onChange} disabled name="Nombre_asesor" autoComplete='off' />
                        <br />
                   
                    </form>
                </div>

                <div className={cambiaDimension ? "container col-12 mt-4" : "container col-6 mt-2"}>
                    <form id="form2">
                    
                    <label className="bg-secondary input-group-text text-light font-weight-normal">Folio Anticipo</label>
                        <input type="number" min="0" placeholder='#' className="form-control" tabIndex={2} value={reporteAnticipo.Folio_anticipo} onChange={onChange} name="Folio_anticipo" autoComplete='off' />
                        <br />

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Unidad</label>
                        <input type="text" className="form-control" tabIndex={4} value={reporteAnticipo.Unidad} onChange={onChange} name="Unidad" autoComplete='off' />
                        <br />

                        {/* <label className="bg-secondary input-group-text text-dark font-weight-normal">Importe Ingresado</label>
                        <input type="number" data-type="currency" placeholder='$' className="form-control" tabIndex={1} onChange={onChange} name="importe" autoComplete='off' />
                        <br /> */}

                        <label className="bg-secondary input-group-text text-light font-weight-normal">Tipo Compra</label>
                        <input type="text" className="form-control" tabIndex={6} value={reporteAnticipo.Tipo_compra} onChange={onChange} name="Tipo_compra" autoComplete='off' />
                        <br />
                        
                        <label className="bg-secondary input-group-text text-light font-weight-normal">Agencia</label>
                        {/* <input type="text" className="form-control" tabIndex={8} onChange={onChange} name="agencia" value={reporteAnticipo.agencia} autoComplete='off' disabled/> */}
                        {/* <select name='empresa' className='form-select' onChange={cambiarAgencia} disabled={selectAgenciaDisabled()}> */}
                        <select name='Agencia' className='form-select' onChange={onChange} disabled={selectAgenciaDisabled()}>
                            <option value="1-1" selected={reporteAnticipo.Empresa === '1' && reporteAnticipo.Sucursal === '1'}>MOCHIS</option>
                            <option value="5-1" selected={reporteAnticipo.Empresa === '5' && reporteAnticipo.Sucursal === '1'}>CULIACAN ZAP</option>
                            <option value="5-2" selected={reporteAnticipo.Empresa === '5' && reporteAnticipo.Sucursal === '2'}>CULIACAN AERO</option>
                            <option value="3-1" selected={reporteAnticipo.Empresa === '3' && reporteAnticipo.Sucursal === '1'}>GUASAVE</option>
                            <option value="7-1" selected={reporteAnticipo.Empresa === '7' && reporteAnticipo.Sucursal === '1'}>CADILLAC</option>  
                        </select> 
                        <br />
                    </form>
                </div>

            </div>

            <div className='row justify-content-center'>
                <div className='col-auto'>
                <br />
                {/* <button className='btn btn-info text-dark font-italic' type='button' tabIndex={9} onClick={imprimir}> */}
                <button className='btn btn-info text-dark font-italic' type='button' tabIndex={9} onClick={() => {
                    onSubmit(reporteAnticipo)
                    resetFormFields()
                    }}>
                {/* <button className='btn btn-info text-dark font-italic' type='button' tabIndex={9} onClick={() => onSubmit(reporteAnticipo)}> */}
                {/* <button className='btn btn-info text-dark font-italic' type='button' tabIndex={9}> */}
                    Registrar
                </button>
                {/* &nbsp;&nbsp;
                <button className="btn btn-secondary text-dark font-italic" onClick={borrar} type="reset">Limpiar</button> */}
                {/* <button className="btn btn-secondary text-dark font-italic" type="reset">Limpiar</button> */}
               
                </div>
            </div>

        </div>
    )

}