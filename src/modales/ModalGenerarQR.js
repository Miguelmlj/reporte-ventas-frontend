
import React, { useState, useEffect } from 'react'

export const ModalGenerarQR = (props) => {
    /* const [qrs, setQrs] = useState({
        vin_1: '',
        vin_2: '',
        vin_3: '',
        vin_4: '',
        vin_5: '',
        vin_6: '',
        vin_7: '',
        vin_8: '',
        vin_9: '',
        vin_10: ''
    }) */

    const [qrs, setQrs] = useState({
        txtarea: ''
    })

    const onChange = (e) => {
        setQrs({
            ...qrs,
            [e.target.name]: e.target.value.trim()
        })
    }

    const [cambiaDimension, setCambiaDimension] = useState(false)

    const RedimensionarInputs = () => {
        if (window.innerWidth <= 500) {
            setCambiaDimension(true)
        } else {
            setCambiaDimension(false)
        }

    }

    useEffect(() => {
        RedimensionarInputs();
    }, [])

    const borrarTexto = (e) => {
        setQrs({
            ...qrs,
            [e.target.name]: ''
        })
        document.getElementById("form").reset();
    }

    const keyEventPressed = (e) => {
        if (e.key === 'Enter') {
            props.onSubmit(qrs);
            borrarTexto(e);
        }
    }

    window.addEventListener('resize', RedimensionarInputs);

    return (
        <div className='bg-light'>
            <h5 className='text-center text-dark'>VIN</h5>

            <div className='row'>
                <div className='container mt-4 mb-4'>
                    <form id="form">
                        <textarea rows={15} cols={5}
                            className="form-control"
                            placeholder='Favor de escribir cada VIN separado por un espacio.'
                            tabIndex={1}
                            onChange={onChange}
                            name="txtarea"
                            autoComplete='off'
                            onKeyPress={(e) => keyEventPressed(e)}
                        />

                    </form>
                </div>

            </div>

            {/* botones */}
            <div className='row justify-content-center'>
                <div className='col-auto'>
                    <br />
                    <button name="txtarea" className='btn btn-info text-dark mt-5 font-italic'
                        type='button'
                        tabIndex={9}
                        onClick={
                            (e) => {
                                props.onSubmit(qrs);
                                borrarTexto(e);
                            }
                        }>
                        {/* <button className='btn btn-info text-dark mt-5 font-italic' type='button' tabIndex={9} onClick={imprimir}> */}
                        Generar
                    </button>

                </div>
            </div>
        </div>
    )
}
