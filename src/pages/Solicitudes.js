import React, { useEffect, useState } from 'react'
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import { ToastContainer, toast } from 'react-toastify';
import { useModal } from '../hooks/useModal';
import Modal from '../componentes/Modal';
import '../css/Modulos.css'
import { Apiurl } from '../services/Apirest';
import { ModalSolicitudes } from '../modales/ModalSolicitudes';
//Bootstrap and jQuery libraries
import 'bootstrap/dist/css/bootstrap.min.css';
//Datatable Modules
import $ from 'jquery';
import { confDataTable } from '../componentes/ConfDataTable';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

const Solicitudes = () => {
  const [data, setData] = useState([])
  const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
  const [agenciaSolicitudes, setAgenciaSolicitudes] = useState("");
  const [allowRegister, setAllowRegister] = useState(true)
  const [isEditMode, setIsEditMode] = useState(false)
  const [solicitudes, setSolicitudes] = useState({
    id_fecha: "",
    //solicitudes nuevos
    sol_gmf_n: "",
    sol_bancos_n: "",
    sol_aprobaciones_gmf_n: "",
    sol_contratos_comprados_n: "",
    sol_contratos_comprados_leasing: "",
    //solicitudes seminuevos
    sol_gmf_s: "",
    sol_bancos_s: "",
    sol_aprobaciones_gmf_s: "",
    sol_contratos_comprados_s: ""
  })
  const responsable = window.sessionStorage.getItem('responsable');
  let objusuario = {
    user: window.sessionStorage.getItem('usuario'),
    empresa: window.sessionStorage.getItem('empresa'),
    sucursal: window.sessionStorage.getItem('sucursal')
  }

  useEffect(() => {
    peticionGetSolicitudesDiarias();
    peticionNombreAgencia();
    //3.- usuario BDC puede acceder a módulo solicitudes pero no puede registar ni editar.
    if (responsable === '3') setAllowRegister(false)

  }, [])


  const peticionGetSolicitudesDiarias = async () => {
    const url1 = Apiurl + "api/solicitudes/"
    await axios.post(url1, objusuario)
      .then(response => {
        dataTableDestroy()
        setData(response['data']);
        dataTable()

      })
      .catch(error => {
        console.log(error);
      })
  }

  const dataTable = () => {
    $(document).ready(function () {
      $('#solicitudes').DataTable(
        confDataTable
      );
    });
  }

  const dataTableDestroy = () => {
    $('#solicitudes').DataTable().destroy();
  }

  const peticionNombreAgencia = async () => {
    let urlAgencia = Apiurl + "api/inicio/agencia";
    await axios.post(urlAgencia, objusuario)
      .then(response => {
        // console.log(response['data'].Nombre)
        setAgenciaSolicitudes(response['data'].Nombre)

      })
      .catch(error => {
        toast.error("Error al obtener nombre de agencia");
      })

  }

  const nuevoReporteDiarioSolicitudes = async (newRep) => {
    // validar campos formulario nuevos

    if (newRep.id_fecha === "") {
      toast.info('Campo fecha vacío')
      setValuesWrittenForm(newRep);
      return

    }
    /* if (isNaN(newRep.sol_gmf_n)) {
      toast.info('Digite un número en campo solicitudes GMF nuevo')
      return

    }
    if (isNaN(newRep.sol_bancos_n)) {
      toast.info('Digite un número en campo solicitudes bancos nuevo')
      return

    }
    if (isNaN(newRep.sol_aprobaciones_gmf_n)) {
      toast.info('Digite un número en campo Aprobaciones GMF nuevo')
      return

    }
    if (isNaN(newRep.sol_contratos_comprados_n)) {
      toast.info('Digite un número en campo contratos comprados nuevo')
      return

    }

    // validar campos formulario seminuevos
    if (isNaN(newRep.sol_gmf_s)) {
      toast.info('Digite un número en campo solicitudes GMF seminuevo')
      return

    }
    if (isNaN(newRep.sol_bancos_s)) {
      toast.info('Digite un número en campo solicitudes bancos seminuevo')
      return

    }
    if (isNaN(newRep.sol_aprobaciones_gmf_s)) {
      toast.info('Digite un número en campo Aprobaciones GMF seminuevo')
      return

    }
    if (isNaN(newRep.sol_contratos_comprados_s)) {
      toast.info('Digite un número en campo contratos comprados seminuevo')
      return

    } */

    if (
      newRep.sol_gmf_n.toString().substring(0, 1) === "-" ||
      newRep.sol_bancos_n.toString().substring(0, 1) === "-" ||
      newRep.sol_aprobaciones_gmf_n.toString().substring(0, 1) === "-" ||
      newRep.sol_contratos_comprados_n.toString().substring(0, 1) === "-" ||
      newRep.sol_contratos_comprados_leasing.toString().substring(0, 1) === "-" ||
      newRep.sol_gmf_s.toString().substring(0, 1) === "-" ||
      newRep.sol_bancos_s.toString().substring(0, 1) === "-" ||
      newRep.sol_aprobaciones_gmf_s.toString().substring(0, 1) === "-" ||
      newRep.sol_contratos_comprados_s.toString().substring(0, 1) === "-"
    ) {
      toast.info("No se permiten números negativos, intentar de nuevo.");
      setValuesWrittenForm(newRep);
      return;
    }

    if (newRep.sol_gmf_n === "") newRep.sol_gmf_n = 0;
    if (newRep.sol_bancos_n === "") newRep.sol_bancos_n = 0;
    if (newRep.sol_aprobaciones_gmf_n === "") newRep.sol_aprobaciones_gmf_n = 0;
    if (newRep.sol_contratos_comprados_n === "") newRep.sol_contratos_comprados_n = 0;
    if (newRep.sol_contratos_comprados_leasing === "") newRep.sol_contratos_comprados_leasing = 0;
    if (newRep.sol_gmf_s === "") newRep.sol_gmf_s = 0;
    if (newRep.sol_bancos_s === "") newRep.sol_bancos_s = 0;
    if (newRep.sol_aprobaciones_gmf_s === "") newRep.sol_aprobaciones_gmf_s = 0;
    if (newRep.sol_contratos_comprados_s === "") newRep.sol_contratos_comprados_s = 0;



    //agregar el usuario dependiendo la agencia ...
    let newRepClone = {
      ...newRep,
      user: objusuario.user,
      empresa: objusuario.empresa,
      sucursal: objusuario.sucursal
    }

    if (isEditMode) {
      actualizarValoresSolicitudes(newRepClone);
      return;
    }

    isRegisterAlreadyCreated(newRepClone);

  }

  const actualizarValoresSolicitudes = async (newRep) => {

    let url3 = Apiurl + "api/solicitudesupdate/";

    //peticion patch
    axios.patch(url3, newRep)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          closeModalReporte();
          peticionGetSolicitudesDiarias();
          toast.success('Reporte diario - Solicitudes se ha registrado correctamente')
        }
      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })

  }

  const registrarValoresSolicitudes = async (newRep) => {
    //si los otros modulos no han registrado su reporte diario, se ejecutará 
    //esta función para crear el reporte.

    //ruta para crear
    let url4 = Apiurl + "api/solicitudescreate/";

    axios.post(url4, newRep)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          closeModalReporte();
          peticionGetSolicitudesDiarias();
          toast.success('Reporte diario - Solicitudes se ha registrado correctamente')
        }
      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })

  }

  const editSolicitudes = (RequestObject) => {
    setIsEditMode(true);
    setSolicitudes({
      id_fecha: RequestObject.id_fecha.substring(0, 10),
      //solicitudes nuevos
      sol_gmf_n: RequestObject.sol_gmf_n,
      sol_bancos_n: RequestObject.sol_bancos_n,
      sol_aprobaciones_gmf_n: RequestObject.sol_aprobaciones_gmf_n,
      sol_contratos_comprados_n: RequestObject.sol_contratos_comprados_n,
      sol_contratos_comprados_leasing: RequestObject.sol_contratos_comprados_leasing,
      //solicitudes seminuevos
      sol_gmf_s: RequestObject.sol_gmf_s,
      sol_bancos_s: RequestObject.sol_bancos_s,
      sol_aprobaciones_gmf_s: RequestObject.sol_aprobaciones_gmf_s,
      sol_contratos_comprados_s: RequestObject.sol_contratos_comprados_s
    })
    openModalReporte();
  }

  const createSolicitudes = () => {
    setIsEditMode(false);

    setSolicitudes({
      id_fecha: FechaDeHoy(),
      //solicitudes nuevos
      sol_gmf_n: "",
      sol_bancos_n: "",
      sol_aprobaciones_gmf_n: "",
      sol_contratos_comprados_n: "",
      sol_contratos_comprados_leasing: "",
      //solicitudes seminuevos
      sol_gmf_s: "",
      sol_bancos_s: "",
      sol_aprobaciones_gmf_s: "",
      sol_contratos_comprados_s: ""
    })

    openModalReporte();
  }

  const setValuesWrittenForm = (RequestObject) => {
    isEditMode
      ?
      setSolicitudes({
        id_fecha: solicitudes.id_fecha,
        sol_gmf_n: solicitudes.sol_gmf_n,
        sol_bancos_n: solicitudes.sol_bancos_n,
        sol_aprobaciones_gmf_n: solicitudes.sol_aprobaciones_gmf_n,
        sol_contratos_comprados_n: solicitudes.sol_contratos_comprados_n,
        sol_contratos_comprados_leasing: solicitudes.sol_contratos_comprados_leasing,
        sol_gmf_s: solicitudes.sol_gmf_s,
        sol_bancos_s: solicitudes.sol_bancos_s,
        sol_aprobaciones_gmf_s: solicitudes.sol_aprobaciones_gmf_s,
        sol_contratos_comprados_s: solicitudes.sol_contratos_comprados_s
      })
      :
      setSolicitudes({
        id_fecha: RequestObject.id_fecha,
        sol_gmf_n: RequestObject.sol_gmf_n,
        sol_bancos_n: RequestObject.sol_bancos_n,
        sol_aprobaciones_gmf_n: RequestObject.sol_aprobaciones_gmf_n,
        sol_contratos_comprados_n: RequestObject.sol_contratos_comprados_n,
        sol_contratos_comprados_leasing: RequestObject.sol_contratos_comprados_leasing,
        sol_gmf_s: RequestObject.sol_gmf_s,
        sol_bancos_s: RequestObject.sol_bancos_s,
        sol_aprobaciones_gmf_s: RequestObject.sol_aprobaciones_gmf_s,
        sol_contratos_comprados_s: RequestObject.sol_contratos_comprados_s
      })
  }

  const isRegisterAlreadyCreated = async (newRepClone) => {
    let url2 = Apiurl + "api/solicitudesbyfecha/";

    axios.post(url2, newRepClone)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          if (response['data'].message === 'registrocreado') {
            //actualizar - patch
            actualizarValoresSolicitudes(newRepClone);

          } else if (response['data'].message === 'registronocreado') {
            registrarValoresSolicitudes(newRepClone);

          }
        }
      })
      .catch(error => {
        toast.error('Error al conectarse con el servidor')
      })
  }

  return (

    <div className='content-wrapper'>

      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-12">

              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className="m-0 text-dark">{agenciaSolicitudes}</h5>
                </div>
              </div>

            </div>
            <h6 className="mb-3 mt-4"> NUEVOS | SEMINUEVOS</h6>
          </div>
          <button type='button' onClick={createSolicitudes} className="btn btn-outline-info" disabled={!allowRegister}>Registrar Solicitudes</button>
        </div>
      </div>

      <div className="container-fluid">
        <div className="row">
          <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
            <ModalSolicitudes
              data={solicitudes}
              onSubmit={nuevoReporteDiarioSolicitudes}
              editMode={isEditMode}
            />
          </Modal>

          <div className='col-md-12 col-xs-5 col-sm-12 table-responsive mb-4'>
            <table id="solicitudes" className="table table-bordered table-striped">
              <thead>
                <tr>

                  {/* nuevos */}
                  <th className='text-center'>Fecha</th>
                  <th className='text-center'>GMF-N</th>
                  <th className='text-center'>Bancos-N</th>
                  <th className='text-center'>Aprob-N</th>
                  {/* <th>A GMF-N</th> */}
                  <th className='text-center'>Contratos C-N</th>
                  <th className='text-center'>Leasing</th>

                  {/* seminuev os */}
                  <th className='text-center'>GMF-S</th>
                  <th className='text-center'>Bancos-S</th>
                  <th className='text-center'>Aprob-S</th>
                  {/* <th>A GMF-S</th> */}
                  <th className='text-center'>Contratos C-S</th>
                  <th className='text-center'>Editar</th>
                </tr>
              </thead>
              {/* <tfoot>
                <tr>
                  <td colspan="9">{`Mostrando ${registrosPorPagina} de ${totalRegistrosFooter} registros`}</td>

                </tr>
              </tfoot> */}
              <tbody id="table_body_solicitudes">
                {data.length > 0 ? data.map((reporte) => {
                  return (
                    <tr>
                      {/* nuevos */}
                      <td className='text-center'>{reverseDate(reporte.id_fecha.substring(0, 10))}</td>
                      <td className='text-center'>{reporte.sol_gmf_n}</td>
                      <td className='text-center'>{reporte.sol_bancos_n}</td>
                      <td className='text-center'>{reporte.sol_aprobaciones_gmf_n}</td>
                      <td className='text-center'>{reporte.sol_contratos_comprados_n}</td>
                      <td className='text-center'>{reporte.sol_contratos_comprados_leasing}</td>

                      {/* seminuevos */}
                      <td className='text-center'>{reporte.sol_gmf_s}</td>
                      <td className='text-center'>{reporte.sol_bancos_s}</td>
                      <td className='text-center'>{reporte.sol_aprobaciones_gmf_s}</td>
                      <td className='text-center'>{reporte.sol_contratos_comprados_s}</td>
                      <td className='text-center'>{
                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editSolicitudes(reporte)}><FontAwesomeIcon icon={faEdit} /></button>
                      }</td>
                    </tr>
                  )
                }) : 'No hay registros'}

              </tbody>
            </table>

          </div>

        </div>
      </div>



      <ToastContainer />
    </div>
  )
}

export default Solicitudes;
