import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';
import { Apiurl } from '../services/Apirest';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';

import { IconContext } from 'react-icons/lib'
// import { FcBarChart, FcInspection, FcTodoList } from "react-icons/fc";
// import '../css/Inicio.css'

function Inicio() {
  const [usuario, setUsuario] = useState("");
  const [data, setData] = useState([])
  const [dataMes, setDataMes] = useState([])
  const [agencia, setAgencia] = useState("");

  // total acumulados nuevos
  const [totalAcumuladoMesAfluenciaNuevos, setTotalAcumuladoMesAfluenciaNuevos] = useState(-1)
  const [totalAcumuladoMesCitasNuevos, setTotalAcumuladoMesCitasNuevos] = useState(-1)
  const [totalAcumuladoMesSolicitudesNuevos, setTotalAcumuladoMesSolicitudesNuevos] = useState(-1)

  // total acumulados seminuevos
  const [totalAcumuladoMesAfluenciaSeminuevos, setTotalAcumuladoMesAfluenciaSeminuevos] = useState(-1)
  const [totalAcumuladoMesSolicitudesSeminuevos, setTotalAcumuladoMesSolicitudesSeminuevos] = useState(-1)

  //total reporte diario nuevos
  const [totalHoyAfluenciaNuevos, setTotalHoyAfluenciaNuevos] = useState(-1)
  const [totalHoyCitasNuevos, setTotalHoyCitasNuevos] = useState(-1)
  const [totalHoySolicitudesNuevos, setTotalHoySolicitudesNuevos] = useState(-1)

  //total reporte diario nuevos
  const [totalHoyAfluenciaSeminuevos, setTotalHoyAfluenciaSeminuevos] = useState(-1)
  const [totalHoySolicitudesSeminuevos, setTotalHoySolicitudesSeminuevos] = useState(-1)

  let objusuario = {
    user: window.sessionStorage.getItem('usuario'),
    empresa: window.sessionStorage.getItem('empresa'),
    sucursal: window.sessionStorage.getItem('sucursal')
  }

  useEffect(() => {
    setUsuario(window.sessionStorage.getItem('usuario'))
    peticionReporteDeHoy()
    peticionAcumuladoDelMes()
    peticionNombreAgencia()

  }, [])

  //se convertirá en post en lugar de get, para enviar el usuario y extraer en la base de datos correspondiente
  let url = Apiurl + "api/inicio/";
  const peticionReporteDeHoy = async () => {

    await axios.post(url, objusuario)
      .then(response => {
        setData(response['data']);

        //SUMA HOY ALFUENCIA NUEVOS
        if (response['data'].length > 0) {

          response['data'][0].af_fresh_n === null
            ? setTotalHoyAfluenciaNuevos(0)
            :  setTotalHoyAfluenciaNuevos(response['data'][0].af_fresh_n + response['data'][0].af_bback_n + response['data'][0].af_primera_cita_n + response['data'][0].af_digitales_n)

        } else {
          setTotalHoyAfluenciaNuevos(0)

        }

        //SUMA HOY CITAS NUEVOS
        if (response['data'].length > 0) {
          response['data'][0].cit_agendadas_n === null ?
            setTotalHoyCitasNuevos(0) :
            setTotalHoyCitasNuevos(response['data'][0].cit_agendadas_n + response['data'][0].cit_cumplidas_n)

        } else {
          setTotalHoyCitasNuevos(0)
        }
        
        //SUMA HOY SOLICITUDES NUEVOS
        if(response['data'].length > 0){
          response['data'][0].sol_gmf_n === null ?
          setTotalHoySolicitudesNuevos(0) :
          setTotalHoySolicitudesNuevos(response['data'][0].sol_gmf_n + response['data'][0].sol_bancos_n)
        } else {
          setTotalHoySolicitudesNuevos(0)
        }

        //SUMA HOY ALFUENCIA SEMINUEVOS
        if(response['data'].length > 0){
          response['data'][0].af_fresh_s === null ?
          setTotalHoyAfluenciaSeminuevos(0) :
          setTotalHoyAfluenciaSeminuevos(response['data'][0].af_fresh_s + response['data'][0].af_bback_s + response['data'][0].af_primera_cita_s)
          
          // setTotalHoyAfluenciaSeminuevos
        }else{
          setTotalHoyAfluenciaSeminuevos(0)
        }
        
        //SUMA HOY SOLICITUDES SEMINUEVOS
        if(response['data'].length > 0){
          response['data'][0].sol_gmf_s === null ?
          setTotalHoySolicitudesSeminuevos(0) :
          setTotalHoySolicitudesSeminuevos(response['data'][0].sol_gmf_s + response['data'][0].sol_aprobaciones_gmf_s + response['data'][0].sol_contratos_comprados_s)
        }else{
          setTotalHoySolicitudesSeminuevos(0)
        }

      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })

  }

  let urlAgencia = Apiurl + "api/inicio/agencia";
  const peticionNombreAgencia = async () => {
    await axios.post(urlAgencia, objusuario)
      .then(response => {
        setAgencia(response['data'].Nombre)

      })
      .catch(error => {
        toast.error("Error al obtener nombre de agencia");
      })

  }

  //registros del mes para obtener acumulado
  let urlAcumulado = Apiurl + "api/acumulado/";
  const peticionAcumuladoDelMes = async () => {
    await axios.post(urlAcumulado, objusuario)
      .then(response => {
        setDataMes(response['data']);

        //SUMAS DE TOTALES DEL MES.

        //SUMA MES ALFUENCIA NUEVOS
        setTotalAcumuladoMesAfluenciaNuevos(
          response['data'].af_fresh_n +
          response['data'].af_bback_n +
          response['data'].af_primera_cita_n +
          response['data'].af_digitales_n
        )

        //SUMA MES CITAS NUEVOS
        setTotalAcumuladoMesCitasNuevos(
          response['data'].cit_agendadas_n +
          response['data'].cit_cumplidas_n
        )

        //SUMA MES SOLICITUDES NUEVOS
        setTotalAcumuladoMesSolicitudesNuevos(
          response['data'].sol_gmf_n +
          response['data'].sol_bancos_n
        )

        //SUMA MES AFLUENCIA SEMINUEVOS
        setTotalAcumuladoMesAfluenciaSeminuevos(
          response['data'].af_fresh_s +
          response['data'].af_bback_s +
          response['data'].af_primera_cita_s
        )

        //SUMA MES SOLICITUDES SEMINUEVOS
        setTotalAcumuladoMesSolicitudesSeminuevos(
          response['data'].sol_gmf_s +
          response['data'].sol_aprobaciones_gmf_s +
          response['data'].sol_contratos_comprados_s
        )

      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })
  }

  return (
    //VALIDAR SI NO HAY DATA PARA MOSTRAR MENSAJE DE AUN NO CAPTURADO EN LOS CAMPOS CON CONDICION
    <IconContext.Provider value={{ color: '#000000', size: 60 }}>
      <div className='content-wrapper '>

        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-12">
                <div className='card card-outline card-primary'>
                  <div className='card-header'>

                   <h5 className="m-0 text-dark">{agencia}</h5>
                
                  </div>

                </div>
              </div>
              <h6 className="mb-2 ml-2 mt-4">VENTAS DIARIAS | ACUMULADOS DEL MES</h6>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <div className="row">

            {/* alfuencia nuevos*/}
            <div className="col-12 col-sm-4 col-md-4">

              <div className="card card-info direct-chat direct-chat-info">
                <div className="card-header">
                  <i className="nav-icon fas fa-car mr-2" /><span className="info-box-text">VEHÍCULOS NUEVOS</span>
                </div>
                <table className="table">
                  <thead>
                    <tr style={{ fontSize: 12 }}>
                      <th>AFLUENCIA</th>
                      <th>HOY</th>
                      <th style={{ textAlign: 'right' }}>ACUM</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* HOY                                                                                                                                             ACUMULADO */}
                    <tr style={{ fontSize: 12, backgroundColor: "#D3D3D3" }}><th>TOTAL</th><th>{totalHoyAfluenciaNuevos}</th><th style={{ textAlign: 'right' }}>{totalAcumuladoMesAfluenciaNuevos}</th></tr>
                    
                    <tr style={{ fontSize: 12 }}><td>Fresh</td><td>{data.length > 0 ? `${data[0].af_fresh_n === null ? 'vacío' : data[0].af_fresh_n}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.af_fresh_n}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Bback</td><td>{data.length > 0 ? `${data[0].af_bback_n === null ? 'vacío' : data[0].af_bback_n}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.af_bback_n}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Primera Cita</td><td>{data.length > 0 ? `${data[0].af_primera_cita_n === null ? 'vacío' : data[0].af_primera_cita_n}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.af_primera_cita_n}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Digital</td><td>{data.length > 0 ? `${data[0].af_digitales_n === null ? 'vacío' : data[0].af_digitales_n}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.af_digitales_n}</td></tr>

                  </tbody>
                </table>
              </div>

            </div>

            {/* citas nuevos*/}
            <div className="col-12 col-sm-4 col-md-4">

              <div className="card card-info direct-chat direct-chat-info">
                <div className="card-header">
                  <i className="nav-icon fas fa-car mr-2" /><span className="info-box-text">VEHÍCULOS NUEVOS</span>
                </div>
                <table className="table">
                  <thead>
                    <tr style={{ fontSize: 12 }}>
                      <th>CITAS</th>
                      <th>HOY</th>
                      <th style={{ textAlign: 'right' }}>ACUM</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* HOY                                                                                                                                             ACUMULADO */}
                    <tr style={{ fontSize: 12, backgroundColor: "#D3D3D3" }}><th>TOTAL</th><th>{totalHoyCitasNuevos}</th><th style={{ textAlign: 'right' }}>{totalAcumuladoMesCitasNuevos}</th></tr>
                   
                    <tr style={{ fontSize: 12 }}><td>Agendadas</td><td>{data.length > 0 ? `${data[0].cit_agendadas_n === null ? 'vacío' : data[0].cit_agendadas_n}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.cit_agendadas_n}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Cumplidas</td><td>{data.length > 0 ? `${data[0].cit_cumplidas_n === null ? 'vacío' : data[0].cit_cumplidas_n}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.cit_cumplidas_n}</td></tr>

                  </tbody>
                </table>
              </div>

            </div>

            {/* solicitudes nuevos*/}
            <div className="col-12 col-sm-4 col-md-4">

              <div className="card card-info direct-chat direct-chat-info">
                <div className="card-header">
                  <i className="nav-icon fas fa-car mr-2" /><span className="info-box-text">VEHÍCULOS NUEVOS</span>
                </div>
                <table className="table">
                  <thead>
                    <tr style={{ fontSize: 12 }}>
                      <th>SOLICITUDES</th>
                      <th>HOY</th>
                      <th style={{ textAlign: 'right' }}>ACUM</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* HOY                                                                                                                                             ACUMULADO */}
                    <tr style={{ fontSize: 12, backgroundColor: "#D3D3D3" }}><th>TOTAL</th><th>{totalHoySolicitudesNuevos}</th><th style={{ textAlign: 'right' }}>{totalAcumuladoMesSolicitudesNuevos}</th></tr>
                   
                    <tr style={{ fontSize: 12 }}><td>GMF</td><td>{data.length > 0 ? `${data[0].sol_gmf_n === null ? 'vacío' : data[0].sol_gmf_n}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.sol_gmf_n}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Bancos</td><td>{data.length > 0 ? `${data[0].sol_bancos_n === null ? 'vacío' : data[0].sol_bancos_n}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.sol_bancos_n}</td></tr>

                  </tbody>
                </table>
              </div>

            </div>

          </div>

          <div className="row">

            {/* afluencia seminuevos */}
            <div className="col-12 col-sm-4 col-md-4">

              <div className="card card-warning direct-chat direct-chat-warning">
                <div className="card-header">
                  <i className="nav-icon fas fa-car mr-2" /><span className="warning-box-text">VEHÍCULOS SEMINUEVOS</span>
                </div>
                <table className="table">
                  <thead>
                    <tr style={{ fontSize: 12 }}>
                      <th>AFLUENCIA</th>
                      <th>HOY</th>
                      <th style={{ textAlign: 'right' }}>ACUM</th>
                    </tr>
                  </thead>
                  <tbody>

                    {/* HOY                                                                                                                                             ACUMULADO */}
                    <tr style={{ fontSize: 12, backgroundColor: "#D3D3D3" }}><th>TOTAL</th><th>{totalHoyAfluenciaSeminuevos}</th><th style={{ textAlign: 'right' }}>{totalAcumuladoMesAfluenciaSeminuevos}</th></tr>
                   
                    <tr style={{ fontSize: 12 }}><td>Fresh</td><td>{data.length > 0 ? `${data[0].af_fresh_s === null ? 'vacío' : data[0].af_fresh_s}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.af_fresh_s}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Bback</td><td>{data.length > 0 ? `${data[0].af_bback_s === null ? 'vacío' : data[0].af_bback_s}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.af_bback_s}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Primera Cita</td><td>{data.length > 0 ? `${data[0].af_primera_cita_s === null ? 'vacío' : data[0].af_primera_cita_s}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.af_primera_cita_s}</td></tr>

                  </tbody>
                </table>
              </div>

            </div>
            {/* afluencia seminuevos */}

            {/* solicitudes seminuevos */}

            <div className="col-12 col-sm-4 col-md-4">

              <div className="card card-warning direct-chat direct-chat-warning">
                <div className="card-header">
                  <i className="nav-icon fas fa-car mr-2" /><span className="warning-box-text">VEHÍCULOS SEMINUEVOS</span>
                </div>
                <table className="table">
                  <thead>
                    <tr style={{ fontSize: 12 }}>
                      <th>SOLICITUDES</th>
                      <th>HOY</th>
                      <th style={{ textAlign: 'right' }}>ACUM</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* HOY                                                                                                                                             ACUMULADO */}
                    <tr style={{ fontSize: 12,  backgroundColor: "#D3D3D3" }}><th>TOTAL</th><th>{totalHoySolicitudesSeminuevos}</th><th style={{ textAlign: 'right' }}>{totalAcumuladoMesSolicitudesSeminuevos}</th></tr>
                    
                    <tr style={{ fontSize: 12 }}><td>Solicitudes</td><td>{data.length > 0 ? `${data[0].sol_gmf_s === null ? 'vacío' : data[0].sol_gmf_s}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.sol_gmf_s}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Aprobaciones</td><td>{data.length > 0 ? `${data[0].sol_aprobaciones_gmf_s === null ? 'vacío' : data[0].sol_aprobaciones_gmf_s}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.sol_aprobaciones_gmf_s}</td></tr>

                    <tr style={{ fontSize: 12 }}><td>Contratos comprados</td><td>{data.length > 0 ? `${data[0].sol_contratos_comprados_s === null ? 'vacío' : data[0].sol_contratos_comprados_s}` : `NC`}</td><td style={{ textAlign: 'right' }}>{dataMes.sol_contratos_comprados_s}</td></tr>

                  </tbody>
                </table>
              </div>

            </div>
            {/* solicitudes seminuevos */}

          </div>

        </div>

        <ToastContainer />
      </div>
    </IconContext.Provider>


  )
}

export default Inicio;
