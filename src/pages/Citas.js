import React, { useEffect, useState } from 'react'
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import { ToastContainer, toast } from 'react-toastify';
import 'bootstrap/dist/css/bootstrap.min.css';
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery';
import { confDataTable } from '../componentes/ConfDataTable';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import '../css/Modulos.css'
import { Apiurl } from '../services/Apirest';
import { useModal } from '../hooks/useModal';
import Modal from '../componentes/Modal';
import { ModalCitas } from '../modales/ModalCitas';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

const Citas = () => {
  const [data, setData] = useState([])
  const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
  const [agenciaCitas, setAgenciaCitas] = useState("");
  const [isEditMode, setIsEditMode] = useState(false)
  const [citas, setCitas] = useState({
    id_fecha: "",
    cit_agendadas_n: "",
    cit_cumplidas_n: ""
    /* cit_primera_cita_n: 0,
        cit_digitales_n: 0 */
  })
  let objusuario = {
    user: window.sessionStorage.getItem('usuario'),
    empresa: window.sessionStorage.getItem('empresa'),
    sucursal: window.sessionStorage.getItem('sucursal')
  }

  useEffect(() => {
    peticionGetCitasDiarias();
    peticionNombreAgencia()
  }, [])

  
  const peticionGetCitasDiarias = async () => {
    const url1 = Apiurl + "api/citas/"
    await axios.post(url1, objusuario)
      .then(response => {
        dataTableDestroy()
        setData(response['data']);
        dataTable()

      })
      .catch(error => {
        console.log(error);
      })
  }

  const peticionNombreAgencia = async () => {
    let urlAgencia = Apiurl + "api/inicio/agencia";

    await axios.post(urlAgencia, objusuario)
      .then(response => {
        setAgenciaCitas(response['data'].Nombre)

      })
      .catch(error => {
        toast.error("Error al obtener nombre de agencia");
      })

  }

  const nuevoReporteDiarioCitas = async (newRep) => {

    if (newRep.id_fecha === "") {
      toast.info('Campo fecha vacío')
      setValuesWrittenForm(newRep);
      return

    } 

    if(
      newRep.cit_agendadas_n.toString().substring(0,1) === "-" ||
      newRep.cit_cumplidas_n.toString().substring(0,1) === "-"
    ) {
      toast.info("No se permiten números negativos, intentar de nuevo.");
      setValuesWrittenForm(newRep);
      return;
    }

    if ( newRep.cit_agendadas_n === "" ) newRep.cit_agendadas_n = 0;
    if ( newRep.cit_cumplidas_n === "" ) newRep.cit_cumplidas_n = 0;

    //agregar el usuario dependiendo la agencia ...
    let newRepClone = {
      ...newRep,
      user: objusuario.user,
      empresa: objusuario.empresa,
      sucursal: objusuario.sucursal
    }

    if ( isEditMode ) {
      actualizarValoresCitas(newRepClone);
      return;
    }

    isRegisterAlreadyCreated(newRepClone)
    

  }

  const isRegisterAlreadyCreated = async (newRepClone) => {
    let url2 = Apiurl + "api/citasbyfecha/";

    axios.post(url2, newRepClone)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          if (response['data'].message === 'registrocreado') {
            //actualizar - patch
            actualizarValoresCitas(newRepClone);

          } else if (response['data'].message === 'registronocreado') {
            //crear - primeros valores del reporte del día serán afluencia
            registrarValoresCitas(newRepClone);
          }

        }
      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })
  }

  const actualizarValoresCitas = async (newRep) => {
    let url3 = Apiurl + "api/citasupdate/";

    //peticion patch
    axios.patch(url3, newRep)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          closeModalReporte();
          peticionGetCitasDiarias();
          toast.success('Reporte diario - Citas se ha registrado correctamente')
        }
      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })
  }

  const registrarValoresCitas = async (newRep) => {
    //ruta para crear
    let url4 = Apiurl + "api/citascreate/";

    axios.post(url4, newRep)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          closeModalReporte();
          peticionGetCitasDiarias();
          toast.success('Reporte diario - Afluencia se ha registrado correctamente')
        }
      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })
  }

  const createCitas = () => {
    setIsEditMode(false);

    setCitas({
      id_fecha: FechaDeHoy(),
      cit_agendadas_n: "",
      cit_cumplidas_n: ""
    })

    openModalReporte();
  }

  const editCitas = (CITObject) => {
    setIsEditMode(true);
    setCitas({
      id_fecha: CITObject.id_fecha.substring(0, 10),
      cit_agendadas_n: CITObject.cit_agendadas_n,
      cit_cumplidas_n: CITObject.cit_cumplidas_n,
      id_reporte: CITObject.id_reporte
    })
    openModalReporte();
  }

  const setValuesWrittenForm = (CITObject) => {
    isEditMode
      ?
      setCitas({
        id_fecha: citas.id_fecha,
        cit_agendadas_n: citas.cit_agendadas_n,
        cit_cumplidas_n: citas.cit_cumplidas_n,
        id_reporte: citas.id_reporte
      })
      :
      setCitas({
        id_fecha: CITObject.id_fecha,
        cit_agendadas_n: CITObject.cit_agendadas_n,
        cit_cumplidas_n: CITObject.cit_cumplidas_n
      })
  }

  const dataTable = () => {
    $(document).ready(function () {
      $('#citas').DataTable(
        confDataTable
      );
    });
  }

  const dataTableDestroy = () => {
    $('#citas').DataTable().destroy();
  }


  return (
    <div className='content-wrapper'>
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-12">

              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className="m-0 text-dark">{agenciaCitas}</h5>

                </div>
              </div>

            </div>
            <h6 className="mb-3 mt-4"> NUEVOS</h6>
          </div>
          <button type='button' onClick={createCitas} className="btn btn-outline-info">Registrar Citas</button>
        </div>
      </div>


      <div className="container-fluid">
        <div className="row">
          <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
            <ModalCitas
              data={citas}
              onSubmit={nuevoReporteDiarioCitas}
              editMode={isEditMode}
            />
          </Modal>

          <div className='col-md-12 col-xs-5 col-sm-12 table-responsive mb-4'>
            <table id="citas" className="table table-bordered table-striped">
              <thead>
                <tr>
                  <th className='text-center'>Fecha</th>
                  <th className='text-center'>Agendadas</th>
                  <th className='text-center'>Cumplidas</th>
                  <th className='text-center'>Editar</th>
                  {/* <th>1 Cita</th>
                  <th>Digitales</th> */}
                </tr>

              </thead>
              {/* <tfoot>
                <tr>
                  <td colspan="3">{`Mostrando ${registrosPorPagina} de ${totalRegistrosFooter} registros`}</td>

                </tr>
              </tfoot> */}
              <tbody id='table_body_citas'>
                {data.length > 0 ? data.map((reporte) => {
                  return (
                    <tr>
                      <td className='text-center'>{reverseDate(reporte.id_fecha.substring(0, 10))}</td>
                      <td className='text-center'>{reporte.cit_agendadas_n}</td>
                      <td className='text-center'>{reporte.cit_cumplidas_n}</td>
                      <td className='text-center'>{
                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editCitas(reporte)}><FontAwesomeIcon icon={faEdit} /></button>
                      }</td>
                      {/* <td>{reporte.cit_primera_cita_n}</td>

                    <td>{reporte.cit_digitales_n}</td> */}
                    </tr>
                  )
                }) : 'No hay registros'}
              </tbody>

            </table>
          </div>

        </div>
      </div>


      <ToastContainer />
    </div>
  )
}

export default Citas;
