import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalCtrlOper_GAP } from '../modales/ModalCtrlOper_GAP';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';
import { getAsesores } from '../helpers/getAsesores';
import { activos } from '../constantes/asesores';

const CtrlOper_GAP = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [isEditMode, setIsEditMode] = useState(false)
    const [GAP, setGAP] = useState({
        Fecha: "",
        Importe_GAP: "",
        Asesor: "",
        Nombre_Cliente: "",
        Utilidad: "",
        Comision_Asesor: "",
        Utilidad_Neta: ""

    })
    const [data, setData] = useState([])
    const [asesores, setAsesores] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    useEffect(async () => {
        getGAP();
        setAsesores(await getAsesores(activos))
    }, [])

    const getGAP = async () => {
        url = Apiurl + "api/ctrloperGAP/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createGAP = () => {
        setIsEditMode(false);
        setGAP({
            Fecha: FechaDeHoy(),
            Importe_GAP: "",
            Asesor: asesores.length > 0 ? asesores[0].Nombre_Asesor : "",
            Nombre_Cliente: "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })

        openModalReporte();
    }

    const nuevoRegistroGAP = (gapObject) => {

        if (gapObject.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(gapObject);
            return;
        }

        if (gapObject.Asesor === "") {
            toast.info("El campo Nombre Asesor se encuentra vacío.");
            setValuesWrittenForm(gapObject);
            return;
        }

        if (gapObject.Nombre_Cliente === "") {
            toast.info("El campo Nombre Cliente se encuentra vacío.");
            setValuesWrittenForm(gapObject);
            return;
        }

        if (
            (gapObject.Importe_GAP.toString().substring(0, 1) === "-") ||
            (gapObject.Comision_Asesor.toString().substring(0, 1) === "-") ||
            (gapObject.Utilidad_Neta.toString().substring(0, 1) === "-") ||
            (gapObject.Utilidad.toString().substring(0, 1) === "-")
          ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(gapObject);
            return;
          }

        if (gapObject.Importe_GAP === "") gapObject.Importe_GAP = 0;
        else gapObject.Importe_GAP = removerComas(gapObject.Importe_GAP)

        if (gapObject.Comision_Asesor === "") gapObject.Comision_Asesor = 0;
        else gapObject.Comision_Asesor = removerComas(gapObject.Comision_Asesor)

        if (gapObject.Utilidad_Neta === "") gapObject.Utilidad_Neta = 0;
        else gapObject.Utilidad_Neta = removerComas(gapObject.Utilidad_Neta)

        if (gapObject.Utilidad === "") gapObject.Utilidad = 0;
        else gapObject.Utilidad = removerComas(gapObject.Utilidad)

          
        let body = {
        GAP: gapObject,
        agencia
        }

        if ( isEditMode ) {
            updateGAPSelected(body);
            return;
        }

        createGAPWritten(body);

    }

    const setValuesWrittenForm = (gapObject) => {
        isEditMode
            ?
            setGAP({
                Fecha: GAP.Fecha,
                Importe_GAP: GAP.Importe_GAP,
                Asesor: GAP.Asesor,
                Nombre_Cliente: GAP.Nombre_Cliente,
                Utilidad: GAP.Utilidad,
                Comision_Asesor: GAP.Comision_Asesor,
                Utilidad_Neta: GAP.Utilidad_Neta,
                Id: GAP.Id
            })
            :
            setGAP({
                Fecha: gapObject.Fecha,
                Importe_GAP: gapObject.Importe_GAP,
                Asesor: gapObject.Asesor,
                Nombre_Cliente: gapObject.Nombre_Cliente,
                Utilidad: gapObject.Utilidad,
                Comision_Asesor: gapObject.Comision_Asesor,
                Utilidad_Neta: gapObject.Utilidad_Neta,
            })
    }

    const updateGAPSelected = async (body) => {
        url = Apiurl + "api/ctrloperGAP/update"
        await axios.patch(url, body)
            .then(response => {
                if (response['data'].isUpdated) {
                    getGAP();
                    setIsEditMode(false);
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createGAPWritten = async (body) => {
        url = Apiurl + "api/ctrloperGAP/create"
        await axios.post(url, body)
            .then(response => {

                if (response['data'].isCreated) {
                    getGAP();
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte();
                    return;
                }

                if (!response['data'].isCreated) { //body.GAP
                    setValuesWrittenForm(body.GAP)
                    toast.error("Ya existe un registro con la fecha seleccionada en la BD.")
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const editGAP = (gapObject) => {
        setIsEditMode(true);
        setGAP({
            Fecha: gapObject.Fecha.substring(0, 10),
            Importe_GAP: formatoMoneda(formatDecimalNumber(gapObject.Importe_GAP)),
            Asesor: gapObject.Asesor,
            Nombre_Cliente: gapObject.Nombre_Cliente,
            Utilidad: formatoMoneda(formatDecimalNumber(gapObject.Utilidad)),
            Comision_Asesor: formatoMoneda(formatDecimalNumber(gapObject.Comision_Asesor)),
            Utilidad_Neta: formatoMoneda(formatDecimalNumber(gapObject.Utilidad_Neta)),
            Id: gapObject.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#CtrlOper_GAP').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#CtrlOper_GAP').DataTable().destroy();

    }


    return (
        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">GAP: PROGRAMA DE VALOR AGREGADO</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createGAP}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalCtrlOper_GAP
                            data={GAP}
                            onSubmit={nuevoRegistroGAP}
                            editMode={isEditMode}
                            asesores={asesores}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="CtrlOper_GAP" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" className='text-center'>Fecha</th>
                                    <th width="10%" className='text-center'>Importe GAP</th>
                                    <th width="20%" className='text-center'>Asesor</th>
                                    <th width="20%" className='text-center'>Nombre Cliente</th>
                                    <th width="10%" className='text-center'>Utilidad</th>
                                    <th width="15%" className='text-center'>Comisión Asesor</th>
                                    <th width="10%" className='text-center'>Utilidad Neta</th>
                                    <th width="5%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((gap) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(gap.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(gap.Importe_GAP))}</td>
                                                    <td className='text-center'>{gap.Asesor}</td>
                                                    <td className='text-center'>{gap.Nombre_Cliente}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(gap.Utilidad))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(gap.Comision_Asesor))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(gap.Utilidad_Neta))}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editGAP(gap)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default CtrlOper_GAP;