import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalIndicadorLeadsCentroProp } from '../modales/ModalIndicadorLeadsCentroProp';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
// import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { Apiurl } from '../services/Apirest';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FechaDeHoy } from "../componentes/FechaFormatoHoy"
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import axios from 'axios'
import $ from 'jquery';

const IndicadorLeadsCentroProp = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [leadsCentroProp, setLeadsCentroProp] = useState({
        Fecha: "",
        Encuesta_pulso: ""
    })
    const [isEditMode, setIsEditMode] = useState(false)
    const [data, setData] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    const getLeadsCentroProp = async () => {
        url = Apiurl + "api/LeadsCentroProp/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    useEffect(() => {
        getLeadsCentroProp();
    }, [])

    const setValuesWrittenForm = ( leadsCentroP ) => {
        if (isEditMode) {
            setLeadsCentroProp({
                Fecha: leadsCentroProp.Fecha,
                Encuesta_pulso: leadsCentroProp.Encuesta_pulso, 
                Id: leadsCentroProp.Id
            })
        }

        if (!isEditMode) {
            setLeadsCentroProp({
                Fecha: leadsCentroP.Fecha,
                Encuesta_pulso: leadsCentroP.Encuesta_pulso 
            })
        }
    }

    const nuevoRegistroLeadsCentroProp = async (leadsCentroP) => {

        console.log(leadsCentroP.Encuesta_pulso)

        if (leadsCentroP.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.")
            setValuesWrittenForm( leadsCentroP )
            return;
        }

        if (leadsCentroP.Encuesta_pulso.substring(0,1) === "-") {
            setValuesWrittenForm( leadsCentroP );
            toast.info("No se permiten números negativos, intentar de nuevo.")
            return;
        }

        if (leadsCentroP.Encuesta_pulso === "") {
            leadsCentroP.Encuesta_pulso = 0;
        }

        url = Apiurl + "api/LeadsCentroProp/create"

        let body = {
            leadsCentroProp: leadsCentroP,
            agencia
        }

        if (isEditMode) {
            // CREAR URL PARA ACTUALIZAR EL REGISTRO ...
            url = Apiurl + "api/LeadsCentroProp/update"
            await axios.patch(url, body)
                .then(response => {
                    if (response['data'].isUpdated) {
                        getLeadsCentroProp();
                        setIsEditMode(false);
                        toast.success("El registro se actualizó exitosamente.");
                        closeModalReporte();
                    }
                })
                .catch(err => {
                    toast.error("Ocurrió un error al conectarse con el servidor")
                })

            return;
        }

        await axios.post(url, body)
            .then(response => {

                if (response['data'].isCreated) {
                    getLeadsCentroProp()
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte()
                }
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")

            })


    }

    const createIndicador = () => {
        setIsEditMode(false)
        setLeadsCentroProp({
            Fecha: FechaDeHoy(),
            Encuesta_pulso: ""
        })

        openModalReporte()
    }

    const editIndicador = (indicador) => {
        setIsEditMode(true)
        setLeadsCentroProp({
            Fecha: indicador.Fecha.substring(0, 10),
            Encuesta_pulso: indicador.Encuesta_pulso,
            Id: indicador.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#leads_centro_propietario').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#leads_centro_propietario').DataTable().destroy();

    }

    return (
        <div className='content-wrapper'>

            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">CENTRO DE PROPIETARIO</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createIndicador}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalIndicadorLeadsCentroProp
                            data={leadsCentroProp}
                            onSubmit={nuevoRegistroLeadsCentroProp}
                            editMode={isEditMode}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="leads_centro_propietario" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%" className='text-center'>Fecha</th>
                                    <th width="60%" className='text-center'># Encuesta de Pulso</th>
                                    <th width="20%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((indicador) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(indicador.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{indicador.Encuesta_pulso}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editIndicador(indicador)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default IndicadorLeadsCentroProp;
