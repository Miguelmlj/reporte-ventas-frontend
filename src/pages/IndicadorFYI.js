
import React, { useEffect, useState } from 'react'
import axios from 'axios';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalIndicadorFYI } from '../modales/ModalIndicadorFYI';
import { ToastContainer, toast } from 'react-toastify';
import { Apiurl } from '../services/Apirest';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from "../componentes/FechaFormatoHoy"
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda'
import $ from 'jquery';

const IndicadorFYI = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [indicadorFYI, setIndicadorFYI] = useState({
        Fecha: "",
        GarExtendidas: "",
        Seguros: "",
        OnStart: "",
        ProgValorFactura: "",
        ImporteAccesorios: "",
        Id_indicador: ""
    })
    const [data, setData] = useState([])
    const [agencia, setAgencia] = useState({
        Empresa: getEmpresa(),
        Sucursal: getSucursal()
    })
    const [editMode, setEditMode] = useState(false)
    let url;

    const getIndicadoresFYI = async () => {
        url = Apiurl + "api/indicadorFYI/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                // console.log(response['data']);
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })

    }

    useEffect(() => {
        getIndicadoresFYI();
    }, [])

    const dataTable = () => {
        $(document).ready(function () {
            $('#indicador_fyi').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#indicador_fyi').DataTable().destroy();

    }

    const editIndicador = (indicador) => {
        setEditMode(true)
        setIndicadorFYI({
            Fecha: indicador.Fecha.substring(0, 10),
            GarExtendidas: indicador.Garantias_extendidas,
            Seguros: indicador.Seguros,
            OnStart: indicador.On_start,
            ProgValorFactura: indicador.GAP,
            ImporteAccesorios: formatoMoneda(formatDecimalNumber(indicador.Importe_accesorios)),
            Id_indicador: indicador.Id_indicador
        })
        openModalReporte()
        // console.log(Id_indicador);
    }

    const createIndicador = () => {
        setEditMode(false)
        setIndicadorFYI({
            Fecha: FechaDeHoy(),
            GarExtendidas: "",
            Seguros: "",
            OnStart: "",
            ProgValorFactura: "",
            ImporteAccesorios: "",

        })
        openModalReporte()
    }

    const setValuesWrittenForm = ( indFYI ) => {
        if ( editMode ) {
            setIndicadorFYI({
                Fecha: indicadorFYI.Fecha,
                GarExtendidas: indicadorFYI.GarExtendidas,
                Seguros: indicadorFYI.Seguros,
                OnStart: indicadorFYI.OnStart,
                ProgValorFactura: indicadorFYI.ProgValorFactura,
                ImporteAccesorios: indicadorFYI.ImporteAccesorios,
                Id_indicador: indicadorFYI.Id_indicador
            })
        }

        if ( !editMode ) {
            setIndicadorFYI({
                Fecha: indFYI.Fecha,
                GarExtendidas: indFYI.GarExtendidas,
                Seguros: indFYI.Seguros,
                OnStart: indFYI.OnStart,
                ProgValorFactura: indFYI.ProgValorFactura,
                ImporteAccesorios: indFYI.ImporteAccesorios
            })
        }
    }

    const nuevoReporteIndicadorFYI = async (indFYI) => {

        if(indFYI.Fecha === "") {
            setValuesWrittenForm( indFYI );
            toast.info("Favor de elegir una fecha para hacer el registro.")
            return;
        }

        if ( 
           (indFYI.GarExtendidas.toString().substring(0,1) === "-") || 
           (indFYI.ImporteAccesorios.toString().substring(0,1) === "-") || 
           (indFYI.OnStart.toString().substring(0,1) === "-") || 
           (indFYI.ProgValorFactura.toString().substring(0,1) === "-") || 
           (indFYI.Seguros.toString().substring(0,1) === "-") 

         ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(indFYI);
            return;
        }
        

        if (indFYI.GarExtendidas === "") {
            indFYI.GarExtendidas = 0;
        }

        if (indFYI.ImporteAccesorios === "") indFYI.ImporteAccesorios = 0;
        else indFYI.ImporteAccesorios = removerComas(indFYI.ImporteAccesorios)
        
        if (indFYI.OnStart === "") {
            indFYI.OnStart = 0;
        }

        if (indFYI.ProgValorFactura === "") {
            indFYI.ProgValorFactura = 0;
        }
        
        if (indFYI.Seguros === "") {
            indFYI.Seguros = 0;
        }

        let body = {
            indicadorFYI: indFYI,
            agencia: agencia
        }


        url = Apiurl + "api/indicadorFYI/create"

        if (editMode) {
            url = Apiurl + "api/indicadorFYI/update"
            //Update Request
            await axios.patch(url, body)
                .then(response => {
                    if(response['data'].isUpdated){
                        getIndicadoresFYI();
                        setEditMode(false);
                        toast.success("El registro se actualizó exitosamente.")
                        closeModalReporte()
                        
                    }
                })
                .catch(err => {
                    toast.error("Ocurrió un error al conectarse con el servidor")
                })

            return;
        }

        //Create Request
         await axios.post(url, body)
             .then(response => {

                if (response['data'].isCreated) {
                     getIndicadoresFYI();
                     toast.success("Registro completado exitosamente.")
                     closeModalReporte()
                 }
             })
             .catch(err => {
                 toast.error("Ocurrió un error al conectarse con el servidor")
 
             })

    }

    return (
        <div className='content-wrapper'>


            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">INDICADORES F&I</h5>

                                </div>
                            </div>

                        </div>

                    </div>
                    <button type='button' onClick={createIndicador} className="btn btn-outline-info mt-4 mb-2">Registrar</button>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalIndicadorFYI
                            data={indicadorFYI}
                            onSubmit={nuevoReporteIndicadorFYI}
                            editMode={editMode}
                        />
                    </Modal>

                    <div className='col-md-12 col-xs-5 col-sm-12 table-responsive mb-4'>
                        <table id="indicador_fyi" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" className='text-center'>Fecha</th>
                                    <th width="15%" className='text-center'># Garantías Extendidas</th>
                                    <th width="15%" className='text-center'># Seguros</th>
                                    <th width="15%" className='text-center'># OnStart</th>
                                    <th width="15%" className='text-center'># GAP</th>
                                    <th width="20%" className='text-end'>$ Importe en Accesorios</th>
                                    <th width="10%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody id='table_body_indicador_fyi'>
                                {
                                    data.length > 0 ?
                                        data.map((indicador) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(indicador.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{indicador.Garantias_extendidas}</td>
                                                    <td className='text-center'>{indicador.Seguros}</td>
                                                    <td className='text-center'>{indicador.On_start}</td>
                                                    <td className='text-center'>{indicador.GAP}</td>
                                                    <td className='text-end'>{formatoMoneda(formatDecimalNumber(indicador.Importe_accesorios))}</td>
                                                    {/* <td className='text-end'>{formatDecimalNumber(indicador.Importe_accesorios)}</td> */}
                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editIndicador(indicador)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default IndicadorFYI;
