import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import '../css/Login.css'
import { removerVariablesDeSesion } from '../helpers/removeSessionStorage';
import { Apiurl } from '../services/Apirest';
import image_gf from '../assets/images/LogoGF.png'
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Login = () => {
  const [usuario, setUsuario] = useState({
    nombre: "",
    clave: "",
    Empresa: "0",
    Sucursal: "0"
  });
  const [showPass, setshowPass] = useState(false)
  const [sincronizado, setsincronizado] = useState(false)
  const [userRols, setUserRols] = useState([])
  const history = useHistory();

  useEffect(() => {
    getUserRols()
    removerVariablesDeSesion()
    if (window.sessionStorage.getItem('sincronizacion') === null) {window.sessionStorage.setItem('sincronizacion', 'N')}
  }, [])

  const handleLogin = async (e) => {

    if (usuario.Empresa === "0") {
      toast.info("Por favor seleccione una agencia");
      return

    } else if (usuario.Sucursal === "0") {
      toast.info("Por favor seleccione una agencia");
      return

    } else if (usuario.nombre === "") {
      toast.info("Debe ingresar un nombre de usuario");
      return

    } else if (usuario.clave === "") {
      toast.info("Debe ingresar una contraseña");
      return

    }
    iniciarSesionUsuariosDMS()

  };

  const iniciarSesionUsuariosDMS = async () => {
    setsincronizado(true)

    let url = Apiurl + "api/login/usuariosDMS";
    await axios.post(url, usuario)
      .then(response => {
        if (response['data'].isUserFound) {
          window.sessionStorage.setItem('nombre', response['data'].Nombre)
          iniciarSesion();

        } else {
          if (window.sessionStorage.getItem('sincronizacion') === "S") {
            setsincronizado(false)
            toast.info("Usuario o contraseña incorrecta")
            return;
          }
          existsUsersDMS()

        }
      })
      .catch(err => {
        toast.error("Usuario o contraseña incorrecta.")
      })

  }

  const existsUsersDMS = async () => {
    setsincronizado(true)
    // setIsSynchronizing(true);
    let url = Apiurl + "api/usuarios/ExistsUsersDMS";
    axios.get(url)
      .then(response => {
        if (response['data'].message === "existen") {
          DeleteUsersDMS();

        } else if (response['data'].message === "noexisten") {
          CreateUsersDMS()

        }

      })
      .catch(err => {
        console.log(err);
      })


  }

  const DeleteUsersDMS = async () => {
    let url = Apiurl + "api/usuarios/DeleteUsersDMS"
    axios.delete(url)
      .then(response => {
        if (response['data'].isDeleted === true) {
          CreateUsersDMS();
        }
      })
      .catch(err => {
        toast.error("Usuario o contraseña incorrecta.")
      })

  }

  const CreateUsersDMS = async () => {
    let url = Apiurl + "api/usuarios/CreateUsersDMS"
    axios.get(url)
      .then(response => {
        if (response['data'].isCreated === true) {
          CreateUsersCapturas()

        }

        if (response['data'].isCreated === false) {
          toast.error("Usuario o contraseña incorrecta.")
          // setIsSynchronizing(false);
          setsincronizado(false)

        }
      })
      .catch(err => {
        toast.error("Usuario o contraseña incorrecta.")
      })

  }

  const CreateUsersCapturas = async () => {
    let url = Apiurl + "api/usuarios/InsertNewUsers";
    const UsersDMSAndUserRols = {
      "UserRols": userRols
    }

    axios.post(url, UsersDMSAndUserRols)
      .then(response => {
        if (response['data'].isSinchronizationCompleted) {
          window.sessionStorage.setItem('sincronizacion', 'S')
          /* setTimeout(() => {
          }, 5000); */
            iniciarSesionUsuariosDMSDespuesDeSincronizar()
        }

      })
      .catch(err => {
        setsincronizado(false)
        toast.error("Usuario o contraseña incorrecta.")
      })

  }

  const iniciarSesionUsuariosDMSDespuesDeSincronizar = async () => {
    let url = Apiurl + "api/login/usuariosDMS";
    await axios.post(url, usuario)
      .then(response => {
        if (response['data'].isUserFound) {
          window.sessionStorage.setItem('nombre', response['data'].Nombre)
          iniciarSesion();

        } else {
          setsincronizado(false)
          toast.info("Usuario o contraseña incorrecta.")
          return;
        }
      })
      .catch(err => {
        toast.error("Usuario o contraseña incorrecta.")
      })
  }

  const getUserRols = async () => {
    let url = Apiurl + "api/usuarios/UserRols";
    axios.get(url)
      .then(response => {
        setUserRols(response['data'])

      })
      .catch(err => {
        toast.info("Ocurrió un error al obtener los roles de usuario.")
      })
  }


  const iniciarSesion = async (e) => {
    let url = Apiurl + "api/login";
    await axios.post(url, usuario)
      .then(response => {
        if (response['data'].isUserFound) {
          setsincronizado(false)
          asignarVariablesDeSesion(response['data']);

          //redireccionar
          if (response['data'].responsable === 2) {
            history.push("/capturas/afluencia");
            return;
          }

          if (response['data'].responsable === 3) {
            history.push("/capturas/citas");
            return;
          }

          if (response['data'].responsable === 4) {
            history.push("/capturas/solicitudes");
            return;
          }

          if (response['data'].responsable === 1) {
            history.push("/capturas/inicio");
            return;
          }

          if (response['data'].responsable === 5) {
            history.push("/capturas/invfisico");
            return;
          }

          if (response['data'].responsable === 6) {
            history.push("/capturas/anticipos");
            return;
          }

          if (response['data'].responsable === 7) {
            history.push("/capturas/inicio");
            return;
          }

          if (response['data'].responsable === 8) {
            toast.info("No cuentas con permisos de acceso, favor de consultar con su administrador.")
            return;
          }

          if (response['data'].responsable === 9) {
            history.push("/capturas/indicadorMkt");
            return;
          }

          if (response['data'].responsable === 10) {
            history.push("/capturas/invfisico");
            return;
          }

        } else {
          toast.info("Usuario o contraseña incorrecta.")

        }
      })
      .catch(err => {
        toast.error("Usuario o contraseña incorrecta.")
      })
  }

  const asignarVariablesDeSesion = (data) => {
    window.sessionStorage.setItem('usuario', data.nombre_usuario)
    window.sessionStorage.setItem('empresa', data.empresa)
    window.sessionStorage.setItem('sucursal', data.sucursal)
    window.sessionStorage.setItem('tipo_usuario', data.tipo_usuario)
    window.sessionStorage.setItem('responsable', data.responsable)
    window.sessionStorage.setItem('inicio', data.inicio)
    window.sessionStorage.setItem('afluencia', data.afluencia)
    window.sessionStorage.setItem('citas', data.citas)
    window.sessionStorage.setItem('solicitudes', data.solicitudes)
    window.sessionStorage.setItem('anticipos', data.anticipos)
    window.sessionStorage.setItem('invfisico', data.invfisico)
    window.sessionStorage.setItem('codigosqr', data.codigosqr)
    window.sessionStorage.setItem('usuarios', data.usuarios)
    window.sessionStorage.setItem('indFYI', data.indFYI)
    window.sessionStorage.setItem('indLeads', data.indLeads)
    window.sessionStorage.setItem('indCentroProp', data.indCentroProp)
    window.sessionStorage.setItem('indJDPower', data.indJDPower)
    window.sessionStorage.setItem('indBDC', data.indBDC)
    window.sessionStorage.setItem('indMkt', data.indMkt)
    window.sessionStorage.setItem('CO_SegGMF', data.CO_SegGMF)
    window.sessionStorage.setItem('CO_GarGMPlus', data.CO_GarGMPlus)
    window.sessionStorage.setItem('CO_OnStrt', data.CO_OnStrt)
    window.sessionStorage.setItem('CO_GAP', data.CO_GAP)
    window.sessionStorage.setItem('CO_Accsrios', data.CO_Accsrios)
    window.sessionStorage.setItem('CO_PrecioFlx', data.CO_PrecioFlx)
    window.sessionStorage.setItem('CO_GarPlus', data.CO_GarPlus)
    window.sessionStorage.setItem('CO_SegInbros', data.CO_SegInbros)
    window.sessionStorage.setItem('CO_PrecioFlxCntdo', data.CO_PrecioFlxCntdo)
    window.sessionStorage.setItem('DtllsCntrtsGMF', data.DtllsCntrtsGMF)
    window.sessionStorage.setItem('CO_Resumen', data.CO_Resumen)

  }

  const onChange = (e) => {

    if (e.target.name === "nombre") {
      setUsuario({
        ...usuario,
        [e.target.name]: e.target.value.toLowerCase()
      })
    }

    if (e.target.name === "clave") {
      setUsuario({
        ...usuario,
        [e.target.name]: e.target.value.toLowerCase()
      })

    }

    if (e.target.name === "agencia") {

      if (e.target.value === '0') {
        setUsuario({
          ...usuario,
          Empresa: '0',
          Sucursal: '0'
        })
      }

      if (e.target.value === '1') {
        setUsuario({
          ...usuario,
          Empresa: 1,
          Sucursal: 1
        })
      }

      if (e.target.value === '2') {
        setUsuario({
          ...usuario,
          Empresa: 3,
          Sucursal: 1
        })
      }

      if (e.target.value === '3') {
        setUsuario({
          ...usuario,
          Empresa: 5,
          Sucursal: 1
        })
      }

      if (e.target.value === '4') {
        setUsuario({
          ...usuario,
          Empresa: 5,
          Sucursal: 2
        })
      }

      if (e.target.value === '5') {
        setUsuario({
          ...usuario,
          Empresa: 7,
          Sucursal: 1
        })
      }

    }

  }

  const muestraContra = () => {
    setshowPass(!showPass)
  }

  const keyEventPressed = (e) => {
    if (e.key === 'Enter') {
      handleLogin()
    }
  }

  return (
    <div className="hold-transition login-page">
      {/* NUEVO DISENO */}

      <div className="login-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            {/* <i to="/" className="h1">
              <b className='mr-2'>GRUPO</b>
              <span>FÉLIX</span>
            </i> */}
            <div className="text-center">
              <img className='profile-user-img img-fluid img-circle' style={{ width: 140 }} src={image_gf} alt="User profile picture" />
            </div>
          </div>
          <div className="card-body">
            <h5 className="login-box-msg mb-3">INICIAR SESIÓN</h5>
            <form >

              <div className="form-group mb-3">
                <label className="input-group-text font-weight-normal ">Agencia</label>
                <select name='agencia' className='form-select' onChange={onChange}>
                  <option value={0}>Seleccionar Agencia</option>
                  <option value={1}>MOCHIS</option>
                  <option value={2}>GUASAVE</option>
                  <option value={3}>CULIACÁN ZAPATA</option>
                  <option value={4}>CULIACÁN AEROPUERTO</option>
                  <option value={5}>CADILLAC</option>

                </select>
              </div>

              <label className="input-group-text font-weight-normal ">Usuario Global DMS</label>
              <div className="input-group mb-3">

                <input type="email" className="form-control" onChange={onChange} onKeyPress={(e) => keyEventPressed(e)} placeholder="Nombre de usuario" name="nombre" aria-label="correo electronico" aria-describedby="basic-addon2" required autoComplete='off' />
                <div className="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-user"></span>
                  </div>
                </div>

              </div>


              <div className="input-group mb-3">

                <input type={showPass ? "text" : "password"} className="form-control" onChange={onChange} onKeyPress={(e) => keyEventPressed(e)} placeholder="Contraseña" name="clave" aria-label="correo electronico" aria-describedby="basic-addon2" required />
                <div className="input-group-append">
                  <div class="input-group-text">

                    <span class="fas fa-lock"></span>
                  </div>
                </div>

              </div>


              <div className="mb-3">
                <center>
                  <input className="form-check-input" type="checkbox" value="" id="mostrarContrasena" onClick={muestraContra} />
                  <label className="form-check-label" htmlFor="mostrarContrasena">
                    Mostrar contraseña
                  </label>
                </center>
              </div>

            </form>
            {sincronizado &&
              <div className='style-spinner'>
                <Box >
                  <CircularProgress />
                </Box>
                {/* <p>
                  <small>{"Sincronizando"}</small>
                  </p> */}
              </div>
            }

            <div className="mb-0 text-center">
              <button type="button" onClick={handleLogin} className="btn btn-primary" id="btnSesion">INICIAR</button>
            </div>

          </div>
        </div>
      </div>

      <ToastContainer />
    </div>

  )
}

export default Login;
