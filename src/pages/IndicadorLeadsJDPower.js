import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalIndicadorLeadsJDPower } from '../modales/ModalIndicadorLeadsJDPower';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { Apiurl } from '../services/Apirest';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FechaDeHoy } from "../componentes/FechaFormatoHoy"
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import axios from 'axios'
import $ from 'jquery';

const IndicadorLeadsJDPower = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [leadsJDPower, setLeadsJDPower] = useState({
        Fecha: "",
        Cantidad_encuestas: ""
    })
    const [isEditMode, setIsEditMode] = useState(false)
    const [data, setData] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    const createIndicadorJDPower = () => {
        setIsEditMode(false);
        setLeadsJDPower({
            Fecha: FechaDeHoy(),
            Cantidad_encuestas: ""
        })

        openModalReporte();
    }

    const getLeadsPDPower = async () => {
        
        url = Apiurl + "api/indicadorJDPower/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    useEffect(() => {
        getLeadsPDPower();
    }, [])

    const setValuesWrittenForm = (JDPower) => {
        isEditMode
                ?
                setLeadsJDPower({
                    Fecha: leadsJDPower.Fecha,
                    Cantidad_encuestas: leadsJDPower.Cantidad_encuestas,
                    Id: leadsJDPower.Id
                })
                :
                setLeadsJDPower({
                    Fecha: JDPower.Fecha,
                    Cantidad_encuestas: JDPower.Cantidad_encuestas,
                })
    }

    const nuevoRegistroLeadsJDPower = async (JDPower) => {
        
        if (JDPower.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.")
            setValuesWrittenForm(JDPower)
            return;
        }

        if (JDPower.Cantidad_encuestas.substring(0,1) === "-") {
            setValuesWrittenForm(JDPower);
            toast.info("No se permiten números negativos, intentar de nuevo.")
            return;
        }

        if (JDPower.Cantidad_encuestas === "") JDPower.Cantidad_encuestas = 0;

        let body = {
            leadsJDPower: JDPower,
            agencia
        }

        if (isEditMode) {
            url = Apiurl + "api/indicadorJDPower/update"
            await axios.patch(url, body)
                .then(response => {
                    if (response['data'].isUpdated) {
                        getLeadsPDPower();
                        setIsEditMode(false);
                        toast.success("El registro se actualizó exitosamente.")
                        closeModalReporte();
                    }
                })
                .catch(err => {
                    toast.error("Ocurrió un error al conectarse con el servidor")
                })

            return;
        }

        url = Apiurl + "api/indicadorJDPower/create"

        await axios.post(url, body)
            .then(response => {
                if ( response['data'].isCreated ) {
                    getLeadsPDPower()
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte()
                }
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")

            })


    }

    const editIndicadorJDPower = (indicador) => {
        setIsEditMode(true)
        setLeadsJDPower({
            Fecha: indicador.Fecha.substring(0, 10),
            Cantidad_encuestas: indicador.Cantidad_encuestas,
            Id: indicador.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#leads_JDPower').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#leads_JDPower').DataTable().destroy();

    }

    return (
        <div className='content-wrapper'>

            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">JD POWER</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            // onClick={createIndicador}
                            onClick={createIndicadorJDPower}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalIndicadorLeadsJDPower
                            data={leadsJDPower}
                            onSubmit={nuevoRegistroLeadsJDPower}
                            editMode={isEditMode}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="leads_JDPower" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%" className='text-center'>Fecha</th>
                                    <th width="60%" className='text-center'>Cantidad de Encuestas</th>
                                    <th width="20%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((indicador) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(indicador.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{indicador.Cantidad_encuestas}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editIndicadorJDPower(indicador)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default IndicadorLeadsJDPower;