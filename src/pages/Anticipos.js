import React, { useEffect, useState } from 'react'
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalAnticipos } from '../modales/ModalAnticipos';
import { FechaDeHoy } from "../componentes/FechaFormatoHoy"
import axios from 'axios';
import { Apiurl } from '../services/Apirest';
import { ToastContainer, toast } from 'react-toastify';
// import { confDataTable } from '../componentes/ConfDataTable';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import $ from 'jquery';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "datatables.net-dt/css/jquery.dataTables.min.css"
import { getAgencia } from '../helpers/getAgencia';
import { getEmpresa, getSucursal, getNombreAsesor } from '../helpers/getEmpresaSucursal';
import { getAgenciaActualizado } from '../helpers/getAgencia';

const Anticipos = () => {
  // const [modalEditarReporte, setModalEditarReporte] = useState(false)
  // const [isOpenModalEditarReporte, opendModalEditarReporte, closeModalEditarReporte]= useModal(false);
    const [data, setData] = useState([])
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [isEditMode, setIsEditMode] = useState(false)
    const nombreAsesor = getNombreAsesor();
    const rol = window.sessionStorage.getItem('responsable');
    const idEmpresa = getEmpresa();
    const idSucursal = getSucursal();
    const nombreAgencia = getAgencia(
      idEmpresa, 
      idSucursal
    );
    const [reporteAnticipo, setReporteAnticipo] = useState({
      Empresa: "",
      Sucursal: "",
      Id_anticipo:"",
      Folio_anticipo: "",
      Fecha: "", 
      Nombre_cliente: "",
      Unidad: "",
      Paquete: "",
      Color: "",
      Tipo_compra: "",
      Nombre_asesor: "",
      Agencia: ""
  })
  
  //objAnticipo se utiliza al realizar la petición de los registros
  let objAnticipo = {
    Empresa: idEmpresa,
    Sucursal: idSucursal,
    nombreAsesor: nombreAsesor,
    rol: rol
  }

    useEffect(() => {
      peticionGetAnticipos();
    }, [])
    
    const peticionGetAnticipos = async () => {
      const url1 = Apiurl + "api/anticipo"
      await axios.post(url1, objAnticipo)
      .then(response => {
        dataTableDestroy();
        setData(response['data'])
        dataTable()
      })
      .catch(error => {
        toast.error("Ocurrió un error al obtener los registros.");
      })
    }

    const setValuesWrittenForm = (anticip) => {
      isEditMode
      ?
      setReporteAnticipo({
        Empresa: reporteAnticipo.Empresa,
        Sucursal: reporteAnticipo.Sucursal,
        Id_anticipo: reporteAnticipo.Id_anticipo,
        Folio_anticipo: reporteAnticipo.Folio_anticipo,
        Fecha: reporteAnticipo.Fecha, 
        Nombre_cliente: reporteAnticipo.Nombre_cliente,
        Unidad: reporteAnticipo.Unidad,
        Paquete: reporteAnticipo.Paquete,
        Color: reporteAnticipo.Color,
        Tipo_compra: reporteAnticipo.Tipo_compra,
        Nombre_asesor: reporteAnticipo.Nombre_asesor,
        Agencia: reporteAnticipo.Agencia
      })
      :
      setReporteAnticipo({
        Empresa: anticip.Empresa,
        Sucursal: anticip.Sucursal,
        Id_anticipo:anticip.Id_anticipo,
        Folio_anticipo: anticip.Folio_anticipo,
        Fecha: anticip.Fecha, 
        Nombre_cliente: anticip.Nombre_cliente,
        Unidad: anticip.Unidad,
        Paquete: anticip.Paquete,
        Color: anticip.Color,
        Tipo_compra: anticip.Tipo_compra,
        Nombre_asesor: anticip.Nombre_asesor,
        Agencia: anticip.Agencia
      })
    }

    const nuevoReporteAnticipo = async(newRep) => {
        if(newRep.Folio_anticipo === ""){
          toast.info('Campo Folio Anticipo Vacío')
          setValuesWrittenForm(newRep)
          return
        }
        
        if(newRep.Folio_anticipo.length > 15){
          toast.info('Folio Anticipo demasiado largo, debe contener un máximo de 15 dígitos.')
          setValuesWrittenForm(newRep)
          return
        }

        if(newRep.Nombre_cliente === ""){
          toast.info('Campo Nombre Cliente Vacío')
          setValuesWrittenForm(newRep)
          return
        }
        
        if(newRep.Unidad === ""){
          toast.info('Campo Unidad Vacío')
          setValuesWrittenForm(newRep)
          return
        }
        
        if(newRep.Paquete === ""){
          toast.info('Campo Paquete Vacío')
          setValuesWrittenForm(newRep)
          return
        }
        
        if(newRep.Tipo_compra === ""){
          toast.info('Campo Tipo Compra Vacío')
          setValuesWrittenForm(newRep)
          return
        }

        if(newRep.Color === ""){
          toast.info('Campo Color Vacío')
          setValuesWrittenForm(newRep)
          return
        }

        if ( isEditMode ) {
          updateReporteAnticipo( newRep )
          return;
        }
        createNuevoAnticipoBD( newRep )
    }

    const createNuevoAnticipoBD = ( anticip ) => {
      let url = Apiurl + "api/anticipocreate";
        axios.post(url, anticip)
        .then(response => {
          if(`${response['statusText']}` === 'OK'){
            peticionGetAnticipos();
            closeModalReporte();
            toast.success(response.data.message);
            
          }
        })
        .catch(error => {
          toast.error('Error al conectarse con el servidor');
        })
    }

    const updateReporteAnticipo = (anticipoActualizado) => {
      const {Id_anticipo} = anticipoActualizado;

      let url3 = Apiurl + `api/anticipo/${Id_anticipo}`
      axios.patch(url3, anticipoActualizado)
      .then(async response => {

        if (`${response['statusText']}` === 'OK') {
          peticionGetAnticipos();
          closeModalReporte();
          toast.success(response.data.message);
        }

      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })


    }

    const createAnticipo = () => {
      setIsEditMode(false);
      setReporteAnticipo({
        Empresa: idEmpresa,
        Sucursal: idSucursal,
        Id_anticipo:"",
        Folio_anticipo: "",
        Fecha: FechaDeHoy(), 
        Nombre_cliente: "",
        Unidad: "",
        Paquete: "",
        Color: "",
        Tipo_compra: "",
        Nombre_asesor: nombreAsesor.toUpperCase(),
        Agencia: getAgenciaActualizado(idEmpresa, idSucursal)
      })

      openModalReporte();
    }

    const editAnticipos = (ant) => {
      setIsEditMode( true );
      setReporteAnticipo({
        Empresa: ant.Empresa.toString(),
        Sucursal: ant.Sucursal.toString(),
        Id_anticipo: ant.Id_anticipo,
        Folio_anticipo: ant.Folio_anticipo,
        Fecha: ant.Fecha.substring(0,10),
        Nombre_cliente: ant.Nombre_cliente,
        Unidad: ant.Unidad,
        Paquete: ant.Paquete,
        Color: ant.Color,
        Tipo_compra: ant.Tipo_compra,
        Nombre_asesor: ant.Nombre_asesor,
        Agencia: ant.Agencia
      })

      openModalReporte();
    }

    const dataTable = () => {
      $(document).ready(function () {
        $('#anticipos').DataTable(
          confDataTableIndicadores
        );
      });
    }

    const dataTableDestroy = () => {
      $('#anticipos').DataTable().destroy();
    }

  return (
    <div className='content-wrapper'>

      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-12">
              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className="m-0 text-dark">ANTICIPOS</h5>
                </div>
              </div>

            </div>
          </div>
          {/* <button type='button' onClick={openModalReporte} className="btn btn-outline-info mt-4">Registrar Anticipo</button> */}
          <button type='button' onClick={createAnticipo} className="btn btn-outline-info mt-4">Registrar Anticipo</button>
        </div>
      </div>

      <div className="container-fluid">
        <div className="row">
            <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                <ModalAnticipos 
                onSubmit={nuevoReporteAnticipo} 
                nombreAsesor={nombreAsesor} 
                nombreAgencia={nombreAgencia}
                idEmpresa={idEmpresa}
                idSucursal={idSucursal}
                data={reporteAnticipo}
                />
            </Modal>

         <div className='col-md-12 col-xs-5 col-sm-12 table-responsive mb-4'>
          <table id="anticipos" className="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Folio</th>
                <th>Cliente</th>
                <th>Unidad</th>
                <th>Paquete</th>
                {/* <th>Importe</th> */}
                <th>Color</th>
                <th>Compra</th>
                <th>Asesor</th>
                <th>Agencia</th>
                <th>Editar</th>

              </tr>
            </thead>
            {/* <tfoot>
                <tr>
                  <td colspan="11">{`Mostrando ${registrosPorPagina} de ${totalRegistrosFooter} registros`}</td>

                </tr>
              </tfoot> */}
            {/* <tbody id="table_body_anticipos"> */}
            <tbody>

              {
                data.length > 0 ? data.map((anticipo) => {
                  return (
                    <tr>
                      <td>{reverseDate( anticipo.Fecha.substring(0, 10) )}</td>
                      <td>{anticipo.Folio_anticipo}</td>
                      <td>{anticipo.Nombre_cliente}</td>
                      <td>{anticipo.Unidad}</td>
                      <td>{anticipo.Paquete}</td>
                      <td>{anticipo.Color}</td>
                      <td>{anticipo.Tipo_compra}</td>
                      <td>{anticipo.Nombre_asesor}</td>
                      <td>{anticipo.Agencia}</td>
                      <td>{
                      // <button  type="button" className="btn btn-dark" title="Editar" onClick={() => handleReporteAnticipo(anticipo.Id_anticipo)}><FontAwesomeIcon icon={faEdit}></FontAwesomeIcon></button>
                      <button  type="button" className="btn btn-dark" title="Editar" onClick={() => editAnticipos(anticipo)}><FontAwesomeIcon icon={faEdit}></FontAwesomeIcon></button>
                      }</td>
                    </tr>
                  )
                }):"No hay registros"
              }

            </tbody>
          </table>
          </div>

        </div>


        {/* <Modal isOpen={isOpenModalEditarReporte} closeModal={closeModalEditarReporte} >
        <ModalEditarAnticipos 
          anticipo={reporteAnticipo} 
          onSubmit={updateReporteAnticipo} 
        />
        </Modal> */}
      </div>

      <ToastContainer />
    </div>

    
  )
}

export default Anticipos;
