import React, { useEffect, useState } from 'react'
import axios from 'axios';
import QRCode from "react-qr-code";
import { ToastContainer, toast } from 'react-toastify';
import { jsPDF } from "jspdf";
import html2canvas from 'html2canvas';
import '../css/Modulos.css';
import { useModal } from '../hooks/useModal';
import Modal from '../componentes/Modal';
import { ModalGenerarQR } from '../modales/ModalGenerarQR';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { Apiurl } from '../services/Apirest';
import { PrintQRS } from '../componentes/PrintQRS';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckToSlot } from '@fortawesome/free-solid-svg-icons';


const CodesQR = () => {
    const [data, setData] = useState([]);
    const [checkedQRS, setCheckedQRS] = useState([])
    const [generarQr, setGenerarQr] = useState(false);
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    // const url = "http://fasaip.ddns.net:9080/globaldms/invvehiculos.asp";
    const [nuevosqr, setNuevosqr] = useState([]);
    const [impresionActiva, setImpresionActiva] = useState(false)
    const [invActualizado, setInvActualizado] = useState(false)
    const [objeto, setObjeto] = useState({ Empresa: 0, Sucursal: 0 })
    const [invmochis, setInvmochis] = useState([])
    const [invguasave, setInvguasave] = useState([])
    const [invcadillac, setInvcadillac] = useState([])
    const [invculiacan, setInvculiacan] = useState([])
    const [selectAllQRSVIN, setSelectAllQRSVIN] = useState(false)
    const [isVINGenerated, setisVINGenerated] = useState(false)
    const [codigosAgencias, setCodigosAgencias] = useState(
        [
            { nombreagencia: "Seleccionar Agencia", valor: -1, numseries: [] },

        ]
    )
    const [codigosAgenciaGenerados, setCodigosAgenciaGenerados] = useState("null")
    //variables para manejar la creación de las opciones del select
    //contadores y estados

    const [firsttimeMochis, setFirsttimeMochis] = useState(false)
    const [firsttimeGuasave, setFirsttimeGuasave] = useState(false)
    const [firsttimeCadillac, setFirsttimeCadillac] = useState(false)
    const [firsttimeCuliacan, setFirsttimeCuliacan] = useState(false)
    const Empresa = window.sessionStorage.getItem('empresa')
    const Sucursal = window.sessionStorage.getItem('sucursal')
    const Responsable = window.sessionStorage.getItem('responsable')

    const mensajeEspera = "Favor de esperar un momento..."
    const mensajeEspera2 = "Actualizando Inventarios..."
    let contadorselect = 0;

    let contador = 1;
    let cantidadqr = 20;
    let aumentoCantidadqr = 20;
    let cuentaRegresiva = -1;
    let cuentaRegresivaStart = 3;


    const incrementarValores = (index) => {
        contador += 1;
        cantidadqr = contador * aumentoCantidadqr;

        cuentaRegresiva = index + 3;
    }

    const quitCheckedBox = (e) => {
        let id_tag_qrs = document.getElementById('divToPrint');
        for (const iterator of id_tag_qrs.children) {
            iterator.children[0].childNodes[0].checked = false
        }

        setCheckedQRS([])
        setSelectAllQRSVIN(false)
    }

    const cambiarEmpresa = (e) => {

        if (e.target.value === "0") {
            setData([])
            setCodigosAgenciaGenerados("null")
        }

        quitCheckedBox()
        setisVINGenerated(false)


        if (e.target.value !== "0") {
            setData(codigosAgencias[e.target.value].numseries);
            setCodigosAgenciaGenerados(codigosAgencias[e.target.value].nombreagencia)
            toast.success(`Códigos ${codigosAgencias[e.target.value].nombreagencia} han sido generados correctamente.`)
        }

    }


    useEffect(() => {
        actualizarInventarioSistema();

    }, [])


    useEffect(() => {
        /* nota: los estados firsttimeMOchis y crearSelectMochis son manejados 
        debido a que el useEffect carga dos veces al cargarse la página,
        y para evitar que las opciones del select se crearán más de una vez
        se utilizaron estas variables. */

        if (firsttimeMochis && invmochis.length > 0) {
            let arrayFragmentos = [];
            let cont = 1;
            let multiplo = 60;

            while (invmochis.length >= (multiplo * cont)) {
                let arrayTemp;
                if (cont === 1) {
                    arrayTemp = invmochis.slice(invmochis.length - invmochis.length, multiplo);
                    arrayFragmentos.push({
                        nombreagencia: `Mochis (${(invmochis.length - invmochis.length) + 1} - ${multiplo})`,
                        valor: contadorselect += 1,
                        numseries: arrayTemp
                    });
                } else {
                    arrayTemp = invmochis.slice((multiplo * cont) - multiplo, (multiplo * cont));
                    arrayFragmentos.push({
                        nombreagencia: `Mochis (${((multiplo * cont) - multiplo) + 1} - ${(multiplo * cont)})`,
                        valor: contadorselect += 1,
                        numseries: arrayTemp
                    });
                }
                cont++;
            }

            if (invmochis.length > (multiplo * (cont - 1)) && invmochis.length < (multiplo * cont)) {
                let arrayTemp = invmochis.slice((multiplo * (cont - 1)), invmochis.length);
                arrayFragmentos.push({
                    nombreagencia: `Mochis (${(multiplo * (cont - 1)) + 1} - ${invmochis.length})`,
                    valor: contadorselect += 1,
                    numseries: arrayTemp
                });
            }

            setCodigosAgencias([
                ...codigosAgencias,
                ...arrayFragmentos
            ])

        }

        // if(firsttimeMochis) setCrearSelectMochis(false)

        setFirsttimeMochis(true)
        /* crearSelectMochis+=1;
        console.log(crearSelectMochis); */
    }, [invmochis])

    useEffect(() => {

        if (firsttimeGuasave && invguasave.length > 0) {
            let arrayFragmentos = [];
            let cont = 1;
            let multiplo = 60;

            while (invguasave.length >= (multiplo * cont)) {
                let arrayTemp;
                if (cont === 1) {

                    arrayTemp = invguasave.slice(invguasave.length - invguasave.length, multiplo);
                    arrayFragmentos.push({
                        nombreagencia: `Guasave (${(invguasave.length - invguasave.length) + 1} - ${multiplo})`,
                        valor: contadorselect += 1,
                        numseries: arrayTemp
                    });

                } else {

                    arrayTemp = invguasave.slice((multiplo * cont) - multiplo, (multiplo * cont));
                    arrayFragmentos.push({
                        nombreagencia: `Guasave (${((multiplo * cont) - multiplo) + 1} - ${(multiplo * cont)})`,
                        valor: contadorselect += 1,
                        numseries: arrayTemp
                    });

                }
                cont++;
            }

            if (invguasave.length > (multiplo * (cont - 1)) && invguasave.length < (multiplo * cont)) {
                let arrayTemp = invguasave.slice((multiplo * (cont - 1)), invguasave.length);
                arrayFragmentos.push({
                    nombreagencia: `Guasave (${(multiplo * (cont - 1)) + 1} - ${invguasave.length})`,
                    valor: contadorselect += 1,
                    numseries: arrayTemp
                });

            }

            setCodigosAgencias([
                ...codigosAgencias,
                ...arrayFragmentos
            ])


        }

        setFirsttimeGuasave(true)
    }, [invguasave])

    useEffect(() => {

        if (firsttimeCadillac && invcadillac.length > 0) {
            let arrayFragmentos = [];
            let cont = 1;
            let multiplo = 60;

            while (invcadillac.length >= (multiplo * cont)) {
                let arrayTemp;
                if (cont === 1) {
                    arrayTemp = invcadillac.slice(invcadillac.length - invcadillac.length, multiplo);
                    arrayFragmentos.push({
                        nombreagencia: `Cadillac (${(invcadillac.length - invcadillac.length) + 1} - ${multiplo})`,
                        valor: contadorselect += 1,
                        numseries: arrayTemp
                    });

                } else {
                    arrayTemp = invcadillac.slice((multiplo * cont) - multiplo, (multiplo * cont));
                    arrayFragmentos.push({
                        nombreagencia: `Cadillac (${((multiplo * cont) - multiplo) + 1} - ${(multiplo * cont)})`,
                        valor: contadorselect += 1,
                        numseries: arrayTemp
                    });
                }
                cont++;
            }

            if (invcadillac.length > (multiplo * (cont - 1)) && invcadillac.length < (multiplo * cont)) {
                let arrayTemp = invcadillac.slice((multiplo * (cont - 1)), invcadillac.length);
                arrayFragmentos.push({
                    nombreagencia: `Cadillac (${(multiplo * (cont - 1)) + 1} - ${invcadillac.length})`,
                    valor: contadorselect += 1,
                    numseries: arrayTemp
                });
            }

            setCodigosAgencias([
                ...codigosAgencias,
                ...arrayFragmentos
            ])

        }

        setFirsttimeCadillac(true)

    }, [invcadillac])

    useEffect(() => {

        //pensar como agregar todos los qrs de culiacán por segmentos, sin repetirse... done!!

        if (firsttimeCuliacan && invculiacan.length > 0) {
            let arrayFragmentos = [];
            let cont = 1;
            let multiplo = 60;

            //array.lengt = invculiacan.length
            while (invculiacan.length >= (multiplo * cont)) {

                let arrayTemp;
                if (cont === 1) {

                    arrayTemp = invculiacan.slice(invculiacan.length - invculiacan.length, multiplo);
                    arrayFragmentos.push({
                        nombreagencia: `Culiacán (${(invculiacan.length - invculiacan.length) + 1} - ${multiplo})`,
                        valor: contadorselect += 1,
                        numseries: arrayTemp
                    });


                } else {

                    arrayTemp = invculiacan.slice((multiplo * cont) - multiplo, (multiplo * cont));
                    arrayFragmentos.push({
                        nombreagencia: `Culiacán (${((multiplo * cont) - multiplo) + 1} - ${(multiplo * cont)})`,
                        valor: contadorselect += 1,
                        numseries: arrayTemp
                    });

                }
                cont++;
            }

            if (invculiacan.length > (multiplo * (cont - 1)) && invculiacan.length < (multiplo * cont)) {
                let arrayTemp = invculiacan.slice((multiplo * (cont - 1)), invculiacan.length);
                arrayFragmentos.push({
                    nombreagencia: `Culiacán (${(multiplo * (cont - 1)) + 1} - ${invculiacan.length})`,
                    valor: contadorselect += 1,
                    numseries: arrayTemp
                });

            }

            setCodigosAgencias([
                ...codigosAgencias,
                ...arrayFragmentos
            ])


        }

        setFirsttimeCuliacan(true)

    }, [invculiacan])

    const insertDataOfDMSVehiculosToVehiculosSQL = async () => {
        let url = Apiurl + "api/codigosqr/insertAllRowsOfDMSToVehiculos"
        axios.post(url)
            .then(response => {
                // response['data'].isCreated) 

                //se deactivara el spinner
                //se mandará llamar todos los registros 
                loadSelectFromInventarioVehiculos()

            })
            .catch(err => {
                console.log(err);
                toast.info("Ocurrió un error al insertar los registros en la tabla vehículos")
            })
    }

    const loadSelectFromInventarioVehiculos = async () => {
        setInvActualizado(true)
        if ((Empresa === '1' && Sucursal === '1' && Responsable === '5') || (Responsable === '7')) {
            peticionGetInventarioMochis()

        }
        if ((Empresa === '3' && Sucursal === '1' && Responsable === '5') || (Responsable === '7')) {
            peticionGetInventarioGuasave()

        }
        if ((Empresa === '7' && Sucursal === '1' && Responsable === '5') || (Responsable === '7')) {
            peticionGetInventarioCadillac()

        }
        if ((Empresa === '5' && Sucursal === '1' && Responsable === '5') || (Responsable === '7')) {
            peticionGetInventarioCuliacan()

        }
    }

    const updateTableVehiculosSQL = async () => {

        /* 
        1.- rquest to vehiculosDMS
        2.- request to vehiculos
        3.- Compare both arrays with lenght property
        4.- if they are equals:
            1.- Compare if they have the same VIN:
                yes: do not do anything
                no: eliminamos los que ya están facturados, insertamos los nuevos
                        que no están en tabla vehiculos dms.

        5. if DMS vehi is bigger than vehiculossql: insert rows
        6. if vehiculossql is bigger than DMS vehi: delete rows.
        */

        let urldmsv = Apiurl + "api/codigosqr/getVehiculosDMS";
        let urlsqlv = Apiurl + "api/codigosqr/getVehiculosSQL";

        const dms_vehi = await axios.get(urldmsv);
        const sql_vehi = await axios.get(urlsqlv);

        insertDeleteTableVehiculos(dms_vehi['data'], sql_vehi['data']);


    }

    const insertDeleteTableVehiculos = (dms_vehi_list, sql_vehi_list) => {
        let registerDMSNoFoundInSQLVehiList = [];
        let registerSQLVehiNoFoundInDMSList = [];


        for (const registerDMS of dms_vehi_list) {
            let isFoundRegDMS = false;
            for (const registerSQL of sql_vehi_list) {
                if (registerDMS.vehi_serie === registerSQL.vehi_serie) {
                    isFoundRegDMS = true;
                }
            }

            if (!isFoundRegDMS) {
                registerDMSNoFoundInSQLVehiList.push(registerDMS)
            }

        }


        for (const registerSQL of sql_vehi_list) {
            let isFoundRegSQLVehi = false;
            for (const registerDMS of dms_vehi_list) {
                if (registerDMS.vehi_serie === registerSQL.vehi_serie) {
                    isFoundRegSQLVehi = true;
                }
            }

            if (!isFoundRegSQLVehi) {
                registerSQLVehiNoFoundInDMSList.push(registerSQL)
            }

        }



        /* console.log("registerDMSNoFoundInSQLVehiList", registerDMSNoFoundInSQLVehiList)
        console.log("registerSQLVehiNoFoundInDMSList", registerSQLVehiNoFoundInDMSList) */

        //registerSQLVehiNoFoundInDMSList se eliminará estos registros de la tabla vehiculos
        //registerDMSNoFoundInSQLVehiList se insertarán estos registros en la tabla vehiculosDMS


        if (registerSQLVehiNoFoundInDMSList.length > 0) {
            // console.log("Eliminamos");
            //axios.delete
            let urldelvehi = Apiurl + "api/codigosqr/deleteVehiculosSQL";
            axios.delete(urldelvehi, { data: registerSQLVehiNoFoundInDMSList })
                .then(response => {
                    // response['data'].isDeleted
                    loadSelectFromInventarioVehiculos()

                })
                .catch(err => {
                    toast.error("Ocurrió un problema al actualizar el inventario")
                })
        } else loadSelectFromInventarioVehiculos()


        if (registerDMSNoFoundInSQLVehiList.length > 0) {
            // console.log("Insertarmos");
            let urlinsertvehi = Apiurl + "api/codigosqr/insertVehiculosSQL";
            axios.post(urlinsertvehi, registerDMSNoFoundInSQLVehiList)
                .then(response => {
                    // response['data'].isCreated
                    loadSelectFromInventarioVehiculos()

                })
                .catch(err => {
                    toast.error("Ocurrió un problema al actualizar el inventario")
                })
        } else loadSelectFromInventarioVehiculos()


    }

    const tableVehiculosIsEmpty = async () => {
        let url = Apiurl + "api/codigosqr/tableVehiculosIsEmpty"

        axios.get(url)
            .then(response => {
                if (response['data'].isEmpty) {
                    // toast.info("NO Existen registros en la tabla(ISVACIA)")
                    // 2.- yes: insertAllRowsOfDMSVehiculos() || no: updateTableVehiculosSQL()
                    insertDataOfDMSVehiculosToVehiculosSQL()
                }

                if (!response['data'].isEmpty) {
                    // toast.info("Existen registros en la tabla")

                    updateTableVehiculosSQL()
                }

            })
            .catch(err => {
                console.log(err);
            })
    }

    const actualizarInventarioSistema = async () => {

        //PETICIÓN A API DE ASP, ENDPOINT DE ORACLE
        let url = "/globaldms/invvehiculos.asp";
        await axios.get(url)
            .then(response => {

                // tableVehiculosIsEmpty()
                loadSelectFromInventarioVehiculos();
                /* 
                1.- tableVehiculosIsEmpty()
                2.- yes: createAllRowsOfDMSVehiculos() || no: refreshTableVehiculosSQL()
                3.-
                */

                //CODE BELOW COMMENTED TEMPORALY
                // setInvActualizado(true)
                /* if ((Empresa === '1' && Sucursal === '1' && Responsable === '5') || (Responsable === '7')) {
                    peticionGetInventarioMochis()

                }
                if ((Empresa === '3' && Sucursal === '1' && Responsable === '5') || (Responsable === '7')) {
                    peticionGetInventarioGuasave()

                }
                if ((Empresa === '7' && Sucursal === '1' && Responsable === '5') || (Responsable === '7')) {
                    peticionGetInventarioCadillac()

                }
                if ((Empresa === '5' && Sucursal === '1' && Responsable === '5') || (Responsable === '7')) {
                    peticionGetInventarioCuliacan()

                } */

            })
            .catch(err => {
                console.log(err)
            })

    }

    const peticionGetInventarioMochis = async () => {
        let url = Apiurl + "api/vehiculosdmsqr";
        const objeto = { Empresa: 1, Sucursal: 1 }
        await axios.post(url, objeto)
            .then(response => {
                setInvmochis(response['data']);

            })
            .catch(err => {
                toast.error("Ocurrió un problema con el sevidor al obtener inventario")
            })

    }

    const peticionGetInventarioGuasave = async () => {
        let url = Apiurl + "api/vehiculosdmsqr";
        const objeto = { Empresa: 3, Sucursal: 1 }
        await axios.post(url, objeto)
            .then(response => {
                setInvguasave(response['data'])

            })
            .catch(err => {
                toast.error("Ocurrió un problema con el sevidor al obtener inventario")
            })

    }
    const peticionGetInventarioCadillac = async () => {
        let url = Apiurl + "api/vehiculosdmsqr";
        const objeto = { Empresa: 7, Sucursal: 1 }
        await axios.post(url, objeto)
            .then(response => {
                setInvcadillac(response['data'])

            })
            .catch(err => {
                toast.error("Ocurrió un problema con el sevidor al obtener inventario")
            })
    }
    const peticionGetInventarioCuliacan = async () => {
        let url = Apiurl + "api/vehiculosdmsqr";
        const objeto = { Empresa: 5, Sucursal: 1 }
        await axios.post(url, objeto)
            .then(response => {
                /* console.log("culiacan");
                console.log(response['data']); */
                setInvculiacan(response['data'])
                // setCamposSelectAgencia();
            })
            .catch(err => {
                toast.error("Ocurrió un problema con el sevidor al obtener inventario")
            })
    }

    const nuevosCodigosQr = (qrs) => {
        let objeto = [];
        let arreglo = qrs.txtarea.split(" ")

        if (arreglo.length === 1) {
            if (arreglo[0] === "") {
                toast.info("Favor de ingresar números de serie.")
                return
            }

        }

        for (const vin of arreglo) {
            objeto.push({ "vehi_serie": vin })
        }

        setNuevosqr(
            [
                ...nuevosqr,
                ...objeto
            ]
        )

        closeModalReporte();
        toast.success('Los códigos qr ingresados han sido generados correctamente.')

    }

    useEffect(() => {
        setData(nuevosqr)
    }, [nuevosqr])

    const printDocument = async () => {
        setImpresionActiva(true)

        setTimeout(() => {

            const input = document.getElementById('divToPrint');

            html2canvas(input)
                .then((canvas) => {

                    const imgData = canvas.toDataURL('image/png');
                    // let imgWidth = 210;
                    let imgWidth = 220;
                    let pageHeight = 295;
                    // let pageHeight = 295;
                    let imgHeight = canvas.height * imgWidth / canvas.width;
                    let heightLeft = imgHeight;

                    let doc = new jsPDF();

                    let position = 0;

                    doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;

                    while (heightLeft >= 0) {
                        position = heightLeft - imgHeight;
                        doc.addPage();
                        doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                        heightLeft -= pageHeight;
                    }
                    // doc.save('file.pdf');
                    doc.save(`${codigosAgenciaGenerados}.pdf`);

                    setImpresionActiva(false)
                })

        }, 2000);

    }

    const borrarObjetoQrs = async () => {
        setNuevosqr([]);
        toast.success("Los códigos QR han sido eliminados correctamente.")
    }

    const CleanDOM = () => {
        setCodigosAgenciaGenerados("Códigos QR generados")
        setData([]);
        setisVINGenerated(true)

        let select = document.getElementById('agencySelectList');
        let value = select.options[select.selectedIndex].value;
        if (value !== "0") select.selectedIndex = 0;

    }

    const selectedQRS = (e, valor) => {
        //si checked es true - insertarmos el vin en el array
        //si checked es false - filtramos el vin para extraerlo del array

        if (e.target.checked === true) {
            setCheckedQRS([
                ...checkedQRS,
                valor
            ]
            )
        }

        if (e.target.checked === false) {
            const filteredVINS = checkedQRS.filter(object => object.vehi_serie !== valor.vehi_serie)
            setCheckedQRS(filteredVINS)
        }


    }

    const selectAllQRS = () => {
        let id_tag_qrs = document.getElementById('divToPrint');

        // console.log(id_tag_qrs.children[0].children[0].childNodes[0].checked)

        if (selectAllQRSVIN === false) {
            for (const iterator of id_tag_qrs.children) {
                iterator.children[0].childNodes[0].checked = true
            }

            setCheckedQRS(data)
            setSelectAllQRSVIN(true)
        }

        if (selectAllQRSVIN === true) {
            for (const iterator of id_tag_qrs.children) {
                iterator.children[0].childNodes[0].checked = false
            }

            setCheckedQRS([])
            setSelectAllQRSVIN(false)
        }


    }

    const updateSelectAgencyList = () => {
        //función con map anidado y ciclo for of

        if ((!isVINGenerated) && (Responsable === '5')) {//&& rol es Aux contable

            const updateData1 = codigosAgencias.map((object) => {
                if (object.nombreagencia === codigosAgenciaGenerados) {
                    object.numseries.map((register) => {
                        for (const vinselected of checkedQRS) {
                            if (vinselected.vehi_serie === register.vehi_serie) {
                                register.impreso = "S"

                            }
                        }
                        return register;
                    })
                }
                return object;
            })
            // setData(updateData1[indiceSelected].numseries)
            setCodigosAgencias(updateData1);

        }

    }

    return (
        <div className='content-wrapper'>

            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-12">
                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">INVENTARIOS | CÓDIGOS QR</h5>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="d-flex align-items-center justify-content-between mt-4">
                        {/* <div className="buttonAdd__margin"> */}
                        <div className="d-flex">
                            {/* <button type='button' onClick={printDocument} className="btn btn-outline-info" disabled={impresionActiva || data.length === 0}>Imprimir PDF</button> */}
                            <div>
                                <span onClick={updateSelectAgencyList}>
                                    <PrintQRS 
                                    data={checkedQRS} 
                                    isVinGenerated={isVINGenerated} 
                                    responsable={Responsable}
                                    />
                                </span>
                            </div>


                        </div>

                        {!invActualizado ?
                            <div className='mt-2 ml-2'>
                                <Box sx={{ display: 'flex' }}>
                                    <CircularProgress />
                                </Box>
                                <p>
                                    <small>{mensajeEspera2}</small>
                                </p>
                            </div>
                            :

                            ""
                        }

                        {/* <div className="input__margin"> */}
                        <div className="form-group mb-3">
                            <label className="input-group-text font-weight-normal">Empresa | Sucursal</label>
                            <select id='agencySelectList' name='empresa' className='form-select' onChange={cambiarEmpresa} disabled={!invActualizado}>
                                {codigosAgencias.map((element, idx) => (
                                    <option value={idx}>{element.nombreagencia}</option>
                                ))}

                            </select>

                        </div>
                    </div>


                </div>
            </div>

            <div className='d-flex ml-3'>

                <div>
                    <button
                        type='button'
                        onClick={() => {
                            openModalReporte();
                            CleanDOM();
                        }
                        }
                        className="btn btn-outline-info ml-2"
                        disabled={impresionActiva || !invActualizado}>
                        Generar Nuevos QR
                    </button>

                    <button type='button'
                        onClick={borrarObjetoQrs}
                        className="btn btn-outline-danger ml-2"
                        disabled={impresionActiva || nuevosqr.length === 0}>
                        Borrar QR
                    </button>

                    <button
                        type='button'
                        onClick={selectAllQRS}
                        className="btn btn-outline-dark ml-2"
                        disabled={data.length === 0}
                    >
                        {selectAllQRSVIN ? "Desmarcar todos" : "Marcar todos"}
                    </button>



                </div>

            </div>


            <div className="container-fluid p-4 ml-4">
                <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                    <ModalGenerarQR onSubmit={nuevosCodigosQr} />
                    {/* <ModalAnticipos onSubmit={nuevoReporteAnticipo}/> */}
                </Modal>
                <div className="d-flex align-items-center justify-content-between">
                </div>
                <div className='row' id='divToPrint'>


                    {
                        data.map((valor) => {

                            return (
                                <>
                                    <div className='col-3 mt-4 mb-4'>
                                        <div className='justify-content-end'>
                                            <input
                                                type="checkbox"
                                                onChange={(e) => selectedQRS(e, valor)}
                                                className="form-check-input"
                                                name="inicio"
                                                id='checkedVins'
                                            />
                                            {
                                                valor.impreso === "S" ?
                                                    <FontAwesomeIcon color='green' title="Impreso" className='' size="lg" icon={faCheckToSlot} />
                                                    :
                                                    <FontAwesomeIcon color='gray' title="No Impreso" className='' size="lg" icon={faCheckToSlot} />

                                            }
                                        </div>

                                        <QRCode value={valor.vehi_serie} size={190} />
                                        <p className='mb-1 font__size'>{valor.vehi_serie}</p>
                                    </div>
                                </>
                            )

                        })

                    }


                </div>
            </div>

            <ToastContainer />
        </div>
    )
}

export default CodesQR;
