import React from 'react'
import { IconContext } from 'react-icons/lib'
import { FcCancel } from "react-icons/fc";
import image_gf from '../assets/images/LogoGF.png'

import '../css/Modulos.css'

const Error404 = () => {
  return (
    <IconContext.Provider value={{ color: '#000000', size: 100 }}>
      <br />
      <div className='content-wrapper '>
        <div className='container d-flex h-100 justify-content-center align-items-center'>
        <div className=' card p-2'>
        <div className="brand-link" >
          <div className="text-center">
            <img className='profile-user-img img-fluid img-circle' src={image_gf} alt="User profile picture" />
          </div>
        </div>
          {/* <center><p>Favor de consultar con su administrador para acceder a este módulo</p></center> */}
          <center><p>Lo sentimos, no cuenta con permisos de acceso. Favor de consultar con el administrador.</p></center>
        </div>
      </div>
      </div>
    </IconContext.Provider>
  )
}

export default Error404;
