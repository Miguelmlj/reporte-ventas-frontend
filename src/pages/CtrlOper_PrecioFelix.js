import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalCtrlOper_PrecioFelix } from '../modales/ModalCtrlOper_PrecioFelix';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';
import { getAsesores } from '../helpers/getAsesores';
import { activos } from '../constantes/asesores';

const CtrlOper_PrecioFelix = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [isEditMode, setIsEditMode] = useState(false)
    const [PrecioFelix, setPrecioFelix] = useState({
        Fecha: "",
        Importe_PrecioFelix: "",
        Asesor: "",
        Nombre_Cliente: "",
        Factura_PrecioFelix: "",
        Utilidad: "",
        Comision_Asesor: "",
        Utilidad_Neta: ""

    })
    const [data, setData] = useState([])
    const [asesores, setAsesores] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    useEffect(async() => {
        getPrecioFelix();
        setAsesores( await getAsesores(activos) )
    }, [])

    const getPrecioFelix = async () => {
        url = Apiurl + "api/ctrloperPrecioFelix/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createPrecioFelix = () => {
        setIsEditMode(false);
        setPrecioFelix({
            Fecha: FechaDeHoy(),
            Importe_PrecioFelix: "",
            Asesor: asesores.length > 0 ? asesores[0].Nombre_Asesor : "",
            Nombre_Cliente: "",
            Factura_PrecioFelix: "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })

        openModalReporte();
    }

    const nuevoRegistroPrecioFelix = (PFObject) => {

        if (PFObject.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(PFObject);
            return;
        }

        if (PFObject.Asesor === "") {
            toast.info("El campo Nombre Asesor se encuentra vacío.");
            setValuesWrittenForm(PFObject);
            return;
        }

        if (PFObject.Nombre_Cliente === "") {
            toast.info("El campo Nombre Cliente se encuentra vacío.");
            setValuesWrittenForm(PFObject);
            return;
        }
        
        if (PFObject.Factura_PrecioFelix === "") {
            toast.info("El campo Nombre Cliente se encuentra vacío.");
            setValuesWrittenForm(PFObject);
            return;
        }
        
        if (PFObject.Factura_PrecioFelix.length > 10) {
            toast.info("El campo Factura debe contener menos de 11 caracteres");
            setValuesWrittenForm(PFObject);
            return;
        }

        if (
            (PFObject.Importe_PrecioFelix.toString().substring(0, 1) === "-") ||
            (PFObject.Comision_Asesor.toString().substring(0, 1) === "-") ||
            // (PFObject.Factura_PrecioFelix.toString().substring(0, 1) === "-") ||
            (PFObject.Utilidad_Neta.toString().substring(0, 1) === "-") ||
            (PFObject.Utilidad.toString().substring(0, 1) === "-")
        ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(PFObject);
            return;
        }

        if (PFObject.Importe_PrecioFelix === "") PFObject.Importe_PrecioFelix = 0;
        else PFObject.Importe_PrecioFelix = removerComas( PFObject.Importe_PrecioFelix )

        if (PFObject.Comision_Asesor === "") PFObject.Comision_Asesor = 0;
        else PFObject.Comision_Asesor = removerComas( PFObject.Comision_Asesor )

        if (PFObject.Utilidad_Neta === "") PFObject.Utilidad_Neta = 0;
        else PFObject.Utilidad_Neta = removerComas( PFObject.Utilidad_Neta )

        if (PFObject.Utilidad === "") PFObject.Utilidad = 0;
        else PFObject.Utilidad = removerComas( PFObject.Utilidad )

        // if (PFObject.Factura_PrecioFelix === "") PFObject.Factura_PrecioFelix = 0;

        let body = {
            PrecioFelix: PFObject,
            agencia
        }

        if ( isEditMode ) {
            updatePrecioFelixSelected(body);
            return;
        }

        createPrecioFelixWritten(body);

    }

    const setValuesWrittenForm = (PFObject) => {
        isEditMode
            ?
            setPrecioFelix({
                Fecha: PrecioFelix.Fecha,
                Importe_PrecioFelix: PrecioFelix.Importe_PrecioFelix,
                Asesor: PrecioFelix.Asesor,
                Nombre_Cliente: PrecioFelix.Nombre_Cliente,
                Factura_PrecioFelix: PrecioFelix.Factura_PrecioFelix,
                Utilidad: PrecioFelix.Utilidad,
                Comision_Asesor: PrecioFelix.Comision_Asesor,
                Utilidad_Neta: PrecioFelix.Utilidad_Neta,
                Id: PrecioFelix.Id
            })
            :
            setPrecioFelix({
                Fecha: PFObject.Fecha,
                Importe_PrecioFelix: PFObject.Importe_PrecioFelix,
                Asesor: PFObject.Asesor,
                Nombre_Cliente: PFObject.Nombre_Cliente,
                Factura_PrecioFelix: PFObject.Factura_PrecioFelix,
                Utilidad: PFObject.Utilidad,
                Comision_Asesor: PFObject.Comision_Asesor,
                Utilidad_Neta: PFObject.Utilidad_Neta,
            })
    }

    const updatePrecioFelixSelected = async (body) => {
        url = Apiurl + "api/ctrloperPrecioFelix/update"
        await axios.patch(url, body)
            .then(response => {
                if (response['data'].isUpdated) {
                    getPrecioFelix();
                    setIsEditMode(false);
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createPrecioFelixWritten = async (body) => {
        url = Apiurl + "api/ctrloperPrecioFelix/create"
        await axios.post(url, body)
            .then(response => {

                if (response['data'].isCreated) {
                    getPrecioFelix();
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte();
                    return;
                }

                if (!response['data'].isCreated) { //body.GAP
                    setValuesWrittenForm(body.PrecioFelix)
                    toast.error("Ya existe un registro con la fecha seleccionada en la BD.")
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const editPrecioFelix = (PFObject) => {
        setIsEditMode(true);
        setPrecioFelix({
            Fecha: PFObject.Fecha.substring(0, 10),
            Importe_PrecioFelix: formatoMoneda(formatDecimalNumber(PFObject.Importe_PrecioFelix)),
            Asesor: PFObject.Asesor,
            Nombre_Cliente: PFObject.Nombre_Cliente,
            Factura_PrecioFelix: PFObject.Factura_PrecioFelix,
            Utilidad: formatoMoneda(formatDecimalNumber(PFObject.Utilidad)),
            Comision_Asesor: formatoMoneda(formatDecimalNumber(PFObject.Comision_Asesor)),
            Utilidad_Neta: formatoMoneda(formatDecimalNumber(PFObject.Utilidad_Neta)),
            Id: PFObject.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#CtrlOper_PrecioFelix').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#CtrlOper_PrecioFelix').DataTable().destroy();
    }

    return (
        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">PRECIO FÉLIX</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createPrecioFelix}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalCtrlOper_PrecioFelix
                            data={PrecioFelix}
                            onSubmit={nuevoRegistroPrecioFelix}
                            editMode={isEditMode}
                            asesores={asesores}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="CtrlOper_PrecioFelix" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" className='text-center'>Fecha</th>
                                    <th width="10%" className='text-center'>Importe Precio Félix</th>
                                    <th width="10%" className='text-center'>Asesor</th>
                                    <th width="10%" className='text-center'>Nombre Cliente</th>
                                    <th width="20%" className='text-center'>Factura Precio Félix</th>
                                    <th width="10%" className='text-center'>Utilidad</th>
                                    <th width="15%" className='text-center'>Comisión Asesor</th>
                                    <th width="10%" className='text-center'>Utilidad Neta</th>
                                    <th width="5%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((PF) => {
                                            return (
                                                <tr >
                                                    <td className='text-center'>{reverseDate(PF.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(PF.Importe_PrecioFelix))}</td>
                                                    <td className='text-center'>{PF.Asesor}</td>
                                                    <td className='text-center'>{PF.Nombre_Cliente}</td>
                                                    <td className='text-center'>{PF.Factura_PrecioFelix}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(PF.Utilidad))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(PF.Comision_Asesor))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(PF.Utilidad_Neta))}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editPrecioFelix(PF)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default CtrlOper_PrecioFelix;
