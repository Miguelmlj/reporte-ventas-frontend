import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalDetallesContratosGMF } from '../modales/ModalDetallesContratosGMF';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';

const DetallesContratosGMF = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [isEditMode, setIsEditMode] = useState(false)
    const [DetallesContratosGMF, setDetallesContratosGMF] = useState({
        Fecha: "",
        Folio_Contrato: "",
        Monto_ContratoAFinanciar: "",
        Nombre_Cliente: "",
        Utilidad: ""

    })
    const [data, setData] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    useEffect(() => {
        getDetallesContratosGMF();
    }, [])

    const getDetallesContratosGMF = async () => {
        url = Apiurl + "api/detallesContratosGMF/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createDetallesContratosGMF = () => {
        setIsEditMode(false);
        setDetallesContratosGMF({
            Fecha: FechaDeHoy(),
            Folio_Contrato: "",
            Monto_ContratoAFinanciar: "",
            Nombre_Cliente: "",
            Utilidad: ""
        })

        openModalReporte();
    }

    const nuevoRegistroDetallesContratosGMF = (DCGObject) => {

        if (DCGObject.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(DCGObject);
            return;
        }

        if (DCGObject.Folio_Contrato === "") {
            toast.info("El campo Folio Contrato se encuentra vacío.");
            setValuesWrittenForm(DCGObject);
            return;
        }

        if (DCGObject.Nombre_Cliente === "") {
            toast.info("El campo Nombre Cliente se encuentra vacío.");
            setValuesWrittenForm(DCGObject);
            return;
        }

        if (
            (DCGObject.Monto_ContratoAFinanciar.toString().substring(0, 1) === "-") ||
            (DCGObject.Utilidad.toString().substring(0, 1) === "-")
        ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(DCGObject);
            return;
        }

        if (DCGObject.Monto_ContratoAFinanciar === "") DCGObject.Monto_ContratoAFinanciar = 0;
        else DCGObject.Monto_ContratoAFinanciar = removerComas(DCGObject.Monto_ContratoAFinanciar)
        
        if (DCGObject.Utilidad === "") DCGObject.Utilidad = 0;
        else DCGObject.Utilidad = removerComas(DCGObject.Utilidad)

        let body = {
            DetallesContratosGMF: DCGObject,
            agencia
        }

        // console.log(body)

        if ( isEditMode ) {
            updateDetallesContratosGMFSelected(body);
            return;
        }

        createDetallesContratosGMFWritten(body)

    }

    const setValuesWrittenForm = (DCG) => {
        isEditMode
            ?
            setDetallesContratosGMF({
                Fecha: DetallesContratosGMF.Fecha,
                Folio_Contrato: DetallesContratosGMF.Folio_Contrato,
                Monto_ContratoAFinanciar: DetallesContratosGMF.Monto_ContratoAFinanciar,
                Nombre_Cliente: DetallesContratosGMF.Nombre_Cliente,
                Utilidad: DetallesContratosGMF.Utilidad,
                Id: DetallesContratosGMF.Id
            })
            :
            setDetallesContratosGMF({
                Fecha: DCG.Fecha,
                Folio_Contrato: DCG.Folio_Contrato,
                Monto_ContratoAFinanciar: DCG.Monto_ContratoAFinanciar,
                Nombre_Cliente: DCG.Nombre_Cliente,
                Utilidad: DCG.Utilidad
            })
    }

    const updateDetallesContratosGMFSelected = async (body) => {
        url = Apiurl + "api/detallesContratosGMF/update"
        await axios.patch(url, body)
            .then(response => {
                if (response['data'].isUpdated) {
                    getDetallesContratosGMF();
                    setIsEditMode(false);
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createDetallesContratosGMFWritten = async (body) => {
        url = Apiurl + "api/detallesContratosGMF/create"
        await axios.post(url, body)
            .then(response => {

                if (response['data'].isCreated) {
                    getDetallesContratosGMF();
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte();
                    return;
                }

                if (!response['data'].isCreated) { //body.GAP
                    setValuesWrittenForm(body.DetallesContratosGMF)
                    toast.error("Ya existe un registro con la fecha seleccionada en la BD.")
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const editDetallesContratosGMF = (DCGObject) => {
        setIsEditMode(true);
        setDetallesContratosGMF({
            Fecha: DCGObject.Fecha.substring(0, 10),
            Folio_Contrato: DCGObject.Folio_Contrato,
            Monto_ContratoAFinanciar: formatoMoneda(formatDecimalNumber(DCGObject.Monto_ContratoAFinanciar)),
            Nombre_Cliente: DCGObject.Nombre_Cliente,
            Utilidad: formatoMoneda(formatDecimalNumber(DCGObject.Utilidad)),
            Id: DCGObject.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#DetallesContratosGMF').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#DetallesContratosGMF').DataTable().destroy();
    }


    return (

        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">DETALLES DE CONTRATOS GMF</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createDetallesContratosGMF}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalDetallesContratosGMF
                            data={DetallesContratosGMF}
                            onSubmit={nuevoRegistroDetallesContratosGMF}
                            editMode={isEditMode}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="DetallesContratosGMF" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" className='text-center'>Fecha</th>
                                    <th width="10%" className='text-center'>Folio Contrato</th>
                                    <th width="20%" className='text-center'>Monto Contrato a Financiar</th>
                                    <th width="20%" className='text-center'>Nombre Cliente</th>
                                    <th width="10%" className='text-center'>Utilidad</th>
                                    <th width="5%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((DCG) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(DCG.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{DCG.Folio_Contrato}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(DCG.Monto_ContratoAFinanciar))}</td>
                                                    <td className='text-center'>{DCG.Nombre_Cliente}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(DCG.Utilidad))}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editDetallesContratosGMF(DCG)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>

    )
}

export default DetallesContratosGMF;
