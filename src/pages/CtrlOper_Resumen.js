import React, { useState, useEffect } from 'react'
import Desempeno_Asesores from '../componentes/Desempeno_Asesores'
import Control_Operativo_FYI from '../componentes/Control_Operativo_FYI'
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { year_and_month } from '../helpers/fechaHelpers';
import { getAsesores } from '../helpers/getAsesores';
import { getAsesoresPorAgencia } from '../helpers/getAsesores';
import { todos } from '../constantes/asesores';
import "../css/Resumen_CO.css"

const CtrlOper_Resumen = () => {
  const controlOperativoFYI = 1;
  const desempenoAsesores = 2;
  const Responsable = window.sessionStorage.getItem('responsable')
  const [firstComponent, setFirstComponent] = useState(true)
  const [agenciaFecha, setAgenciaFecha] = useState({
    Empresa: getEmpresa(),
    Sucursal: getSucursal(),
    Fecha: year_and_month(),
    Asesores: []
  })

  useEffect(async () => {
    const listaAsesores = await getAsesoresPorAgencia(agenciaFecha.Empresa, agenciaFecha.Sucursal)
    setAgenciaFecha({
      ...agenciaFecha,
      Asesores: listaAsesores
    })
  }, [])

  const onChange = (value) => {
    if (value === 1) {
      setFirstComponent(true)
      return;
    }
    if (value === 2) setFirstComponent(false);
  }

  const setDate = (e) => {
    setAgenciaFecha({
      ...agenciaFecha,
      [e.target.name]: e.target.value
    })
  }

  const cambiarAgencia = async (e) => {
    const empresa = e.target.value.split("-")
    const listaAsesores = await getAsesoresPorAgencia(empresa[0], empresa[1])

    setAgenciaFecha({
      ...agenciaFecha,
      Empresa: empresa[0],
      Sucursal: empresa[1],
      Asesores: listaAsesores
    })

  }

  const selectAgenciaDisabled = () => {
    if (Responsable === '7' || Responsable === '1' ) return false;
    return true;
  }

  return (
    <div className="content-wrapper">
      <div className="content-header content-header-padding">
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-12">

              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className="m-0 text-dark">RESUMEN</h5>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid">
        <div className="row ml-2 mr-2">

          <div className='flex d-flex flex-row-reverse'>

            <div className="form-group input-agency">
              <small >Seleccionar agencia:</small>
              <select name='empresa' className='form-select' onChange={cambiarAgencia} disabled={selectAgenciaDisabled()}>
                <option value="1-1" selected={agenciaFecha.Empresa === '1' && agenciaFecha.Sucursal === '1'}>Mochis</option>
                <option value="5-1" selected={agenciaFecha.Empresa === '5' && agenciaFecha.Sucursal === '1'}>Culiacán Zapata</option>
                <option value="5-2" selected={agenciaFecha.Empresa === '5' && agenciaFecha.Sucursal === '2'}>Culiacán Aeropuerto</option>
                <option value="3-1" selected={agenciaFecha.Empresa === '3' && agenciaFecha.Sucursal === '1'}>Guasave</option>
                <option value="7-1" selected={agenciaFecha.Empresa === '7' && agenciaFecha.Sucursal === '1'}>Cadillac</option>
              </select>
            </div>

            <div className='input-month-year'>
              <small >Seleccionar Periodo:</small>
              <input type="month" className="form-control" min="2012-01" onChange={setDate} value={agenciaFecha.Fecha} name="Fecha" />
            </div>

          </div>


          <ul className="nav nav-tabs mb-3" >

            <li className="nav-item" role="presentation" onClick={() => onChange(controlOperativoFYI)}>
              <a className={firstComponent ? "nav-link handpointer active background-tabs" : "nav-link handpointer"}>
                <i className="fas fa-money-check" />
                <small className='ml-2'>Control Operativo F&I</small>
              </a>
            </li>

            <li className="nav-item" role="presentation" onClick={() => onChange(desempenoAsesores)}>
              <a className={!firstComponent ? "nav-link handpointer active background-tabs" : "nav-link handpointer"}>
                <i className="fas fa-user-check" />
                <small className='ml-2'>Desempeño Asesores</small>
              </a>
            </li>

          </ul>

          {
            firstComponent
              ? <Control_Operativo_FYI agencia={agenciaFecha} />
              : <Desempeno_Asesores agencia={agenciaFecha} />
          }
        </div>
      </div>
    </div>
  )
}

export default CtrlOper_Resumen