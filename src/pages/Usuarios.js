import React, { useEffect, useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';

import { Apiurl } from '../services/Apirest';
import { useModal } from '../hooks/useModal';
import { confDataTableUsuarios } from '../componentes/ConfDataTable';
import Modal from '../componentes/Modal';
import { ModalUsuarios } from '../modales/ModalUsuarios';
import '../css/Modulos.css'
import axios from 'axios';
// import _ from "lodash";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRotate } from '@fortawesome/free-solid-svg-icons';
import $ from 'jquery';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Usuarios = () => {
  const [data, setData] = useState([])
  const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
  const [usersFilteredByAgency, setUsersFilteredByAgency] = useState([])
  const [userRols, setUserRols] = useState([])
  const [isSynchronizing, setIsSynchronizing] = useState(false)
  const [isLoadingUsersTable, setIsLoadingUsersTable] = useState(false)

  const [modulos, setModulos] = useState({
    inicio: "N",
    afluencia: "N",
    citas: "N",
    solicitudes: "N",
    anticipos: "N",
    inventario: "N",
    codigosqr: "N",
    usuarios: "N",
    rol: 1,
    vehiculos: 1
  })


  useEffect(() => {
    // peticionGetUsuarios();
    getUserRols();
  }, [])

  const peticionGetUsuarios = async () => {
    setIsLoadingUsersTable(true)
    await axios.get(url)
      .then(response => {
        setIsLoadingUsersTable(false)
        dataTable()
        setData(response['data']);
        // console.log(response['data']);
        setUsersFilteredByAgency(response['data']);

      })
  }

  const getUserRols = async () => {
    let url = Apiurl + "api/usuarios/UserRols";
    axios.get(url)
      .then(response => {
        setUserRols(response['data'])
        peticionGetUsuarios()

      })
      .catch(err => {
        toast.info("Ocurrió un error al obtener los roles de usuario.")
      })
  }

  const dataTable = () => {
    $(document).ready(function () {
      $('#tblusers').DataTable(
        confDataTableUsuarios
      );

    });

  }

  const dataTableDestroy = () => {
    $('#tblusers').DataTable().destroy();

  }

  let url = Apiurl + "api/usuarios";
  let url2 = Apiurl + "api/usuariosbynombre";

  const nuevoUsuario = async (newuser) => {

    let nuevousuario = {
      //LA EMPRESA Y SUCURSAL SON ASIGNADAS SEGÚN EL ADMINISTRADOR DE LA AGENCIA LOGUEADO
      nombre_usuario: newuser.nombre_usuario.trim().toLowerCase(),
      clave: newuser.clave.trim(),
      empresa: window.sessionStorage.getItem('empresa'),
      sucursal: window.sessionStorage.getItem('sucursal'),
      tipo_usuario: newuser.tipo, //tipo automovil
      responsable: newuser.responsable
    }

    //AQUI PODEMOS HACER MÁS VALIDACIONES DEPENDE EL USUARIO QUE SE CREE
    if (nuevousuario.nombre_usuario.trim() === "") {
      toast.info("Debe ingresar un nombre de usuario");

      return
    } else if (nuevousuario.clave.trim() === "") {
      toast.info("Debe ingresar una contraseña");

      return
    } else if (nuevousuario.responsable === "1" && nuevousuario.tipo_usuario === "2") {
      toast.info("Favor de registrar usuario administrador con tipo automóvil: ambos");
      return
    } else if (nuevousuario.responsable === "1" && nuevousuario.tipo_usuario === "3") {
      toast.info("Favor de registrar usuario administrador con tipo automóvil: ambos");
      return
    } else if (nuevousuario.responsable === "2" && nuevousuario.tipo_usuario === "1") {
      toast.info("Favor de registrar usuario Hostess con tipo automóvil: nuevos o seminuevos");
      return
    } else if (nuevousuario.responsable === "3" && nuevousuario.tipo_usuario === "1") {
      toast.info("Favor de registrar usuario BDC con tipo automóvil: nuevos");
      return
    } else if (nuevousuario.responsable === "3" && nuevousuario.tipo_usuario === "3") {
      toast.info("Favor de registrar usuario BDC con tipo automóvil: nuevos");
      return
    } else if (nuevousuario.responsable === "4" && nuevousuario.tipo_usuario === "3") {
      toast.info("Favor de registrar usuario F&I con tipo automóvil: nuevos");
      return
    } else if (nuevousuario.responsable === "4" && nuevousuario.tipo_usuario === "1") {
      toast.info("Favor de registrar usuario F&I con tipo automóvil: nuevos");
      return
    }

    //hacer la petición axios para crear el nuevo usuario

    // console.log(nuevousuario)

    await axios.post(url2, nuevousuario)
      .then(response => {
        if (`${response['statusText']}` === 'OK' && (response['data'].message === 'usuarioexiste')) {
          toast.info("El usuario ya existe, por favor intente con un nombre distinto");

        } else if (`${response['statusText']}` === 'OK' && (response['data'].message === 'usuarionoexiste')) {
          // toast.success("Usuario no existe");
          confirmarAgregarNuevoUsuario(nuevousuario)

        }

      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })

  }

  const confirmarAgregarNuevoUsuario = async (nuevousuario) => {
    await axios.post(url, nuevousuario)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          closeModalReporte();
          peticionGetUsuarios();
          toast.success("Usuario creado exitosamente");
        }

      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })

  }

  //OPCIONES
  //1.- manejar dos funciones, una para los combo select, dropdown || otra para los combo box
  //2.- poner condicional dentro de una función, si el valor name es afluencia, citas etc...

  //const handleComboSelect
  const handlePermissions = async (e, usuario) => {


    if ("afluencia" === e.target.name) {
      const updatedUsers = data.map((user) => {
        //si cumple la siguiente condición: modifica la propiedad afluencia de ese usuario.
        if (user.id_usuario === usuario.id_usuario) {
          if (user.afluencia === "N") {
            usuario.afluencia = "S"
            return {
              ...user,
              afluencia: "S"
            }

          } else if (user.afluencia === "S") {
            usuario.afluencia = "N"
            return {
              ...user,
              afluencia: "N"
            }
          }
        }
        //regresamos el usuario igual o modificado, según sea el caso.
        return user
      })
      setData(updatedUsers)
    }

    if ("anticipos" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.anticipos === "N") {
            usuario.anticipos = "S"
            return {
              ...user,
              anticipos: "S"
            }

          } else if (user.anticipos === "S") {
            usuario.anticipos = "N"
            return {
              ...user,
              anticipos: "N"
            }
          }
        }

        return user
      })

      setData(updatedUsers)
    }

    if ("citas" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.citas === "N") {
            usuario.citas = "S"
            return {
              ...user,
              citas: "S"
            }

          } else if (user.citas === "S") {
            usuario.citas = "N"
            return {
              ...user,
              citas: "N"
            }
          }
        }

        return user
      })

      setData(updatedUsers)
    }

    if ("codigosQr" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.codigosQr === "N") {
            usuario.codigosQr = "S"
            return {
              ...user,
              codigosQr: "S"
            }

          } else if (user.codigosQr === "S") {
            usuario.codigosQr = "N"
            return {
              ...user,
              codigosQr: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers)
    }

    if ("inicio" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.inicio === "N") {
            usuario.inicio = "S"
            return {
              ...user,
              inicio: "S"
            }

          } else if (user.inicio === "S") {
            usuario.inicio = "N"
            return {
              ...user,
              inicio: "N"
            }
          }
        }

        return user
      })

      setData(updatedUsers)
    }

    if ("invfisico" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.invfisico === "N") {
            usuario.invfisico = "S"
            return {
              ...user,
              invfisico: "S"
            }

          } else if (user.invfisico === "S") {
            usuario.invfisico = "N"
            return {
              ...user,
              invfisico: "N"
            }
          }
        }

        return user
      })

      setData(updatedUsers)
    }

    if ("responsable" === e.target.name) {
      const permissions = userRols.filter(element => element.Id_responsable == e.target.value)

      if (permissions.length === 1) {
        const updatedUsers = data.map((user) => {

          if (user.id_usuario === usuario.id_usuario) {
            usuario.responsable = permissions[0].Id_responsable
            usuario.inicio = permissions[0].inicio
            usuario.afluencia = permissions[0].afluencia
            usuario.citas = permissions[0].citas
            usuario.solicitudes = permissions[0].solicitudes
            usuario.anticipos = permissions[0].anticipos
            usuario.invfisico = permissions[0].invfisico
            usuario.codigosQr = permissions[0].codigosQr
            usuario.usuarios = permissions[0].usuarios

            usuario.indFYI = permissions[0].indFYI
            usuario.indLeads = permissions[0].indLeads
            usuario.indCentroProp = permissions[0].indCentroProp
            usuario.indJDPower = permissions[0].indJDPower
            usuario.indBDC = permissions[0].indBDC
            usuario.indMkt = permissions[0].indMkt

            usuario.CO_SegGMF = permissions[0].CO_SegGMF
            usuario.CO_GarGMPlus = permissions[0].CO_GarGMPlus
            usuario.CO_OnStrt = permissions[0].CO_OnStrt
            usuario.CO_GAP = permissions[0].CO_GAP
            usuario.CO_Accsrios = permissions[0].CO_Accsrios
            usuario.CO_PrecioFlx = permissions[0].CO_PrecioFlx
            usuario.CO_GarPlus = permissions[0].CO_GarPlus
            usuario.CO_SegInbros = permissions[0].CO_SegInbros
            usuario.CO_PrecioFlxCntdo = permissions[0].CO_PrecioFlxCntdo
            usuario.DtllsCntrtsGMF = permissions[0].DtllsCntrtsGMF
            usuario.CO_Resumen = permissions[0].CO_Resumen
            usuario.Asig_Contratos = permissions[0].Asig_Contratos
            usuario.Clientes_Flotillas = permissions[0].Clientes_Flotillas
            usuario.OrdenDeCompra_F = permissions[0].OrdenDeCompra_F

            return {
              ...user,
              responsable: permissions[0].Id_responsable,
              inicio: permissions[0].inicio,
              afluencia: permissions[0].afluencia,
              citas: permissions[0].citas,
              solicitudes: permissions[0].solicitudes,
              anticipos: permissions[0].anticipos,
              invfisico: permissions[0].invfisico,
              codigosQr: permissions[0].codigosQr,
              usuarios: permissions[0].usuarios,

              indFYI: permissions[0].indFYI,
              indLeads: permissions[0].indLeads,
              indCentroProp: permissions[0].indCentroProp,
              indJDPower: permissions[0].indJDPower,
              indBDC: permissions[0].indBDC,
              indMkt: permissions[0].indMkt,

              CO_SegGMF: permissions[0].CO_SegGMF,
              CO_GarGMPlus: permissions[0].CO_GarGMPlus,
              CO_OnStrt: permissions[0].CO_OnStrt,
              CO_GAP: permissions[0].CO_GAP,
              CO_Accsrios: permissions[0].CO_Accsrios,
              CO_PrecioFlx: permissions[0].CO_PrecioFlx,
              CO_GarPlus: permissions[0].CO_GarPlus,
              CO_SegInbros: permissions[0].CO_SegInbros,
              CO_PrecioFlxCntdo: permissions[0].CO_PrecioFlxCntdo,
              DtllsCntrtsGMF: permissions[0].DtllsCntrtsGMF,
              CO_Resumen: permissions[0].CO_Resumen,

              //flotillas
              Asig_Contratos: permissions[0].Asig_Contratos,
              Clientes_Flotillas: permissions[0].Clientes_Flotillas,
              OrdenDeCompra_F: permissions[0].OrdenDeCompra_F

            }

          }
          //regresamos el usuario igual o modificado, según sea el caso.
          return user
        })
        setData(updatedUsers)
      }

    }

    if ("solicitudes" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.solicitudes === "N") {
            usuario.solicitudes = "S"
            return {
              ...user,
              solicitudes: "S"
            }

          } else if (user.solicitudes === "S") {
            usuario.solicitudes = "N"
            return {
              ...user,
              solicitudes: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers)
    }

    if ("tipo_usuario" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          usuario.tipo_usuario = e.target.value
          return {
            ...user,
            tipo_usuario: e.target.value
          }
        }

        return user
      })
      setData(updatedUsers)
    }

    if ("usuarios" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.usuarios === "N") {
            usuario.usuarios = "S"
            return {
              ...user,
              usuarios: "S"
            }

          } else if (user.usuarios === "S") {
            usuario.usuarios = "N"
            return {
              ...user,
              usuarios: "N"
            }
          }
        }
        return user
      })

      setData(updatedUsers)
    }

    if ("indFYI" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.indFYI === "N") {
            usuario.indFYI = "S"
            return {
              ...user,
              indFYI: "S"
            }

          } else if (user.indFYI === "S") {
            usuario.indFYI = "N"
            return {
              ...user,
              indFYI: "N"
            }
          }
        }

        return user
      })

      setData(updatedUsers)
    }

    if ("indLeads" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.indLeads === "N") {
            usuario.indLeads = "S"
            return {
              ...user,
              indLeads: "S"
            }

          } else if (user.indLeads === "S") {
            usuario.indLeads = "N"
            return {
              ...user,
              indLeads: "N"
            }
          }
        }

        return user

      })
      setData(updatedUsers)
    }

    if ("indCentroProp" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.indCentroProp === "N") {
            usuario.indCentroProp = "S"
            return {
              ...user,
              indCentroProp: "S"
            }

          } else if (user.indCentroProp === "S") {
            usuario.indCentroProp = "N"
            return {
              ...user,
              indCentroProp: "N"
            }
          }
        }
        return user

      })

      setData(updatedUsers)
    }

    if ("indJDPower" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.indJDPower === "N") {
            usuario.indJDPower = "S"
            return {
              ...user,
              indJDPower: "S"
            }

          } else if (user.indJDPower === "S") {
            usuario.indJDPower = "N"
            return {
              ...user,
              indJDPower: "N"
            }
          }
        }
        return user
      })
      setData(updatedUsers)
    }

    if ("indBDC" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.indBDC === "N") {
            usuario.indBDC = "S"
            return {
              ...user,
              indBDC: "S"
            }

          } else if (user.indBDC === "S") {
            usuario.indBDC = "N"
            return {
              ...user,
              indBDC: "N"
            }
          }
        }
        return user

      })
      setData(updatedUsers)
    }

    if ("indMkt" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.indMkt === "N") {
            usuario.indMkt = "S"
            return {
              ...user,
              indMkt: "S"
            }

          } else if (user.indMkt === "S") {
            usuario.indMkt = "N"
            return {
              ...user,
              indMkt: "N"
            }
          }
        }
        return user
      })
      setData(updatedUsers)
    }

    if ("CO_SegGMF" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_SegGMF === "N") {
            usuario.CO_SegGMF = "S"
            return {
              ...user,
              CO_SegGMF: "S"
            }

          } else if (user.CO_SegGMF === "S") {
            usuario.CO_SegGMF = "N"
            return {
              ...user,
              CO_SegGMF: "N"
            }
          }
        }
        return user
      })
      setData(updatedUsers);
    }

    if ("CO_GarGMPlus" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_GarGMPlus === "N") {
            usuario.CO_GarGMPlus = "S"
            return {
              ...user,
              CO_GarGMPlus: "S"
            }

          } else if (user.CO_GarGMPlus === "S") {
            usuario.CO_GarGMPlus = "N"
            return {
              ...user,
              CO_GarGMPlus: "N"
            }
          }
        }
        return user
      })
      setData(updatedUsers);
    }

    if ("CO_OnStrt" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_OnStrt === "N") {
            usuario.CO_OnStrt = "S"
            return {
              ...user,
              CO_OnStrt: "S"
            }

          } else if (user.CO_OnStrt === "S") {
            usuario.CO_OnStrt = "N"
            return {
              ...user,
              CO_OnStrt: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    if ("CO_GAP" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_GAP === "N") {
            usuario.CO_GAP = "S"
            return {
              ...user,
              CO_GAP: "S"
            }

          } else if (user.CO_GAP === "S") {
            usuario.CO_GAP = "N"
            return {
              ...user,
              CO_GAP: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    if ("CO_Accsrios" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_Accsrios === "N") {
            usuario.CO_Accsrios = "S"
            return {
              ...user,
              CO_Accsrios: "S"
            }

          } else if (user.CO_Accsrios === "S") {
            usuario.CO_Accsrios = "N"
            return {
              ...user,
              CO_Accsrios: "N"
            }
          }
        }
        return user
      })
      setData(updatedUsers);
    }

    if ("CO_PrecioFlx" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_PrecioFlx === "N") {
            usuario.CO_PrecioFlx = "S"
            return {
              ...user,
              CO_PrecioFlx: "S"
            }
          } else if (user.CO_PrecioFlx === "S") {
            usuario.CO_PrecioFlx = "N"
            return {
              ...user,
              CO_PrecioFlx: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    if ("CO_GarPlus" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_GarPlus === "N") {
            usuario.CO_GarPlus = "S"
            return {
              ...user,
              CO_GarPlus: "S"
            }

          } else if (user.CO_GarPlus === "S") {
            usuario.CO_GarPlus = "N"
            return {
              ...user,
              CO_GarPlus: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    if ("CO_SegInbros" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_SegInbros === "N") {
            usuario.CO_SegInbros = "S"
            return {
              ...user,
              CO_SegInbros: "S"
            }

          } else if (user.CO_SegInbros === "S") {
            usuario.CO_SegInbros = "N"
            return {
              ...user,
              CO_SegInbros: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    if ("CO_PrecioFlxCntdo" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_PrecioFlxCntdo === "N") {
            usuario.CO_PrecioFlxCntdo = "S"
            return {
              ...user,
              CO_PrecioFlxCntdo: "S"
            }

          } else if (user.CO_PrecioFlxCntdo === "S") {
            usuario.CO_PrecioFlxCntdo = "N"
            return {
              ...user,
              CO_PrecioFlxCntdo: "N"
            }
          }
        }
        return user
      })
      setData(updatedUsers);
    }

    if ("DtllsCntrtsGMF" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.DtllsCntrtsGMF === "N") {
            usuario.DtllsCntrtsGMF = "S"
            return {
              ...user,
              DtllsCntrtsGMF: "S"
            }

          } else if (user.DtllsCntrtsGMF === "S") {
            usuario.DtllsCntrtsGMF = "N"
            return {
              ...user,
              DtllsCntrtsGMF: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    if ("CO_Resumen" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.CO_Resumen === "N") {
            usuario.CO_Resumen = "S"
            return {
              ...user,
              CO_Resumen: "S"
            }

          } else if (user.CO_Resumen === "S") {
            usuario.CO_Resumen = "N"
            return {
              ...user,
              CO_Resumen: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    if ("Asig_Contratos" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.Asig_Contratos === "N") {
            usuario.Asig_Contratos = "S"
            return {
              ...user,
              Asig_Contratos: "S"
            }

          } else if (user.Asig_Contratos === "S") {
            usuario.Asig_Contratos = "N"
            return {
              ...user,
              Asig_Contratos: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }
    
    if ("Clientes_Flotillas" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.Clientes_Flotillas === "N") {
            usuario.Clientes_Flotillas = "S"
            return {
              ...user,
              Clientes_Flotillas: "S"
            }

          } else if (user.Clientes_Flotillas === "S") {
            usuario.Clientes_Flotillas = "N"
            return {
              ...user,
              Clientes_Flotillas: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    if("OrdenDeCompra_F" === e.target.name) {
      const updatedUsers = data.map((user) => {
        if (user.id_usuario === usuario.id_usuario) {
          if (user.OrdenDeCompra_F === "N") {
            usuario.OrdenDeCompra_F = "S"
            return {
              ...user,
              OrdenDeCompra_F: "S"
            }

          } else if (user.OrdenDeCompra_F === "S") {
            usuario.OrdenDeCompra_F = "N"
            return {
              ...user,
              OrdenDeCompra_F: "N"
            }
          }
        }

        return user
      })
      setData(updatedUsers);
    }

    /* Después de pasar por las validaciones correspondientes, realizamos la petición Axios. */

    let url = Apiurl + "api/usuarios/update/"
    axios.patch(url, usuario)
      .then(response => {
        if (response['data'].isUpdated) {
          // peticionGetUsuarios()
          toast.success(`El usuario ${usuario.nombre_usuario} ha sido actualizado existosamente.`)
        }
      })
      .catch(err => {
        toast.error("Ocurrió un error con el servidor al actualizar permisos")
      })


  }

  const ExistsUsersDMS = async () => {
    setIsSynchronizing(true);
    let url = Apiurl + "api/usuarios/ExistsUsersDMS";

    axios.get(url)
      .then(response => {
        if (response['data'].message === "existen") {
          DeleteUsersDMS();

        } else if (response['data'].message === "noexisten") {
          CreateUsersDMS()

        }

      })
      .catch(err => {
        console.log(err);
      })
  }

  const CreateUsersDMS = async () => {
    let url = Apiurl + "api/usuarios/CreateUsersDMS"

    axios.get(url)
      .then(response => {
        if (response['data'].isCreated === true) {
          CreateUsersCapturas()

        }

        if (response['data'].isCreated === false) {
          toast.error("No es posible realizar la sincronización")
          setIsSynchronizing(false);

        }
      })
      .catch(err => {
        toast.error("Ocurrió un error con el servidor.")
      })

  }

  const CreateUsersCapturas = async () => {
    let url = Apiurl + "api/usuarios/InsertNewUsers";

    const UsersDMSAndUserRols = {
      // "UsersDMS": jsonDMSUsers,
      // "UsersDMS": UsersDMS,
      "UserRols": userRols
    }

    axios.post(url, UsersDMSAndUserRols)
      .then(response => {
        if (response['data'].isSinchronizationCompleted) {
          peticionGetUsuarios()
          toast.info("La sincronización ha sido completada satisfactoriamente.")
          setIsSynchronizing(false);

        }

      })
      .catch(err => {
        toast.error("Ocurrió un error con el servidor.")
      })

  }

  const DeleteUsersDMS = async () => {
    let url = Apiurl + "api/usuarios/DeleteUsersDMS"

    axios.delete(url)
      .then(response => {
        if (response['data'].isDeleted === true) {
          CreateUsersDMS();
        }
      })
      .catch(err => {
        toast.error("Ocurrió un error con el servidor.")
      })

  }

  const filterUsersByAgency = (e) => {
    /* e.target.value === "1" && console.log("mochis");
    e.target.value === "3" && console.log("guasave");
    e.target.value === "5" && console.log("culiacan");
    e.target.value === "7" && console.log("cadillac"); */

    const filteredUsers = usersFilteredByAgency.filter(user => user.Empresa == e.target.value)
    dataTableDestroy();
    setData(filteredUsers);
    dataTable();
    // console.log("filteredUsers", filteredUsers);

  }

  //RENDERIZADO
  return (
    <div className='content-wrapper'>

      {/* prueba con datatable */}

      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-12">
              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className="m-0 text-dark">USUARIOS</h5>
                </div>
              </div>

            </div>
          </div>
          {/* <button type='button' onClick={openModalReporte} className="btn btn-outline-info mt-4">Registrar Usuario</button> */}
          {/* <button type='button' className="btn btn-outline-info mt-4"><FontAwesomeIcon icon={faRotate}></FontAwesomeIcon><small className='ml-2'>Sincronizar</small></button> */}
        </div>
      </div>

      <div className="container-fluid">

        <div className="d-flex align-items-center justify-content-between">
          <div className="d-flex">
            <button type='button' onClick={ExistsUsersDMS} disabled={isSynchronizing || isLoadingUsersTable} className="btn btn-outline-info mb-4"><FontAwesomeIcon icon={faRotate}></FontAwesomeIcon><small className='ml-2'>Sincronizar</small></button>
            {isSynchronizing ?
              <div className='mt-2 ml-2'>
                <Box sx={{ display: 'flex' }}>
                  <CircularProgress />
                </Box>
                <p>
                  <small>{"Sincronizando..."}</small>
                </p>
              </div>
              :

              ""
            }

            {isLoadingUsersTable ?
              <div className='mt-2 ml-2'>
                <Box sx={{ display: 'flex' }}>
                  <CircularProgress />
                </Box>
                <p>
                  <small>{"Cargando tabla.."}</small>
                </p>
              </div>
              :

              ""
            }
          </div>

          <div className="form-group">
            <label className="input-group-text font-weight-normal">Agencia</label>
            <select name='empresa' className='form-select' onChange={(e) => filterUsersByAgency(e)} disabled={isSynchronizing || isLoadingUsersTable}>

              <option value={-1}>Seleccionar Agencia</option>
              <option value={1}>Mochis</option>
              <option value={3}>Guasave</option>
              <option value={5}>Culiacán</option>
              <option value={7}>Cadillac</option>
            </select>
          </div>
        </div>


        <div className="row">
          <div className='col-md-12 col-xs-5 col-sm-12 table-responsive  mb-4'>
            <table id="tblusers" className="table table-bordered table-striped">
              <thead>
                <tr>
                  <th colspan="12"></th>
                  <th colspan="6">INDICADORES</th>
                  <th colspan="11">CONTROL OPERATIVO</th>
                  <th colspan="3">FLOTILLAS</th>
                </tr>
                <tr>
                  {/* <th>#</th> */}
                  <th className='text-center'>Empresa</th>
                  <th className='text-center'>Usuario</th>
                  <th className='text-center'>Rol/Perfil  </th>
                  <th className='text-center'>Vehículos</th>
                  <th className='text-center'>Inicio</th>
                  <th className='text-center'>Afluencia</th>
                  <th className='text-center'>Citas</th>
                  <th className='text-center'>Solicitudes</th>
                  <th className='text-center'>Anticipos</th>
                  <th className='text-center'>Inventario</th>
                  <th className='text-center'>Códigos QR</th>
                  <th className='text-center'>Usuarios</th>

                  <th className='text-center'>FYI</th>
                  <th className='text-center'>Leads Contact.</th>
                  <th className='text-center'>CentroProp</th>
                  <th className='text-center'>JDPower</th>
                  <th className='text-center'>BDC</th>
                  <th className='text-center'>Mkt</th>

                  <th className='text-center'>Seguro GMF</th>
                  <th className='text-center'>Garantía GMPlus</th>
                  <th className='text-center'>OnStar</th>
                  <th className='text-center'>GAP</th>
                  <th className='text-center'>Accesorios</th>
                  <th className='text-center'>PrecioFlx</th>
                  <th className='text-center'>Garantía Plus</th>
                  <th className='text-center'>Seguro Inbros</th>
                  <th className='text-center'>Precio FlxCntdo</th>
                  <th className='text-center'>Detalles Contratos GMF</th>
                  <th className='text-center'>Resumen CO</th>
                  <th className='text-center'>Asignación Contratos</th>
                  <th className='text-center'>Clientes Flotillas</th>
                  <th className='text-center'>Orden De Compra</th>
                </tr>
              </thead>
              <tbody>
                {

                  data.map((user) => {
                    return (
                      <tr>
                        <td className='text-center'>{user.Empresa === 1 ? "MOCHIS" : user.Empresa === 3 ? "GUASAVE" : user.Empresa === 5 ? "CULIACÁN" : user.Empresa === 7 && "CADILLAC"}</td>
                        <td className='text-center'>{user.nombre_usuario}</td>
                        <td className='text-center'>
                          <select name='responsable' className='form-select' onChange={(e) => handlePermissions(e, user)}>

                            {userRols.map((rol) => {
                              return (
                                <option value={rol.Id_responsable} selected={user.responsable === rol.Id_responsable}>{rol.nombre}</option>
                              )
                            })}

                          </select>
                        </td>

                        <td className='text-center'>
                          <select name='tipo_usuario' className='form-select' onChange={(e) => handlePermissions(e, user)}>
                            <option value={1} selected={user.tipo_usuario === 1}>Ambos</option>
                            <option value={2} selected={user.tipo_usuario === 2}>Nuevos</option>
                            <option value={3} selected={user.tipo_usuario === 3}>Seminuevos</option>
                          </select>

                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.inicio === "S"} type="checkbox" className="form-check-input" name="inicio" value="inicio" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.afluencia === "S"} type="checkbox" className="form-check-input" name="afluencia" value="afluencia" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.citas === "S"} type="checkbox" className="form-check-input" name="citas" value="citas" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.solicitudes === "S"} type="checkbox" className="form-check-input" name="solicitudes" value="solicitudes" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.anticipos === "S"} type="checkbox" className="form-check-input" name="anticipos" value="anticipos" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.invfisico === "S"} type="checkbox" className="form-check-input" name="invfisico" value="invfisico" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.codigosQr === "S"} type="checkbox" className="form-check-input" name="codigosQr" value="codigosQr" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.usuarios === "S"} type="checkbox" className="form-check-input" name="usuarios" value="usuarios" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.indFYI === "S"} type="checkbox" className="form-check-input" name="indFYI" value="indFYI" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.indLeads === "S"} type="checkbox" className="form-check-input" name="indLeads" value="indLeads" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.indCentroProp === "S"} type="checkbox" className="form-check-input" name="indCentroProp" value="indCentroProp" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.indJDPower === "S"} type="checkbox" className="form-check-input" name="indJDPower" value="indJDPower" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.indBDC === "S"} type="checkbox" className="form-check-input" name="indBDC" value="indBDC" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.indMkt === "S"} type="checkbox" className="form-check-input" name="indMkt" value="indMkt" />
                        </td>

                        {/* CONTROL OPERATIVO F&I */}
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_SegGMF === "S"} type="checkbox" className="form-check-input" name="CO_SegGMF" value="CO_SegGMF" />
                        </td>

                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_GarGMPlus === "S"} type="checkbox" className="form-check-input" name="CO_GarGMPlus" value="CO_GarGMPlus" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_OnStrt === "S"} type="checkbox" className="form-check-input" name="CO_OnStrt" value="CO_OnStrt" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_GAP === "S"} type="checkbox" className="form-check-input" name="CO_GAP" value="CO_GAP" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_Accsrios === "S"} type="checkbox" className="form-check-input" name="CO_Accsrios" value="CO_Accsrios" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_PrecioFlx === "S"} type="checkbox" className="form-check-input" name="CO_PrecioFlx" value="CO_PrecioFlx" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_GarPlus === "S"} type="checkbox" className="form-check-input" name="CO_GarPlus" value="CO_GarPlus" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_SegInbros === "S"} type="checkbox" className="form-check-input" name="CO_SegInbros" value="CO_SegInbros" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_PrecioFlxCntdo === "S"} type="checkbox" className="form-check-input" name="CO_PrecioFlxCntdo" value="CO_PrecioFlxCntdo" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.DtllsCntrtsGMF === "S"} type="checkbox" className="form-check-input" name="DtllsCntrtsGMF" value="DtllsCntrtsGMF" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.CO_Resumen === "S"} type="checkbox" className="form-check-input" name="CO_Resumen" value="CO_Resumen" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.Asig_Contratos === "S"} type="checkbox" className="form-check-input" name="Asig_Contratos" value="Asig_Contratos" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.Clientes_Flotillas === "S"} type="checkbox" className="form-check-input" name="Clientes_Flotillas" value="Clientes_Flotillas" />
                        </td>
                        <td className='text-center'>
                          <input onChange={(e) => handlePermissions(e, user)} checked={user.OrdenDeCompra_F === "S"} type="checkbox" className="form-check-input" name="OrdenDeCompra_F" value="OrdenDeCompra_F" />
                        </td>
                      </tr>
                    )
                  })
                }

              </tbody>
            </table>
          </div>

        </div>
        <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
          <ModalUsuarios onSubmit={nuevoUsuario} />
        </Modal>
        <ToastContainer />
      </div>

    </div>
  )
}

export default Usuarios;
