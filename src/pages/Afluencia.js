import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { useModal } from '../hooks/useModal';
import Modal from '../componentes/Modal';
import { ToastContainer, toast } from 'react-toastify';
import $ from 'jquery';

import '../css/Modulos.css'
import { Apiurl } from '../services/Apirest';
import { confDataTable } from '../componentes/ConfDataTable';
import { reverseDate } from '../componentes/OrdenarFecha';
import { ModalAfluencia } from '../modales/ModalAfluencia';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

const Afluencia = () => {
  const [data, setData] = useState([])
  const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
  const [agenciaAfluencia, setagenciaAfluencia] = useState("");
  const [hostessNuevos, setHostessNuevos] = useState(false)
  const [hostessSeminuevos, setHostessSeminuevos] = useState(false)
  const [administrador, setAdministrador] = useState(false)
  const [isEditMode, setIsEditMode] = useState(false)
  const [afluencia, setAfluencia] = useState({
    id_fecha: "",
    af_fresh_n: "",
    af_bback_n: "",
    af_primera_cita_n: "",
    af_digitales_n: "",

    af_fresh_s: "",
    af_bback_s: "",
    af_primera_cita_s: "",
    af_digitales_s: ""
  })

  let objusuario = {
    user: window.sessionStorage.getItem('usuario'),
    empresa: window.sessionStorage.getItem('empresa'),
    sucursal: window.sessionStorage.getItem('sucursal'),
    tipo_usuario: window.sessionStorage.getItem('tipo_usuario')
  }

  useEffect(() => {
    asignaTipoUsuarioLogueado()
    peticionGetReporte();
    peticionNombreAgencia()

  }, [])

  const asignaTipoUsuarioLogueado = () => {
    //1.- Administrador, 2.-Nuevos, 3.- Seminuevos
    if (window.sessionStorage.getItem('tipo_usuario') === '1') {
      setAdministrador(true)
      return
    }

    if (window.sessionStorage.getItem('tipo_usuario') === '2') {
      setHostessNuevos(true)

    } else if (window.sessionStorage.getItem('tipo_usuario') === '3') {
      setHostessSeminuevos(true)
    }

  }


  const peticionGetReporte = async () => {
    let url1 = Apiurl + "api/afluencia/";

    await axios.post(url1, objusuario)
      .then(response => {
        dataTableDestroy()
        setData(response['data']);
        dataTable()

      }).catch(error => console.log(error))
  }

  const peticionNombreAgencia = async () => {
    let urlAgencia = Apiurl + "api/inicio/agencia";

    await axios.post(urlAgencia, objusuario)
      .then(response => {
        setagenciaAfluencia(response['data'].Nombre)

      })
      .catch(error => {
        toast.error("Error al obtener nombre de agencia");
      })

  }

  const dataTable = () => {
    $(document).ready(function () {
      $('#afluencia').DataTable(
        confDataTable
      );

    });
  }

  const dataTableDestroy = () => {
    $('#afluencia').DataTable().destroy();
  }

  const nuevoReporte = async (newRep) => {

    if (newRep.id_fecha === "") {
      toast.info('Campo fecha vacío')
      setValuesWrittenForm(newRep);
      return
    }

    if (
      (newRep.af_fresh_n.toString().substring(0, 1) === "-") ||
      (newRep.af_bback_n.toString().substring(0, 1) === "-") ||
      (newRep.af_primera_cita_n.toString().substring(0, 1) === "-") ||
      (newRep.af_digitales_n.toString().substring(0, 1) === "-") ||
      (newRep.af_fresh_s.toString().substring(0, 1) === "-") ||
      (newRep.af_bback_s.toString().substring(0, 1) === "-") ||
      (newRep.af_primera_cita_s.toString().substring(0, 1) === "-") ||
      (newRep.af_digitales_s.toString().substring(0, 1) === "-")

    ) {
      toast.info("No se permiten números negativos, intentar de nuevo.");
      setValuesWrittenForm(newRep);
      return;
    }

    if (newRep.af_fresh_n === "") newRep.af_fresh_n = 0;
    if (newRep.af_bback_n === "") newRep.af_bback_n = 0;
    if (newRep.af_primera_cita_n === "") newRep.af_primera_cita_n = 0;
    if (newRep.af_digitales_n === "") newRep.af_digitales_n = 0;
    if (newRep.af_fresh_s === "") newRep.af_fresh_s = 0;
    if (newRep.af_bback_s === "") newRep.af_bback_s = 0;
    if (newRep.af_primera_cita_s === "") newRep.af_primera_cita_s = 0;
    if (newRep.af_digitales_s === "") newRep.af_digitales_s = 0;

    //ANTES DE HACER EL POST, REVISAR SI EL REPORTE DEL DÍA YA EXISTE
    //ruta para buscar por fecha
    let url2 = Apiurl + "api/afluenciabyfecha/";

    //agregar el usuario dependiendo la agencia ...
    let newRepClone = {
      ...newRep,
      user: objusuario.user,
      empresa: objusuario.empresa,
      sucursal: objusuario.sucursal,
      tipo_usuario: objusuario.tipo_usuario
    }

    if (isEditMode) {
      actualizarValoresAfluencia(newRepClone);
      return;
    }

    axios.post(url2, newRepClone)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          if (response['data'].message === 'registrocreado') {
            //actualizar - patch
            actualizarValoresAfluencia(newRepClone);

          } else if (response['data'].message === 'registronocreado') {
            //crear - primeros valores del reporte del día serán afluencia
            registrarValoresAfluencia(newRepClone);
          }

        }

      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })

  }

  const actualizarValoresAfluencia = async (newRep) => {
    //ruta para actualizar
    let url3 = Apiurl + "api/afluenciaupdate/";
    //peticion patch
    axios.patch(url3, newRep)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          closeModalReporte();
          peticionGetReporte();
          toast.success('Reporte diario - Afluencia se ha registrado correctamente')
        }
      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })

  }

  const registrarValoresAfluencia = async (newRep) => {
    //si los otros modulos no han registrado su reporte diario, se ejecutará 
    //esta función para crear el reporte.

    //ruta para crear
    let url4 = Apiurl + "api/afluenciacreate/";
    axios.post(url4, newRep)
      .then(response => {
        if (`${response['statusText']}` === 'OK') {
          closeModalReporte();
          peticionGetReporte();
          toast.success('Reporte diario - Afluencia se ha registrado correctamente')
        }
      })
      .catch(error => {
        toast.error("Error al conectarse con el servidor");
      })

  }

  const createAfluencia = () => {
    setIsEditMode(false);
    setAfluencia({
      id_fecha: FechaDeHoy(),
      af_fresh_n: "",
      af_bback_n: "",
      af_primera_cita_n: "",
      af_digitales_n: "",

      af_fresh_s: "",
      af_bback_s: "",
      af_primera_cita_s: "",
      af_digitales_s: ""
    })

    openModalReporte();
  }

  const editAfluencia = (AFObject) => {
    setIsEditMode(true);
    setAfluencia({
      id_fecha: AFObject.id_fecha.substring(0, 10),
      af_fresh_n: AFObject.af_fresh_n,
      af_bback_n: AFObject.af_bback_n,
      af_primera_cita_n: AFObject.af_primera_cita_n,
      af_digitales_n: AFObject.af_digitales_n,

      af_fresh_s: AFObject.af_fresh_s,
      af_bback_s: AFObject.af_bback_s,
      af_primera_cita_s: AFObject.af_primera_cita_s,
      af_digitales_s: AFObject.af_digitales_s,
      id_reporte: AFObject.id_reporte
    })
    openModalReporte();
  }

  const setValuesWrittenForm = (AFObject) => {
    isEditMode
      ?
      setAfluencia({
        id_fecha: afluencia.id_fecha,
        af_fresh_n: afluencia.af_fresh_n,
        af_bback_n: afluencia.af_bback_n,
        af_primera_cita_n: afluencia.af_primera_cita_n,
        af_digitales_n: afluencia.af_digitales_n,

        af_fresh_s: afluencia.af_fresh_s,
        af_bback_s: afluencia.af_bback_s,
        af_primera_cita_s: afluencia.af_primera_cita_s,
        af_digitales_s: afluencia.af_digitales_s,
        id_reporte: afluencia.id_reporte
      })
      :
      setAfluencia({
        id_fecha: AFObject.id_fecha,
        af_fresh_n: AFObject.af_fresh_n,
        af_bback_n: AFObject.af_bback_n,
        af_primera_cita_n: AFObject.af_primera_cita_n,
        af_digitales_n: AFObject.af_digitales_n,

        af_fresh_s: AFObject.af_fresh_s,
        af_bback_s: AFObject.af_bback_s,
        af_primera_cita_s: AFObject.af_primera_cita_s,
        af_digitales_s: AFObject.af_digitales_s
      })
  }

  return (
    <div className='content-wrapper'>

      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-12">

              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className="m-0 text-dark">{agenciaAfluencia}</h5>

                </div>
              </div>
            </div>
            <h6 className="mb-3 mt-4">{hostessNuevos ? 'NUEVOS' : hostessSeminuevos ? 'SEMINUEVOS' : 'NUEVOS | SEMINUEVOS'}</h6>

          </div>
          <button type='button' onClick={createAfluencia} className="btn btn-outline-info">Registrar Afluencia</button>
        </div>
      </div>


      <div className="container-fluid">
        <div className="row" >

          <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
            <ModalAfluencia
              data={afluencia}
              onSubmit={nuevoReporte}
              editMode={isEditMode}
            />
          </Modal>
          {
            //si es usuario hostess nuevos
            hostessNuevos ?
              <div className='col-md-12 col-xs-5 col-sm-12 table-responsive mb-4'>
                <table id="afluencia" className="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th className='text-center'>Fecha</th>
                      <th className='text-center'>Fresh-N</th>
                      <th className='text-center'>Bback-N</th>
                      <th className='text-center'>1 cita-N</th>
                      <th className='text-center'>Digital-N</th>
                      <th className='text-center'>Editar</th>
                    </tr>
                  </thead>

                  <tbody id='table_body_afluencia'>
                    {data.length > 0 ? data.map((reporte) => {
                      return (
                        <tr>
                          <td className='text-center'>{reverseDate(reporte.id_fecha.substring(0, 10))}</td>
                          <td className='text-center'>{reporte.af_fresh_n}</td>
                          <td className='text-center'>{reporte.af_bback_n}</td>
                          <td className='text-center'>{reporte.af_primera_cita_n}</td>
                          <td className='text-center'>{reporte.af_digitales_n}</td>
                          <td className='text-center'>{
                            <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editAfluencia(reporte)}><FontAwesomeIcon icon={faEdit} /></button>
                          }</td>
                        </tr>
                      )
                    }) : 'No hay registros'}

                  </tbody>
                </table>
              </div>
              :
              //si es usuario hostess seminuevos
              hostessSeminuevos ?
                <div className='col-md-12 col-xs-5 col-sm-12 table-responsive mb-4'>
                  <table id="afluencia" className="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th className='text-center'>Fecha</th>
                        <th className='text-center'>Fresh-S</th>
                        <th className='text-center'>Bback-S</th>
                        <th className='text-center'>1 cita-S</th>
                        <th className='text-center'>Digital-S</th>
                        <th className='text-center'>Editar</th>

                      </tr>
                    </thead>

                    <tbody id='table_body_afluencia'>
                      {data.length > 0 ? data.map((reporte) => {
                        return (
                          <tr>
                            <td className='text-center'>{reverseDate(reporte.id_fecha.substring(0, 10))}</td>
                            <td className='text-center'>{reporte.af_fresh_s}</td>
                            <td className='text-center'>{reporte.af_bback_s}</td>
                            <td className='text-center'>{reporte.af_primera_cita_s}</td>
                            <td className='text-center'>{reporte.af_digitales_s}</td>
                            <td className='text-center'>{
                              <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editAfluencia(reporte)}><FontAwesomeIcon icon={faEdit} /></button>
                            }</td>
                          </tr>
                        )
                      }) : 'No hay registros'}

                    </tbody>
                  </table>
                </div>

                :

                //HASTA ESTE PUNTO EL USUARIO SERÁ ADMINISTRADOR, Y SE MOSTRARÁ TODOS LOS CAMPOS DE AFLUENCIA (NUEVOS Y SEMINUEVOS)

                <div className='col-md-12 col-xs-5 col-sm-12 table-responsive mb-4'>
                  <table id="afluencia" className="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th className='text-center'>Fecha</th>
                        <th className='text-center'>Fresh-N</th>
                        <th className='text-center'>Bback-N</th>
                        <th className='text-center'>1 cita-N</th>
                        <th className='text-center'>Digital-N</th>
                        <th className='text-center'>Fresh-S</th>
                        <th className='text-center'>Bback-S</th>
                        <th className='text-center'>1 cita-S</th>
                        <th className='text-center'>Digital-S</th>
                        <th className='text-center'>Editar</th>

                      </tr>
                    </thead>

                    <tbody id='table_body_afluencia'>
                      {data.length > 0 ? data.map((reporte) => {
                        return (
                          <tr>
                            <td className='text-center'>{reverseDate(reporte.id_fecha.substring(0, 10))}</td>
                            <td className='text-center'>{reporte.af_fresh_n}</td>
                            <td className='text-center'>{reporte.af_bback_n}</td>
                            <td className='text-center'>{reporte.af_primera_cita_n}</td>
                            <td className='text-center'>{reporte.af_digitales_n}</td>
                            <td className='text-center'>{reporte.af_fresh_s}</td>
                            <td className='text-center'>{reporte.af_bback_s}</td>
                            <td className='text-center'>{reporte.af_primera_cita_s}</td>
                            <td className='text-center'>{reporte.af_digitales_s}</td>
                            <td className='text-center'>{
                              <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editAfluencia(reporte)}><FontAwesomeIcon icon={faEdit} /></button>
                            }</td>
                          </tr>
                        )
                      }) : 'No hay registros'}

                    </tbody>
                  </table>
                </div>

          }

        </div>
      </div>




      <ToastContainer />
    </div>
  )
}

export default Afluencia;
