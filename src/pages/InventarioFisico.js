import React, { useEffect, useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import axios from "axios";
import ModalContainerTuto from '../componentes/ModalContainerTuto';
import { useModalCarrousel } from '../hooks/useModalCarrousel';
import { ModalTutoInvFisico } from '../modales/ModalTutoInvFisico';
import { Apiurl } from '../services/Apirest';
import { reverseDate, reverseNormalDate } from '../componentes/OrdenarFecha';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import $ from 'jquery';
import { confDataTableInv } from '../componentes/ConfDataTable';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import 'react-circular-progressbar/dist/styles.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "datatables.net-dt/css/jquery.dataTables.min.css"
import "../css/InventarioFisico.css"
import 'jspdf-autotable';
import { PrintTable } from '../componentes/PrintTable';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faInfoCircle } from '@fortawesome/free-solid-svg-icons';

const InventarioFisico = () => {
    const [invmatch, setInvmatch] = useState([])
    const [fechasAgencia, setFechasAgencia] = useState([])
    const [cambioEmpresa, setCambioEmpresa] = useState(false)
    const [cambioFechaSelect, setCambioFechaSelect] = useState(false)
    const [invSistema, setInvSistema] = useState([])
    const [contador, setContador] = useState(0)
    const [invGlobalDMSUpdated, setInvGlobalDMSUpdated] = useState(false)
    const [invSistemaUpdated, setInvSistemaUpdated] = useState(false)
    const [isOpenModalTuto, openModalTuto, closeModalTuto] = useModalCarrousel(false)
    const [indicadores, setIndicadores] = useState({
        Fecha:"",
        TotalDeInventario: "",
        EnSistema: "",
        EnFisico: "",
        Sistema_Fisico: "",
        Nuevos: "",
        Seminuevos: ""
    })
    const [objInventario, setObjInventario] = useState({
        Fecha: "",
        Empresa: window.sessionStorage.getItem('empresa'),
        Sucursal: window.sessionStorage.getItem('sucursal'),
        Responsable: window.sessionStorage.getItem('responsable')
    })
    const mensajeEspera = "Favor de esperar, sincronizando información...";
    const Empresa = window.sessionStorage.getItem('empresa');
    const Sucursal = window.sessionStorage.getItem('sucursal');
    const Responsable = window.sessionStorage.getItem('responsable')

    useEffect(() => {
        peticionGetFechas();
        // actualizarInventarioSistema()
        // actualizarInventarioSistema();
        // abrirventana()
    }, [])

    const openHelp = () => {
        openModalTuto()
    }

    const peticionGetInventarioSistema = async () => {
        if ( invSistemaUpdated ) {
            peticionGetInventario();
            return;
        }
        
        //PETICION A API NODE JS, ENDPOINT DE SQL SERVER LOCAL
        let url = Apiurl + "api/vehiculosdms";
        await axios.post(url, objInventario)
            .then(response => {
                setInvSistema(response['data']);
                setInvSistemaUpdated(true)
                // peticionGetInventario();
                
                // setTimeout(peticionGetInventario, 2000)
            })
            .catch(err => {
                toast.error("Ocurrió un error al obtener el inventario del sistema.");
            })
    }

    const peticionGetInventario = () => {

        if ( cambioFechaSelect ) setCambioFechaSelect(false)
        if (objInventario.Fecha === "") {
            if (contador !== 0 ) toast.info('No existen capturas de inventarios físicos.')
            return
        }
        // if("2022-05-07" === FechaDeHoy().trim()){
        // if (objInventario.Fecha === "2022-07-01") {
        if (objInventario.Fecha === FechaDeHoy().trim()) {
                peticionGetInventarioFisico();

        } else {
            peticionGetInventarioHistorial();
        }

    }

    const peticionGetInventarioFisico = async () => {
        let url = Apiurl + "api/inventario";
        //objInventario contains SUCURSAL, EMPRESA, FECHA
        await axios.post(url, objInventario)
            .then(response => {
                dataTableDestroy();
                metodofilter(response['data'])
                dataTable();

            })
            .catch(err => {
                toast.error("Ocurrió un error al obtener los registros inventario.");
            })

    }

    const peticionGetInventarioHistorial = async () => {
        let url = Apiurl + "api/inventarioHistorial";
        await axios.post(url, objInventario)
            .then(response => {
                obtenerVariablesIndicadores(response['data'])
                dataTableDestroy();
                setInvmatch(response['data']);
                dataTable();
            })
            .catch(err => {
                console.log(err)
               
            })
    }

    const actualizarInventarioSistema = async () => {
        //PETICIÓN A API DE ASP, ENDPOINT DE ORACLE
        //se utiliza un proxy declarado en el package.json para acceder al siguiente endpoint
        if ( invGlobalDMSUpdated ){
             return;
        }

        let url = "/globaldms/invvehiculos.asp";
        await axios.get(url)
            .then(response => {
                // peticionGetInventarioSistema()
                // setContador(1);
                setInvGlobalDMSUpdated(true)
                peticionGetFechas();
            })
            .catch(err => {
                console.log(err)
            })

    }

    const peticionGetFechas = async () => {
        let url = Apiurl + "api/inventarioFechas";
        await axios.post(url, objInventario)
            .then(response => {
                setFechasAgencia(response['data'])
            })
            .catch(err => {
                toast.error("Ocurrió un error al obtener las fechas de agencia.");
            })
    }

    const metodofilter = (response) => {
        /* console.log('metodo filter')
        console.log(invSistema); */
        // console.log("inventario fisico", response.length);
        let objetoEncontrado = [];
        let objetoNoEncontrado = [];

        for (const indice in response) {
            let key = false;

            invSistema.find(oracle => {
                if (oracle.vehi_serie === response[indice].VIN) {
                    key = true;

                    objetoEncontrado.push({
                        "Fecha": response[indice].Id_fecha,
                        "Id_vehiculo": oracle.Inventario,
                        "Modelo": oracle.mode_descripcion,
                        "Anio_vehi": oracle.vehi_anio_modelo,
                        "Serie_sistema": oracle.vehi_serie,
                        "Serie_fisico": response[indice].VIN,
                        "Marca": oracle.marc_descrip,
                        "Ubicacion": response[indice].Nombre_ubicacion,
                        "QRCapturado":response[indice].QRCapturado,
                        "UbicacionDMS": oracle.Ubicacion
                    })
                }

                // console.log(key);
            })

            if (key === false) {
                //VIN DE SQLSERVER(INVENTARIO FISICO)
                //NO ENCONTRADO EN ORACLE (INVENTARIO SISTEMA)
                objetoNoEncontrado.push({
                    //VIN, UBICACION, FECHA. sql - server 
                    "Fecha": response[indice].Id_fecha,
                    "Id_vehiculo": "N.E.",
                    "Modelo": "N.E.",
                    "Anio_vehi": "N.E.",
                    "Serie_sistema": "N.E.",
                    "Serie_fisico": response[indice].VIN,
                    "Marca": "N.E.",
                    "Ubicacion": response[indice].Nombre_ubicacion,
                    "QRCapturado":response[indice].QRCapturado,
                    "UbicacionDMS":"N.E."
                })
            }

        } //termina for in


        for (const indice in invSistema) {
            let key = false;
            response.find(sqlserver => {
                if (sqlserver.VIN === invSistema[indice].vehi_serie) {
                    key = true;

                }

            })

            if (key === false) {
                objetoNoEncontrado.push({
                    //SERIE DE ORACLE(INVENTARIO SISTEMA)
                    //NO ENCONTRADO EN SQLSERVER(INVENTARIO FISICO) 
                    "Fecha": "N.E.",
                    "Id_vehiculo": invSistema[indice].Inventario,
                    "Modelo": invSistema[indice].mode_descripcion,
                    "Anio_vehi": invSistema[indice].vehi_anio_modelo,
                    "Serie_sistema": invSistema[indice].vehi_serie,
                    "Serie_fisico": "N.E.",
                    "Marca": invSistema[indice].marc_descrip,
                    "Ubicacion": "N.E.",
                    "QRCapturado":"N", //se colocará esta propiedad como no capturado.
                    "UbicacionDMS": invSistema[indice].Ubicacion
                })
            }

        } //termina for


        setInvmatch([
            ...objetoEncontrado,
            ...objetoNoEncontrado
        ]
        );
        const resultadoComparacion = [...objetoEncontrado, ...objetoNoEncontrado]
        
        obtenerVariablesIndicadores(resultadoComparacion);
        existeInventarioEnHistorial(resultadoComparacion);
    }

    const existeInventarioEnHistorial = async (resultadoComparacion) => {
        let url = Apiurl + "api/inventarioExisteEnHistorial";

        axios.post(url, objInventario)
            .then(response => {
                if (response['data'].mensaje === "existe") {
                    eliminamosInventarioEnHistorial(resultadoComparacion);

                }

                if (response['data'].mensaje === "noexiste") {
                    creamosInventarioEnHistorial(resultadoComparacion);

                }
            })
            .catch(err => {
                console.log(err);
            })

    }

    const creamosInventarioEnHistorial = async (resultadoComparacion) => {
        let url = Apiurl + "api/inventarioCrearEnHistorial"
        let data = {
            "agencia": objInventario,
            "data": resultadoComparacion
        }

        axios.post(url, data)
            .then(response => {
                toast.info(response['data'].mensaje)
            })
            .catch(err => {
                toast.info("Ocurrió un error al intentar registrar resultados en historial.")
            })
    }

    const eliminamosInventarioEnHistorial = async (resultadoComparacion) => {
        let url = Apiurl + "api/inventarioEliminarEnHistorial"
        axios.delete(url, { data: objInventario })
            .then(response => {
                if (response['data'].mensaje === "eliminado") {
                    creamosInventarioEnHistorial(resultadoComparacion);
                }
            })
            .catch(response => {
                toast.info("Ocurrió un problema al actualizar el inventario.")

            })

    }

    const obtenerVariablesIndicadores = (resultadoComparacion) => {

        let fecha = reverseDate(objInventario.Fecha);
        let totalFisicos = resultadoComparacion.map(item => item.Serie_fisico !== "N.E.").reduce((prev, curr) => prev + curr, 0);
        let totalSistema = resultadoComparacion.map(item => item.Serie_sistema !== "N.E.").reduce((prev, curr) => prev + curr, 0);
        let sistema_fisico_match = resultadoComparacion.map(item => item.Serie_sistema === item.Serie_fisico).reduce((prev, curr) => prev + curr, 0);
        let sistema_nuevos = resultadoComparacion.map(item => item.Id_vehiculo.substring(0,2) === "NU").reduce((prev, curr) => prev + curr, 0);
        let sistema_seminuevos = resultadoComparacion.map(item => item.Id_vehiculo.substring(0,2) === "US").reduce((prev, curr) => prev + curr, 0);
        let total_inventario = (totalFisicos - sistema_fisico_match) + totalSistema; 

        /* console.log("totalFisicos", totalFisicos);
        console.log("totalSistema", totalSistema);
        console.log("sistema_fisico_match", sistema_fisico_match);
        console.log("sistema_nuevos", sistema_nuevos);
        console.log("sistema_seminuevos", sistema_seminuevos); */

        setIndicadores({
            ...indicadores,
            Fecha: fecha,
            TotalDeInventario: total_inventario,
            EnSistema: totalSistema,
            EnFisico: totalFisicos,
            Sistema_Fisico: sistema_fisico_match,
            Nuevos: sistema_nuevos,
            Seminuevos: sistema_seminuevos
        })

    }

    useEffect(() => {
        if ( fechasAgencia.length > 0 ) actualizarInventarioSistema()    
        
        if (cambioEmpresa) {  
            peticionGetFechas(); 
        }

        if ( cambioFechaSelect ) {
            peticionGetInventario()
        }

    }, [objInventario])

    useEffect(() => {
        if ( fechasAgencia.length < 1 ){
            setObjInventario({...objInventario, ['Fecha']: ""})
            if ( cambioEmpresa ) setCambioEmpresa( false )
            if (contador !== 0 ) {
                dataTableDestroy();
                setInvmatch([])
                dataTable();
                toast.info('No existen capturas de inventarios físicos.')
            }
            return;
        }
        
        setObjInventario({...objInventario, ['Fecha']: fechasAgencia[0].Id_fecha.substring(0, 10)})
        
        if ( cambioEmpresa ) {
           if ( objInventario.Fecha !==  fechasAgencia[0].Id_fecha.substring(0, 10)){
            return;
           }
           peticionGetInventarioSistema() 
           setCambioEmpresa(false);
           return;
        }
       
        peticionGetInventarioSistema()

    }, [fechasAgencia])

    useEffect(() => {
      if (invSistema.length > 0){
         setContador(1);
         peticionGetInventario();
        }

    }, [invSistema])

    const cambiarEmpresa = (e) => {

        setCambioEmpresa(true);
        if (e.target.value == '1') {
            setObjInventario({
                ...objInventario,
                ['Empresa']: '1',
                ['Sucursal']: '1'
            })
        }
        if (e.target.value == '2') {
            // setInvSistema(cadillac)

            setObjInventario({
                ...objInventario,
                ['Empresa']: '7',
                ['Sucursal']: '1'
            })

        }
        if (e.target.value == '3') {
            // setInvSistema(guasave)

            setObjInventario({
                ...objInventario,
                ['Empresa']: '3',
                ['Sucursal']: '1'
            })
        }
        if (e.target.value == '4') {
            // setInvSistema(culiacan)

            setObjInventario({
                ...objInventario,
                ['Empresa']: '5',
                ['Sucursal']: '1'
            })
        }
    }

    const cambiarFecha = (e) => {
        setCambioFechaSelect( true )
        setObjInventario({
            ...objInventario,
            ['Fecha']: reverseNormalDate(e.target.value)
        })

    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#invfisico').DataTable(confDataTableInv);
           
        });
        
    }

    const dataTableDestroy = () => {
        $('#invfisico').DataTable().destroy();

    }

    return (
        <div className='content-wrapper'>

            <div className="content-header">
                <div className="container-fluid">
                    <ModalContainerTuto 
                    isOpen={isOpenModalTuto} 
                    closeModal={closeModalTuto}
                    >
                    <ModalTutoInvFisico/>
                    </ModalContainerTuto>

                    <div className="row mb-2">
                        <div className="col-sm-12">
                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">INVENTARIO FÍSICO</h5>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className='row'>
                        <div className="col">

                            <div className="card card-info direct-chat direct-chat-info">
                                <div className="card-header">
                                    <i className="nav-icon fas fa-car mr-2" /><span className="info-box-text">Inventario de Vehículos al: {indicadores.Fecha !== "" ? indicadores.Fecha : "dd/mm/aaaa"}</span>
                                </div>
                                <table className="table">
                                    <tbody>
                                        <tr style={{ fontSize: 12 }} className="border-separator-top border-separator-right-left"><th>Total de Inventario</th><td style={{ textAlign: 'left' }}>{indicadores.TotalDeInventario}</td></tr>
                                        <tr style={{ fontSize: 12 }} className="border-separator-right-left"><th>En Sistema</th><td style={{ textAlign: 'left' }}>{indicadores.EnSistema}</td></tr>
                                        <tr style={{ fontSize: 12 }} className="border-separator-right-left"><th>En Físico</th><td style={{ textAlign: 'left' }}>{indicadores.EnFisico}</td></tr>
                                        <tr style={{ fontSize: 12 }} className="border-separator-bottom border-separator-right-left"><th>Sistema / Físico</th><td style={{ textAlign: 'left' }}>{indicadores.Sistema_Fisico}</td></tr>
                                        <tr style={{ fontSize: 12 }} className="border-separator-right-left"><th>Nuevos</th><td style={{ textAlign: 'left' }}>{indicadores.Nuevos}</td></tr>
                                        <tr style={{ fontSize: 12 }} className="border-separator-bottom border-separator-right-left"><th>Seminuevos</th><td style={{ textAlign: 'left' }}>{indicadores.Seminuevos}</td></tr>
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div className="col">

                            <div className="form-group mb-3">
                                <label className="input-group-text font-weight-normal">Empresa | Sucursal</label>
                                {/* <select name='empresa' className='form-select' onChange={cambiarEmpresa}> */}
                                <select name='empresa' className='form-select' onChange={cambiarEmpresa} disabled={contador === 0 || Responsable !== '7'}>
                                    <option value={1} selected={Empresa === '1' && Sucursal === '1'}>Mochis</option>
                                    <option value={2} selected={Empresa === '7' && Sucursal === '1'}>Cadillac</option>
                                    <option value={3} selected={Empresa === '3' && Sucursal === '1'}>Guasave</option>
                                    <option value={4} selected={Empresa === '5' && Sucursal === '1'}>Culiacán</option>

                                </select>

                            </div>

                            <div className="form-group mb-3">
                                <label className="input-group-text font-weight-normal">Fecha</label>
                                {/* <select name='empresa' className='form-select' onChange={cambiarFecha}> */}
                                <select name='empresa' className='form-select' onChange={cambiarFecha} disabled={contador === 0 ? true : false}>
                                    
                                    {
                                        fechasAgencia.map((fecha, index) => (
                                            <option key={index} value={reverseDate(fecha.Id_fecha.substring(0, 10))}>{reverseDate(fecha.Id_fecha.substring(0, 10))}</option>
                                        ))
                                    }

                                </select>

                            </div>
                        </div>

                        <div className="col">
                            <button 
                                className="btn btn-outline-info" 
                                disabled={contador === 0}
                                onClick={peticionGetInventario} 
                                type='button' 
                            >
                                <FontAwesomeIcon icon={faSearch}></FontAwesomeIcon>
                                <small className='ml-2'>Buscar</small>
                            </button>
                            
                            {contador === 0 ?
                                <div className='mt-2 ml-2'>
                                    <Box sx={{ display: 'flex' }}>
                                        <CircularProgress />
                                    </Box>
                                    <p>
                                        <small>{mensajeEspera}</small>
                                    </p>
                                </div>
                                :

                                ""
                            }

                        </div>
                        <div className="col text-right">
                            <button
                                type='button'
                                onClick={openHelp} 
                                className="btn btn-outline-primary"
                            ><FontAwesomeIcon size={20} icon={faInfoCircle}/>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <div className="container-fluid">
                <PrintTable data={invmatch} empresa={objInventario.Empresa} sucursal={objInventario.Sucursal} fecha={objInventario.Fecha} indicadores={indicadores} />
                <div className="row">
                    {/* <div className='col-sm-4'>
                        <button type='button' className="btn btn-info ml-2 mb-4" disabled={invmatch.length === 0} onClick={crearTableToPdf}>Generar PDF</button>

                    </div> */}

                    <div className='col-md-12 col-xs-5 col-sm-12 table-responsive  mb-4'>

                        <table id="invfisico" className="table table-bordered table-striped">

                            <thead id="thead">
                                <tr>
                                    <th colspan="7">Inventario Sistema</th>
                                    <th colspan="3">Inventario Físico</th>
                                </tr>
                                <tr>

                                    <th width="10%">Id Vehículo</th>
                                    <th width="8%">Año</th>
                                    <th width="10%">Marca</th>
                                    <th width="31%">Modelo</th>
                                    <th width="12.5%">Serie</th>
                                    <th width="12.5%">Ubicación DMS</th>
                                    <th width="8%">En Sistema</th>
                                    <th width="8%">En Físico</th>
                                    <th width="12.5%">Ubicación</th>
                                    <th width="10%">QR Escrito</th>

                                </tr>
                            </thead>
                            {/* <tfoot>
                                <tr>
                                    <td colspan="8">{`Mostrando ${totalRegistrosFooter} de ${totalRegistrosFooter} registros`}</td>

                                </tr>
                            </tfoot> */}
                            <tbody id="table_body_invfisico">
                                {
                                    invmatch.length > 0 ? invmatch.map((inventario) => {
                                        return (
                                            <tr>

                                                <td>{inventario.Id_vehiculo}</td>
                                                <td>{inventario.Anio_vehi}</td>
                                                <td>{inventario.Marca}</td>
                                                <td>{inventario.Modelo}</td>
                                                <td>{inventario.Serie_sistema !== "N.E." ? inventario.Serie_sistema : inventario.Serie_fisico}</td>
                                                <td>{inventario.UbicacionDMS}</td>
                                                <td className='text-center'>

                                                    <input
                                                        type="checkbox"
                                                        className="form-check-input"
                                                        checked={inventario.Serie_sistema !== "N.E."}
                                                        readOnly
                                                    />

                                                </td>
                                                <td className='text-center'>
                                                    <input
                                                        type="checkbox"
                                                        className="form-check-input"
                                                        checked={inventario.Serie_fisico !== "N.E."}
                                                        readOnly
                                                    />

                                                </td>
                                                <td>{inventario.Ubicacion}</td>
                                                <td>{inventario.QRCapturado}</td>
                                            </tr>
                                        )
                                    }) : "Presionar el botón Buscar para obtener el inventario."
                                    // }) : <tr><td colspan="8" className='text-gray font-weight-bold font-italic'><FontAwesomeIcon icon={ faExclamationCircle } color='blue' className='mr-2'/>Presionar botón Buscar para obtener el inventario</td></tr>
                                }

                            </tbody>

                        </table>
                    </div>


                </div>

                <ToastContainer />
            </div>


        </div>
    )
}

export default InventarioFisico;