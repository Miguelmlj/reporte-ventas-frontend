import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalCtrlOper_SegurosInbros } from '../modales/ModalCtrlOper_SegurosInbros';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';
import { getAsesores } from '../helpers/getAsesores';
import { activos } from '../constantes/asesores';

const CtrlOper_SegurosInbros = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [isEditMode, setIsEditMode] = useState(false)
    const [SegurosInbros, setSegurosInbros] = useState({
        Fecha: "",
        Importe_Inbros: "",
        Asesor: "",
        Utilidad: "",
        Comision_Asesor: "",
        Utilidad_Neta: ""

    })
    const [data, setData] = useState([])
    const [asesores, setAsesores] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    useEffect(async() => {
        getSegurosInbros();
        setAsesores( await getAsesores(activos) )
    }, [])

    const getSegurosInbros = async () => {
        url = Apiurl + "api/ctrloperSegurosInbros/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createSegurosInbros = () => {
        setIsEditMode(false);

        setSegurosInbros({
            Fecha: FechaDeHoy(),
            Importe_Inbros: "",
            Asesor: asesores.length > 0 ? asesores[0].Nombre_Asesor : "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: "",
        })

        openModalReporte();
    }

    const nuevoRegistroSegurosInbros = (SIObject) => {


        if (SIObject.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(SIObject);
            return;
        }

        if (SIObject.Asesor === "") {
            toast.info("El campo Nombre Asesor se encuentra vacío.");
            setValuesWrittenForm(SIObject);
            return;
        }

        if (
            (SIObject.Importe_Inbros.toString().substring(0, 1) === "-") ||
            (SIObject.Comision_Asesor.toString().substring(0, 1) === "-") ||
            (SIObject.Utilidad_Neta.toString().substring(0, 1) === "-") ||
            (SIObject.Utilidad.toString().substring(0, 1) === "-")
        ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(SIObject);
            return;
        }

        if (SIObject.Importe_Inbros === "") SIObject.Importe_Inbros = 0;
        else SIObject.Importe_Inbros = removerComas( SIObject.Importe_Inbros )

        if (SIObject.Comision_Asesor === "") SIObject.Comision_Asesor = 0;
        else SIObject.Comision_Asesor = removerComas( SIObject.Comision_Asesor )

        if (SIObject.Utilidad_Neta === "") SIObject.Utilidad_Neta = 0;
        else SIObject.Utilidad_Neta = removerComas( SIObject.Utilidad_Neta )

        if (SIObject.Utilidad === "") SIObject.Utilidad = 0;
        else SIObject.Utilidad = removerComas( SIObject.Utilidad )

        let body = {
            SegurosInbros: SIObject,
            agencia
        }

        // console.log(body)

        if ( isEditMode ) {
            updateSegurosInbrosSelected(body);
            return;
        }

        createSegurosInbrosWritten(body);

    }

    const setValuesWrittenForm = (SIObject) => {
        isEditMode
            ?
            setSegurosInbros({
                Fecha: SegurosInbros.Fecha,
                Importe_Inbros: SegurosInbros.Importe_Inbros,
                Asesor: SegurosInbros.Asesor,
                Utilidad: SegurosInbros.Utilidad,
                Comision_Asesor: SegurosInbros.Comision_Asesor,
                Utilidad_Neta: SegurosInbros.Utilidad_Neta,
                Id: SegurosInbros.Id
            })
            :
            setSegurosInbros({
                Fecha: SIObject.Fecha,
                Importe_Inbros: SIObject.Importe_Inbros,
                Asesor: SIObject.Asesor,
                Utilidad: SIObject.Utilidad,
                Comision_Asesor: SIObject.Comision_Asesor,
                Utilidad_Neta: SIObject.Utilidad_Neta,
            })

    }

    const updateSegurosInbrosSelected = async (body) => {
        url = Apiurl + "api/ctrloperSegurosInbros/update"
        await axios.patch(url, body)
            .then(response => {
                if (response['data'].isUpdated) {
                    getSegurosInbros()
                    setIsEditMode(false);
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createSegurosInbrosWritten = async (body) => {
        url = Apiurl + "api/ctrloperSegurosInbros/create"
        await axios.post(url, body)
            .then(response => {

                if (response['data'].isCreated) {
                    getSegurosInbros();
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte();
                    return;
                }

                if (!response['data'].isCreated) { //body.GAP
                    setValuesWrittenForm(body.SegurosInbros)
                    toast.error("Ya existe un registro con la fecha seleccionada en la BD.")
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const editSegurosInbros = (SIObject) => {
        setIsEditMode(true);
        setSegurosInbros({
            Fecha: SIObject.Fecha.substring(0, 10),
            Importe_Inbros: formatoMoneda(formatDecimalNumber(SIObject.Importe_Inbros)),
            Asesor: SIObject.Asesor,
            Utilidad: formatoMoneda(formatDecimalNumber(SIObject.Utilidad)),
            Comision_Asesor: formatoMoneda(formatDecimalNumber(SIObject.Comision_Asesor)),
            Utilidad_Neta: formatoMoneda(formatDecimalNumber(SIObject.Utilidad_Neta)),
            Id: SIObject.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#CtrlOper_SegurosInbros').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#CtrlOper_SegurosInbros').DataTable().destroy();
    }


    return (
        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">SEGUROS INBROS</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createSegurosInbros}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalCtrlOper_SegurosInbros
                            data={SegurosInbros}
                            onSubmit={nuevoRegistroSegurosInbros}
                            editMode={isEditMode}
                            asesores={asesores}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="CtrlOper_SegurosInbros" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" className='text-center'>Fecha</th>
                                    <th width="10%" className='text-center'>Importe Seguro Inbros</th>
                                    <th width="20%" className='text-center'>Asesor</th>
                                    <th width="10%" className='text-center'>Utilidad</th>
                                    <th width="15%" className='text-center'>Comisión Asesor</th>
                                    <th width="10%" className='text-center'>Utilidad Neta</th>
                                    <th width="5%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((SI) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(SI.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(SI.Importe_Inbros))}</td>
                                                    <td className='text-center'>{SI.Asesor}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(SI.Utilidad))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(SI.Comision_Asesor))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(SI.Utilidad_Neta))}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editSegurosInbros(SI)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default CtrlOper_SegurosInbros;
