import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalCtrlOper_PrecioFelixContado } from '../modales/ModalCtrlOper_PrecioFelixContado';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';
import { getAsesores } from '../helpers/getAsesores';
import { activos } from '../constantes/asesores';

const CtrlOper_PrecioFelixContado = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [isEditMode, setIsEditMode] = useState(false)
    const [PrecioFelixContado, setPrecioFelixContado] = useState({
        Fecha: "",
        Importe_PrecioFelixContado: "",
        Asesor: "",
        Nombre_Cliente: "",
        Comision_Asesor: "",

    })
    const [data, setData] = useState([])
    const [asesores, setAsesores] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    useEffect(async() => {
        getPrecioFelixContado();
        setAsesores( await getAsesores(activos) )
    }, [])

    const getPrecioFelixContado = async () => {
        url = Apiurl + "api/ctrloperPrecioFelixContado/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createPrecioFelixContado = () => {
        setIsEditMode(false);
        setPrecioFelixContado({
            Fecha: FechaDeHoy(),
            Importe_PrecioFelixContado: "",
            Asesor: asesores.length > 0 ? asesores[0].Nombre_Asesor : "",
            Comision_Asesor: "",
        })

        openModalReporte();
    }

    const nuevoRegistroPrecioFelixContado = (PFCObject) => {

        if (PFCObject.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(PFCObject);
            return;
        }

        if (PFCObject.Asesor === "") {
            toast.info("El campo Nombre Asesor se encuentra vacío.");
            setValuesWrittenForm(PFCObject);
            return;
        }

        if (
            (PFCObject.Importe_PrecioFelixContado.toString().substring(0, 1) === "-") ||
            (PFCObject.Comision_Asesor.toString().substring(0, 1) === "-")
        ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(PFCObject);
            return;
        }

        if (PFCObject.Importe_PrecioFelixContado === "") PFCObject.Importe_PrecioFelixContado = 0;
        else PFCObject.Importe_PrecioFelixContado = removerComas(PFCObject.Importe_PrecioFelixContado)

        if (PFCObject.Comision_Asesor === "") PFCObject.Comision_Asesor = 0;
        else PFCObject.Comision_Asesor = removerComas(PFCObject.Comision_Asesor)

        let body = {
            PrecioFelixContado: PFCObject,
            agencia
        }

        if ( isEditMode ) {
            updatePrecioFelixContadoSelected(body);
            return;
        }

        createPrecioFelixContadoWritten(body);

    }

    const setValuesWrittenForm = (PFCObject) => {
        isEditMode
            ?
            setPrecioFelixContado({
                Fecha: PrecioFelixContado.Fecha,
                Importe_PrecioFelixContado: PrecioFelixContado.Importe_PrecioFelixContado,
                Asesor: PrecioFelixContado.Asesor,
                Comision_Asesor: PrecioFelixContado.Comision_Asesor,
                Id: PrecioFelixContado.Id
            })
            :
            setPrecioFelixContado({
                Fecha: PFCObject.Fecha,
                Importe_PrecioFelixContado: PFCObject.Importe_PrecioFelixContado,
                Asesor: PFCObject.Asesor,
                Comision_Asesor: PFCObject.Comision_Asesor
            })
    }

    const updatePrecioFelixContadoSelected = async (body) => {
        url = Apiurl + "api/ctrloperPrecioFelixContado/update"
        await axios.patch(url, body)
            .then(response => {
                if (response['data'].isUpdated) {
                    getPrecioFelixContado();
                    setIsEditMode(false);
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createPrecioFelixContadoWritten = async (body) => {
        url = Apiurl + "api/ctrloperPrecioFelixContado/create"
        await axios.post(url, body)
            .then(response => {

                if (response['data'].isCreated) {
                    getPrecioFelixContado();
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte();
                    return;
                }

                if (!response['data'].isCreated) { //body.GAP
                    setValuesWrittenForm(body.PrecioFelixContado)
                    toast.error("Ya existe un registro con la fecha seleccionada en la BD.")
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const editPrecioFelixContado = (PFCObject) => {
        setIsEditMode(true);
        setPrecioFelixContado({
            Fecha: PFCObject.Fecha.substring(0, 10),
            Importe_PrecioFelixContado: formatoMoneda(formatDecimalNumber(PFCObject.Importe_PrecioFelixContado)),
            Asesor: PFCObject.Asesor,
            Comision_Asesor: formatoMoneda(formatDecimalNumber(PFCObject.Comision_Asesor)),
            Id: PFCObject.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#CtrlOper_PrecioFelixContado').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#CtrlOper_PrecioFelixContado').DataTable().destroy();
    }

    /* nombre tabla: CtrlOper_PrecioFelixContado */
    return (

        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">PRECIO FÉLIX CONTADO</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createPrecioFelixContado}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalCtrlOper_PrecioFelixContado
                            data={PrecioFelixContado}
                            onSubmit={nuevoRegistroPrecioFelixContado}
                            editMode={isEditMode}
                            asesores={asesores}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="CtrlOper_PrecioFelixContado" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" className='text-center'>Fecha</th>
                                    <th width="10%" className='text-center'>Importe Precio Félix Contado</th>
                                    <th width="20%" className='text-center'>Asesor</th>
                                    <th width="15%" className='text-center'>Comisión Asesor</th>
                                    <th width="5%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((PFC) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(PFC.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(PFC.Importe_PrecioFelixContado))}</td>
                                                    <td className='text-center'>{PFC.Asesor}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(PFC.Comision_Asesor))}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editPrecioFelixContado(PFC)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>

    )
}

export default CtrlOper_PrecioFelixContado;
