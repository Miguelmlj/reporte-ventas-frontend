import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalCtrlOper_GarantiPlus } from '../modales/ModalCtrlOper_GarantiPlus';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';
import { getAsesores } from '../helpers/getAsesores';
import { activos } from '../constantes/asesores';

const CtrlOper_GarantiPlus = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [isEditMode, setIsEditMode] = useState(false)
    const [GarantiPlus, setGarantiPlus] = useState({
        Fecha: "",
        Importe_GarantiPlus: "",
        Costo_GarantiPlus: "",
        Asesor: "",
        Utilidad: "",
        Comision_Asesor: "",
        Utilidad_Neta: ""

    })

    const [data, setData] = useState([])
    const [asesores, setAsesores] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    useEffect(async() => {
        getGarantiPlus();
        setAsesores( await getAsesores(activos) )
    }, [])

    const getGarantiPlus = async () => {
        url = Apiurl + "api/ctrloperGarantiPlus/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createGarantiPlus = () => {
        setIsEditMode(false);
        setGarantiPlus({
            Fecha: FechaDeHoy(),
            Importe_GarantiPlus: "",
            Costo_GarantiPlus: "",
            Asesor: asesores.length > 0 ? asesores[0].Nombre_Asesor : "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })

        openModalReporte();

    }

    const nuevoRegistroGarantiPlus = (GPObject) => {

        if (GPObject.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(GPObject);
            return;
        }

        if (GPObject.Asesor === "") {
            toast.info("El campo Nombre Asesor se encuentra vacío.");
            setValuesWrittenForm(GPObject);
            return;
        }

        if (
            (GPObject.Importe_GarantiPlus.toString().substring(0, 1) === "-") ||
            (GPObject.Costo_GarantiPlus.toString().substring(0, 1) === "-") ||
            (GPObject.Comision_Asesor.toString().substring(0, 1) === "-") ||
            (GPObject.Utilidad_Neta.toString().substring(0, 1) === "-") ||
            (GPObject.Utilidad.toString().substring(0, 1) === "-")
        ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(GPObject);
            return;
        }

        if (GPObject.Importe_GarantiPlus === "") GPObject.Importe_GarantiPlus = 0;
        else GPObject.Importe_GarantiPlus = removerComas( GPObject.Importe_GarantiPlus )

        if (GPObject.Costo_GarantiPlus === "") GPObject.Costo_GarantiPlus = 0;
        else GPObject.Costo_GarantiPlus = removerComas( GPObject.Costo_GarantiPlus )

        if (GPObject.Comision_Asesor === "") GPObject.Comision_Asesor = 0;
        else GPObject.Comision_Asesor = removerComas( GPObject.Comision_Asesor )

        if (GPObject.Utilidad_Neta === "") GPObject.Utilidad_Neta = 0;
        else GPObject.Utilidad_Neta = removerComas( GPObject.Utilidad_Neta )

        if (GPObject.Utilidad === "") GPObject.Utilidad = 0;
        else GPObject.Utilidad = removerComas( GPObject.Utilidad )

        let body = {
            GarantiPlus: GPObject,
            agencia
        }


        if ( isEditMode ) {
            updateGarantiPlusSelected(body);
            return;
        }

        createGarantiPlusWritten(body);

    }

    const setValuesWrittenForm = (GPObject) => {
        isEditMode
            ?
            setGarantiPlus({
                Fecha: GarantiPlus.Fecha,
                Importe_GarantiPlus: GarantiPlus.Importe_GarantiPlus,
                Costo_GarantiPlus: GarantiPlus.Costo_GarantiPlus,
                Asesor: GarantiPlus.Asesor,
                Utilidad: GarantiPlus.Utilidad,
                Comision_Asesor: GarantiPlus.Comision_Asesor,
                Utilidad_Neta: GarantiPlus.Utilidad_Neta,
                Id: GarantiPlus.Id
            })
            :
            setGarantiPlus({
                Fecha: GPObject.Fecha,
                Importe_GarantiPlus: GPObject.Importe_GarantiPlus,
                Costo_GarantiPlus: GPObject.Costo_GarantiPlus,
                Asesor: GPObject.Asesor,
                Utilidad: GPObject.Utilidad,
                Comision_Asesor: GPObject.Comision_Asesor,
                Utilidad_Neta: GPObject.Utilidad_Neta
            })

    }

    const updateGarantiPlusSelected = async (body) => {
        url = Apiurl + "api/ctrloperGarantiPlus/update"
        await axios.patch(url, body)
            .then(response => {
                if (response['data'].isUpdated) {
                    getGarantiPlus();
                    setIsEditMode(false);
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createGarantiPlusWritten = async (body) => {
        url = Apiurl + "api/ctrloperGarantiPlus/create"
        await axios.post(url, body)
            .then(response => {

                if (response['data'].isCreated) {
                    getGarantiPlus();
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte();
                    return;
                }

                if (!response['data'].isCreated) { //body.GarantiPlus
                    setValuesWrittenForm(body.GarantiPlus)
                    toast.error("Ya existe un registro con la fecha seleccionada en la BD.")
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const editGarantiPlus = (GPObject) => {
        setIsEditMode(true);
        setGarantiPlus({
            Fecha                   : GPObject.Fecha.substring(0, 10),
            Importe_GarantiPlus     : formatoMoneda(formatDecimalNumber(GPObject.Importe_GarantiPlus)),
            Costo_GarantiPlus       : formatoMoneda(formatDecimalNumber(GPObject.Costo_GarantiPlus)),
            Asesor                  : GPObject.Asesor,
            Utilidad                : formatoMoneda(formatDecimalNumber(GPObject.Utilidad)),
            Comision_Asesor         : formatoMoneda(formatDecimalNumber(GPObject.Comision_Asesor)),
            Utilidad_Neta           : formatoMoneda(formatDecimalNumber(GPObject.Utilidad_Neta)),
            Id                      : GPObject.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#CtrlOper_GarantiPlus').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#CtrlOper_GarantiPlus').DataTable().destroy();
    }

    return (
        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">GARANTI PLUS</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createGarantiPlus}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalCtrlOper_GarantiPlus
                            data={GarantiPlus}
                            onSubmit={nuevoRegistroGarantiPlus}
                            editMode={isEditMode}
                            asesores={asesores}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="CtrlOper_GarantiPlus" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" className='text-center'>Fecha</th>
                                    <th width="10%" className='text-center'>Importe GarantiPlus</th>
                                    <th width="20%" className='text-center'>Costo GarantiPlus</th>
                                    <th width="20%" className='text-center'>Asesor</th>
                                    <th width="10%" className='text-center'>Utilidad</th>
                                    <th width="15%" className='text-center'>Comisión Asesor</th>
                                    <th width="10%" className='text-center'>Utilidad Neta</th>
                                    <th width="5%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((GP) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(GP.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(GP.Importe_GarantiPlus))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(GP.Costo_GarantiPlus))}</td>
                                                    <td className='text-center'>{GP.Asesor}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(GP.Utilidad))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(GP.Comision_Asesor))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(GP.Utilidad_Neta))}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editGarantiPlus(GP)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default CtrlOper_GarantiPlus;
