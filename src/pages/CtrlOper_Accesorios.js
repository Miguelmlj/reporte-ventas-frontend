import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalCtrlOper_Accesorios } from '../modales/ModalCtrlOper_Accesorios';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';
import { getAsesores } from '../helpers/getAsesores';
import { activos } from '../constantes/asesores';

const CtrlOper_Accesorios = () => {
  const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
  const [isEditMode, setIsEditMode] = useState(false)
  const [Accesorio, setAccesorio] = useState({
    Fecha: "",
    Importe_Accesorio: "",
    Descrip_Accesorio: "",
    Codigo_Accesorio: "",
    Costo_Accesorio: "",
    Asesor: "",
    Nombre_Cliente: "",
    Utilidad: "",
    Comision_Asesor: "",
    Utilidad_Neta: ""

  })
  const [data, setData] = useState([])
  const [asesores, setAsesores] = useState([])
  let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
  let url;

  useEffect(async() => {
    getAccesorio();
    setAsesores(await getAsesores(activos));
  }, [])

  const getAccesorio = async () => {
    url = Apiurl + "api/ctrloperAccesorios/get"
    await axios.post(url, agencia)
      .then(response => {
        dataTableDestroy();
        setData(response['data'])
        dataTable();
      })
      .catch(err => {
        toast.error("Ocurrió un error al conectarse con el servidor")
      })
  }

  const createAccesorio = () => {
    setIsEditMode(false);
    setAccesorio({
      Fecha: FechaDeHoy(),
      Importe_Accesorio: "",
      Descrip_Accesorio: "",
      Codigo_Accesorio: "",
      Costo_Accesorio: "",
      Asesor: asesores.length > 0 ? asesores[0].Nombre_Asesor : "",
      Nombre_Cliente: "",
      Utilidad: "",
      Comision_Asesor: "",
      Utilidad_Neta: "",
    })
    openModalReporte();
  }

  const nuevoRegistroAccesorio = (AccesorioObject) => {

    if (AccesorioObject.Fecha === "") {
      toast.info("Favor de elegir una fecha para hacer el registro.");
      setValuesWrittenForm(AccesorioObject);
      return;
    }

    if (AccesorioObject.Codigo_Accesorio === "") {
      toast.info("El campo Código se encuentra vacío.");
      setValuesWrittenForm(AccesorioObject);
      return;
    }

    if (AccesorioObject.Descrip_Accesorio === "") {
      toast.info("El campo Descripción se encuentra vacío.");
      setValuesWrittenForm(AccesorioObject);
      return;
    }

    if (AccesorioObject.Asesor === "") {
      toast.info("El campo Nombre Asesor se encuentra vacío.");
      setValuesWrittenForm(AccesorioObject);
      return;
    }

    if (AccesorioObject.Nombre_Cliente === "") {
      toast.info("El campo Nombre Cliente se encuentra vacío.");
      setValuesWrittenForm(AccesorioObject);
      return;
    }

    if (
      (AccesorioObject.Importe_Accesorio.toString().substring(0, 1) === "-") ||
      (AccesorioObject.Costo_Accesorio.toString().substring(0, 1) === "-") ||
      (AccesorioObject.Comision_Asesor.toString().substring(0, 1) === "-") ||
      (AccesorioObject.Utilidad_Neta.toString().substring(0, 1) === "-") ||
      (AccesorioObject.Utilidad.toString().substring(0, 1) === "-")
    ) {
      toast.info("No se permiten números negativos, intentar de nuevo.");
      setValuesWrittenForm(AccesorioObject);
      return;
    }


    if (AccesorioObject.Importe_Accesorio === "") AccesorioObject.Importe_Accesorio = 0;
    else AccesorioObject.Importe_Accesorio = removerComas( AccesorioObject.Importe_Accesorio )

    if (AccesorioObject.Costo_Accesorio === "") AccesorioObject.Costo_Accesorio = 0;
    else AccesorioObject.Costo_Accesorio = removerComas( AccesorioObject.Costo_Accesorio )

    if (AccesorioObject.Comision_Asesor === "") AccesorioObject.Comision_Asesor = 0;
    else AccesorioObject.Comision_Asesor = removerComas( AccesorioObject.Comision_Asesor )

    if (AccesorioObject.Utilidad_Neta === "") AccesorioObject.Utilidad_Neta = 0;
    else AccesorioObject.Utilidad_Neta = removerComas( AccesorioObject.Utilidad_Neta )

    if (AccesorioObject.Utilidad === "") AccesorioObject.Utilidad = 0;
    else AccesorioObject.Utilidad = removerComas( AccesorioObject.Utilidad )

    let body = {
      Accesorio: AccesorioObject,
      agencia
    }

    if ( isEditMode ) {
      updateAccesorioSelected(body);
      return;
    }

    createAccesorioWritten(body);

  }

  const setValuesWrittenForm = (AccesorioObject) => {
    isEditMode
      ?
      setAccesorio({
        Fecha: Accesorio.Fecha,
        Importe_Accesorio: Accesorio.Importe_Accesorio,
        Descrip_Accesorio: Accesorio.Descrip_Accesorio,
        Codigo_Accesorio: Accesorio.Codigo_Accesorio,
        Costo_Accesorio: Accesorio.Costo_Accesorio,
        Asesor: Accesorio.Asesor,
        Nombre_Cliente: Accesorio.Nombre_Cliente,
        Utilidad: Accesorio.Utilidad,
        Comision_Asesor: Accesorio.Comision_Asesor,
        Utilidad_Neta: Accesorio.Utilidad_Neta,
        Id: Accesorio.Id
      })
      :
      setAccesorio({
        Fecha: AccesorioObject.Fecha,
        Importe_Accesorio: AccesorioObject.Importe_Accesorio,
        Descrip_Accesorio: AccesorioObject.Descrip_Accesorio,
        Codigo_Accesorio: AccesorioObject.Codigo_Accesorio,
        Costo_Accesorio: AccesorioObject.Costo_Accesorio,
        Asesor: AccesorioObject.Asesor,
        Nombre_Cliente: AccesorioObject.Nombre_Cliente,
        Utilidad: AccesorioObject.Utilidad,
        Comision_Asesor: AccesorioObject.Comision_Asesor,
        Utilidad_Neta: AccesorioObject.Utilidad_Neta
      })
  }

  const updateAccesorioSelected = async (body) => {
    url = Apiurl + "api/ctrloperAccesorios/update"
    await axios.patch(url, body)
      .then(response => {
        if (response['data'].isUpdated) {
          getAccesorio();
          setIsEditMode(false);
          toast.success("El registro se actualizó exitosamente.")
          closeModalReporte();
        }

      })
      .catch(err => {
        toast.error("Ocurrió un error al conectarse con el servidor")
      })
  }

  const createAccesorioWritten = async (body) => {
    url = Apiurl + "api/ctrloperAccesorios/create"
    await axios.post(url, body)
      .then(response => {

        if (response['data'].isCreated) {
          getAccesorio();
          toast.success("Registro completado exitosamente.")
          closeModalReporte();
          return;
        }

        if (!response['data'].isCreated) { //body.GAP
          setValuesWrittenForm(body.Accesorio)
          toast.error("Ya existe un registro con la fecha seleccionada en la BD.")
        }

      })
      .catch(err => {
        toast.error("Ocurrió un error al conectarse con el servidor")
      })
  }

  const editAccesorio = (AccesorioObject) => {
    setIsEditMode(true);
    setAccesorio({
      Fecha: AccesorioObject.Fecha.substring(0, 10),
      Importe_Accesorio: formatoMoneda(formatDecimalNumber(AccesorioObject.Importe_Accesorio)),
      Descrip_Accesorio: AccesorioObject.Descrip_Accesorio,
      Codigo_Accesorio: AccesorioObject.Codigo_Accesorio,
      Costo_Accesorio: formatoMoneda(formatDecimalNumber(AccesorioObject.Costo_Accesorio)),
      Asesor: AccesorioObject.Asesor,
      Nombre_Cliente: AccesorioObject.Nombre_Cliente,
      Utilidad: formatoMoneda(formatDecimalNumber(AccesorioObject.Utilidad)),
      Comision_Asesor: formatoMoneda(formatDecimalNumber(AccesorioObject.Comision_Asesor)),
      Utilidad_Neta: formatoMoneda(formatDecimalNumber(AccesorioObject.Utilidad_Neta)),
      Id: AccesorioObject.Id
    })
    openModalReporte();
  }

  const dataTable = () => {
    $(document).ready(function () {
      $('#CtrlOper_Accesorios').DataTable(
        confDataTableIndicadores
      );
    });
  }

  const dataTableDestroy = () => {
    $('#CtrlOper_Accesorios').DataTable().destroy();
  }

  return (
    <div className='content-wrapper'>
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-4">
            <div className="col-sm-12">

              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className="m-0 text-dark">ACCESORIOS</h5>

                </div>
              </div>

            </div>

          </div>

          <div style={{ marginLeft: 50 }}>

            <button
              type='button'
              onClick={createAccesorio}
              className="btn btn-outline-info mt-4 mb-2"
            >
              Registrar
            </button>
          </div>
        </div>
      </div>


      <div className="container-fluid">
        <div className="row pl-4 ml-4 mr-4 pr-4">
          <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
            <ModalCtrlOper_Accesorios
              data={Accesorio}
              onSubmit={nuevoRegistroAccesorio}
              editMode={isEditMode}
              asesores={asesores}
            />
          </Modal>

          <div className=' table-responsive mb-4'>
            <table id="CtrlOper_Accesorios" className="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="15%" className='text-center'>Fecha</th>
                  <th width="15%" className='text-center'>Código Accesorio</th>
                  <th width="40%" className='text-center'>Descripción Accesorio</th>
                  <th width="10%" className='text-center'>Costo Accesorio</th>
                  <th width="10%" className='text-center'>Importe Accesorio</th>
                  <th width="15%" className='text-center'>Asesor</th>
                  <th width="40%" className='text-center'>Nombre Cliente</th>
                  <th width="10%" className='text-center'>Utilidad</th>
                  <th width="10%" className='text-center'>Comisión Asesor</th>
                  <th width="10%" className='text-center'>Utilidad Neta</th>
                  <th width="10%" className='text-center'>Editar</th>

                </tr>

              </thead>

              <tbody >
                {
                  data.length > 0 ?
                    data.map((Accesorio) => {
                      return (
                        <tr>
                          <td className='text-center'>{reverseDate(Accesorio.Fecha.substring(0, 10))}</td>
                          <td className='text-center'>{Accesorio.Codigo_Accesorio}</td>
                          <td className='text-center'>{Accesorio.Descrip_Accesorio}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(Accesorio.Costo_Accesorio))}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(Accesorio.Importe_Accesorio))}</td>
                          <td className='text-center'>{Accesorio.Asesor}</td>
                          <td className='text-center'>{Accesorio.Nombre_Cliente}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(Accesorio.Utilidad))}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(Accesorio.Comision_Asesor))}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(Accesorio.Utilidad_Neta))}</td>

                          <td className='text-center'>{
                            <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editAccesorio(Accesorio)}><FontAwesomeIcon icon={faEdit} /></button>
                          }</td>
                        </tr>
                      )
                    })
                    :
                    " "
                }
              </tbody>

            </table>
          </div>

        </div>
      </div>


      <ToastContainer />
    </div>
  )
}

export default CtrlOper_Accesorios;