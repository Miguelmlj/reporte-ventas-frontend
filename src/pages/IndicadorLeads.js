import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalIndicadorLeads } from '../modales/ModalIndicadorLeads';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import axios from 'axios'
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from "../componentes/FechaFormatoHoy"
import { Apiurl } from '../services/Apirest';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import $ from 'jquery';

const IndicadorLeads = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [indicadorLeads, setIndicadorLeads] = useState({
        Fecha: "",
        Oport_registradasPeriodo: "",
        Oport_contactadas: "",
        Oport_contactadasATiempo: "",
        Porcen_contactadosATiempo: ""
    })
    const [isEditMode, setIsEditMode] = useState(false)
    const [data, setData] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    const getIndicadoresLeads = async () => {
        url = Apiurl + "api/indicadorLeads/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    useEffect(() => {
        getIndicadoresLeads();
    }, [])

    const dataTable = () => {
        $(document).ready(function () {
            $('#indicador_leads').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#indicador_leads').DataTable().destroy();

    }

    const editIndicador = (indicador) => {
        setIsEditMode(true)
        setIndicadorLeads({
            Fecha: indicador.Fecha.substring(0, 10),
            Oport_registradasPeriodo: indicador.Oport_registradasPeriodo,
            Oport_contactadas: indicador.Oport_contactadas,
            Oport_contactadasATiempo: indicador.Oport_contactadasATiempo,
            Porcen_contactadosATiempo: formatDecimalNumber(indicador.Porcen_contactadosATiempo),
            Id: indicador.Id

        })
        openModalReporte()
    }

    const createIndicador = () => {
        setIsEditMode(false)
        setIndicadorLeads({
            Fecha: FechaDeHoy(),
            Oport_registradasPeriodo: "",
            Oport_contactadas: "",
            Oport_contactadasATiempo: "",
            Porcen_contactadosATiempo: ""

        })
        openModalReporte()
    }

    const setValuesWrittenForm = (indicadorL) => {
        if (isEditMode) {
            setIndicadorLeads({
                Fecha: indicadorLeads.Fecha,
                Oport_registradasPeriodo: indicadorLeads.Oport_registradasPeriodo,
                Oport_contactadas: indicadorLeads.Oport_contactadas,
                Oport_contactadasATiempo: indicadorLeads.Oport_contactadasATiempo,
                Porcen_contactadosATiempo: indicadorLeads.Porcen_contactadosATiempo,
                Id: indicadorLeads.Id
            })
        }

        if (!isEditMode) {
            setIndicadorLeads({
                Fecha: indicadorL.Fecha,
                Oport_registradasPeriodo: indicadorL.Oport_registradasPeriodo,
                Oport_contactadas: indicadorL.Oport_contactadas,
                Oport_contactadasATiempo: indicadorL.Oport_contactadasATiempo,
                Porcen_contactadosATiempo: indicadorL.Porcen_contactadosATiempo,
            })

        }
    }

    const nuevoReporteIndicadorLeads = async (indicadorL) => {

        if (indicadorL.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(indicadorL);
            return;
        }

        if (
            (indicadorL.Oport_registradasPeriodo.toString().substring(0, 1) === "-") ||
            (indicadorL.Oport_contactadas.toString().substring(0, 1) === "-") ||
            (indicadorL.Oport_contactadasATiempo.toString().substring(0, 1) === "-") ||
            (indicadorL.Porcen_contactadosATiempo.toString().substring(0, 1) === "-")

        ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(indicadorL);
            return;
        }

        if (indicadorL.Oport_registradasPeriodo === "") indicadorL.Oport_registradasPeriodo = 0;
        if (indicadorL.Oport_contactadas === "") indicadorL.Oport_contactadas = 0;
        if (indicadorL.Oport_contactadasATiempo === "") indicadorL.Oport_contactadasATiempo = 0;
        if (indicadorL.Porcen_contactadosATiempo === "") indicadorL.Porcen_contactadosATiempo = 0;

        url = Apiurl + "api/indicadorLeads/create"

        let body = {
            indicadorLeads: indicadorL,
            agencia
        }

        if (isEditMode) {
            url = Apiurl + "api/indicadorLeads/update"

            await axios.patch(url, body)
                .then(response => {
                    if (response['data'].isUpdated) {
                        getIndicadoresLeads();
                        setIsEditMode(false);
                        toast.success("El registro se actualizó exitosamente.")
                        closeModalReporte()

                    }
                })
                .catch(err => {
                    toast.error("Ocurrió un error al conectarse con el servidor")
                })

            return;
        }

        await axios.post(url, body)
            .then(response => {

                if (response['data'].isCreated) {
                    getIndicadoresLeads()
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte()
                }
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")

            })

    }

    return (
        <div className='content-wrapper'>

            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">INDICADORES LEADS | PORCENTAJE CONTACTACIÓN</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <button
                        type='button'
                        onClick={createIndicador}
                        className="btn btn-outline-info mt-4 mb-2">
                        Registrar
                    </button>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalIndicadorLeads
                            data={indicadorLeads}
                            onSubmit={nuevoReporteIndicadorLeads}
                            editMode={isEditMode}
                        />
                    </Modal>

                    <div className='col-md-12 col-xs-5 col-sm-12 table-responsive mb-4'>
                        <table id="indicador_leads" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="8%" className='text-center'>Fecha</th>
                                    <th width="21%" className='text-center'># Oportunidades Registradas</th>
                                    <th width="20%" className='text-center'># Oportunidades Contactadas</th>
                                    <th width="25%" className='text-center'># Oportunidades Contactadas A Tiempo</th>
                                    <th width="16%" className='text-end'>% Contactados A Tiempo</th>

                                    <th width="8%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody id='table_body_indicador_leads'>
                                {
                                    data.length > 0 ?
                                        data.map((indicador) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(indicador.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{indicador.Oport_registradasPeriodo}</td>
                                                    <td className='text-center'>{indicador.Oport_contactadas}</td>
                                                    <td className='text-center'>{indicador.Oport_contactadasATiempo}</td>
                                                    <td className='text-end'>{formatDecimalNumber(indicador.Porcen_contactadosATiempo)}</td>
                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editIndicador(indicador)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default IndicadorLeads;
