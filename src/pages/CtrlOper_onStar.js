import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalCtrlOper_onStar } from '../modales/ModalCtrlOper_onStar';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';
import { getAsesores } from '../helpers/getAsesores';
import { activos } from '../constantes/asesores';

const CtrlOper_onStar = () => {
  const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
  const [isEditMode, setIsEditMode] = useState(false)
  const [OnStar, setOnStar] = useState({
    Fecha: "",
    Importe_OnStar: "",
    Asesor: "",
    Nombre_Cliente: "",
    Utilidad: "",
    Comision_Asesor: "",
    Utilidad_Neta: ""

  })
  const [data, setData] = useState([])
  const [asesores, setAsesores] = useState([])
  let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
  let url;

  useEffect(async () => {
    getOnStar()
    setAsesores(await getAsesores(activos))
  }, [])

  const getOnStar = async () => {
    url = Apiurl + "api/ctrloperOnstart/get"
    await axios.post(url, agencia)
      .then(response => {
        dataTableDestroy();
        setData(response['data'])
        dataTable();
      })
      .catch(err => {
        toast.error("Ocurrió un error al conectarse con el servidor")
      })

  }

  const createOnStar = () => {
    setIsEditMode(false);
    setOnStar({
      Fecha: FechaDeHoy(),
      Importe_OnStar: "",
      Asesor: asesores.length > 0 ? asesores[0].Nombre_Asesor : "",
      Nombre_Cliente: "",
      Utilidad: "",
      Comision_Asesor: "",
      Utilidad_Neta: ""
    })

    openModalReporte();
  }

  const nuevoRegistroOnStar = (onstr) => {

    if (onstr.Fecha === "") {
      toast.info("Favor de elegir una fecha para hacer el registro.");
      setValuesWrittenForm(onstr);
      return;
    }

    if (onstr.Nombre_Cliente === "") {
      toast.info("El campo Nombre Cliente se encuentra vacío.");
      setValuesWrittenForm(onstr);
      return;
    }

    if (onstr.Asesor === "") {
      toast.info("El campo Nombre Asesor se encuentra vacío.");
      setValuesWrittenForm(onstr);
      return;
    }

    if (
      (onstr.Importe_OnStar.toString().substring(0, 1) === "-") ||
      (onstr.Comision_Asesor.toString().substring(0, 1) === "-") ||
      (onstr.Utilidad_Neta.toString().substring(0, 1) === "-") ||
      (onstr.Utilidad.toString().substring(0, 1) === "-")
    ) {
      toast.info("No se permiten números negativos, intentar de nuevo.");
      setValuesWrittenForm(onstr);
      return;
    }

    if (onstr.Importe_OnStar === "") onstr.Importe_OnStar = 0;
    else onstr.Importe_OnStar = removerComas( onstr.Importe_OnStar )

    if (onstr.Comision_Asesor === "") onstr.Comision_Asesor = 0;
    else onstr.Comision_Asesor = removerComas( onstr.Comision_Asesor )

    if (onstr.Utilidad_Neta === "") onstr.Utilidad_Neta = 0;
    else onstr.Utilidad_Neta = removerComas( onstr.Utilidad_Neta )

    if (onstr.Utilidad === "") onstr.Utilidad = 0;
    else onstr.Utilidad = removerComas( onstr.Utilidad )

    let body = {
      OnStar: onstr,
      agencia
    }

    // console.log(body)

    if ( isEditMode ) {
      updateOnStarSelected(body);
      return;
    }

    createOnStarWritten(body);

  }

  const setValuesWrittenForm = (onstr) => {
    isEditMode
      ?
      setOnStar({
        Fecha: OnStar.Fecha,
        Importe_OnStar: OnStar.Importe_OnStar,
        Asesor: OnStar.Asesor,
        Nombre_Cliente: OnStar.Nombre_Cliente,
        Utilidad: OnStar.Utilidad,
        Comision_Asesor: OnStar.Comision_Asesor,
        Utilidad_Neta: OnStar.Utilidad_Neta,
        Id: OnStar.Id
      })
      :
      setOnStar({
        Fecha: onstr.Fecha,
        Importe_OnStar: onstr.Importe_OnStar,
        Asesor: onstr.Asesor,
        Nombre_Cliente: onstr.Nombre_Cliente,
        Utilidad: onstr.Utilidad,
        Comision_Asesor: onstr.Comision_Asesor,
        Utilidad_Neta: onstr.Utilidad_Neta,
      })
  }

  const updateOnStarSelected = async (body) => {
    url = Apiurl + "api/ctrloperOnstart/update"
    await axios.patch(url, body)
      .then(response => {
        if (response['data'].isUpdated) {
          getOnStar();
          setIsEditMode(false);
          toast.success("El registro se actualizó exitosamente.")
          closeModalReporte();
        }

      })
      .catch(err => {
        toast.error("Ocurrió un error al conectarse con el servidor")
      })
  }

  const createOnStarWritten = async (body) => {
    url = Apiurl + "api/ctrloperOnstart/create"
    await axios.post(url, body)
      .then(response => {

        if (response['data'].isCreated) {
          getOnStar();
          toast.success("Registro completado exitosamente.")
          closeModalReporte();
          return;
        }

        if ( !response['data'].isCreated ) { //body.OnStar
          setValuesWrittenForm(body.OnStar)
          toast.error("Ya existe un registro con la fecha seleccionada en la BD.")
        }

      })
      .catch(err => {
        toast.error("Ocurrió un error al conectarse con el servidor")
      })
  }

  const editOnStar = (onstr) => {
    setIsEditMode(true);
    setOnStar({
      Fecha: onstr.Fecha.substring(0, 10),
      Importe_OnStar: formatoMoneda(formatDecimalNumber(onstr.Importe_OnStar)),
      Asesor: onstr.Asesor,
      Nombre_Cliente: onstr.Nombre_Cliente,
      Utilidad: formatoMoneda(formatDecimalNumber(onstr.Utilidad)),
      Comision_Asesor: formatoMoneda(formatDecimalNumber(onstr.Comision_Asesor)),
      Utilidad_Neta: formatoMoneda(formatDecimalNumber(onstr.Utilidad_Neta)),
      Id: onstr.Id
    })
    openModalReporte();
  }

  const dataTable = () => {
    $(document).ready(function () {
      $('#CtrlOper_onStar').DataTable(
        confDataTableIndicadores
      );
    });
  }

  const dataTableDestroy = () => {
    $('#CtrlOper_onStar').DataTable().destroy();
  }

  return (
    <div className='content-wrapper'>
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-4">
            <div className="col-sm-12">

              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className="m-0 text-dark">ON STAR</h5>

                </div>
              </div>

            </div>

          </div>

          <div style={{ marginLeft: 50 }}>

            <button
              type='button'
              onClick={createOnStar}
              className="btn btn-outline-info mt-4 mb-2"
            >
              Registrar
            </button>
          </div>
        </div>
      </div>


      <div className="container-fluid">
        <div className="row pl-4 ml-4 mr-4 pr-4">
          <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
            <ModalCtrlOper_onStar
              data={OnStar}
              onSubmit={nuevoRegistroOnStar}
              editMode={isEditMode}
              asesores={asesores}
            />
          </Modal>

          <div className=' table-responsive mb-4'>
            <table id="CtrlOper_onStar" className="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="10%" className='text-center'>Fecha</th>
                  <th width="10%" className='text-center'>Importe OnStar</th>
                  <th width="20%" className='text-center'>Asesor</th>
                  <th width="20%" className='text-center'>Nombre Cliente</th>
                  <th width="10%" className='text-center'>Utilidad</th>
                  <th width="15%" className='text-center'>Comisión Asesor</th>
                  <th width="10%" className='text-center'>Utilidad Neta</th>
                  <th width="5%" className='text-center'>Editar</th>

                </tr>

              </thead>

              <tbody >
                {
                  data.length > 0 ?
                    data.map((onstar) => {
                      return (
                        <tr>
                          <td className='text-center'>{reverseDate(onstar.Fecha.substring(0, 10))}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(onstar.Importe_OnStar))}</td>
                          <td className='text-center'>{onstar.Asesor}</td>
                          <td className='text-center'>{onstar.Nombre_Cliente}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(onstar.Utilidad))}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(onstar.Comision_Asesor))}</td>
                          <td className='text-center'>{formatoMoneda(formatDecimalNumber(onstar.Utilidad_Neta))}</td>

                          <td className='text-center'>{
                            <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editOnStar(onstar)}><FontAwesomeIcon icon={faEdit} /></button>
                          }</td>
                        </tr>
                      )
                    })
                    :
                    " "
                }
              </tbody>

            </table>
          </div>

        </div>
      </div>


      <ToastContainer />
    </div>
  )
}

export default CtrlOper_onStar;