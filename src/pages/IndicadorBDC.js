import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalIndicadorBDC } from '../modales/ModalIndicadorBDC';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { Apiurl } from '../services/Apirest';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FechaDeHoy } from "../componentes/FechaFormatoHoy"
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import axios from 'axios'
import $ from 'jquery';

const IndicadorBDC = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [indicadorBDC, setIndicadorBDC] = useState({
        Fecha: "",
        Llamadas_entrantesVehNvo: ""
    })
    const [isEditMode, setIsEditMode] = useState(false)
    const [data, setData] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    const createIndicadorBDC = () => {
        setIsEditMode(false);
        setIndicadorBDC({
            Fecha: FechaDeHoy(),
            Llamadas_entrantesVehNvo: ""
        })

        openModalReporte();
    }

    const getIndicadorBDC = async () => {
        url = Apiurl + "api/indicadorBDC/get";
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    useEffect(() => {
        getIndicadorBDC();
    }, [])


    const setValuesWrittenForm = (indicadorB) => {
        isEditMode
            ?
            setIndicadorBDC({
                Fecha: indicadorBDC.Fecha,
                Llamadas_entrantesVehNvo: indicadorBDC.Llamadas_entrantesVehNvo,
                Id: indicadorBDC.Id
            })
            :
            setIndicadorBDC({
                Fecha: indicadorB.Fecha,
                Llamadas_entrantesVehNvo: indicadorB.Llamadas_entrantesVehNvo
            })

    }

    const updateIndicadorSelected = async (body) => {
        url = Apiurl + "api/indicadorBDC/update";
        await axios.patch(url, body)
            .then(response => {
                if (response['data'].isUpdated) {
                    getIndicadorBDC();
                    setIsEditMode(false);
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createIndicadorWritten = async (body) => {
        url = Apiurl + "api/indicadorBDC/create"
        await axios.post(url, body)
            .then(response => {
                if (response['data'].isCreated) {
                    getIndicadorBDC();
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte();
                }
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const nuevoRegistroIndicadorBDC = (indicadorB) => {

        if (indicadorB.Fecha === "") {
            setValuesWrittenForm(indicadorB);
            toast.info("Favor de elegir una fecha para hacer el registro.")
            return;
        }
        
        if (indicadorB.Llamadas_entrantesVehNvo.substring(0,1) === "-") {
            setValuesWrittenForm(indicadorB);
            toast.info("No se permiten números negativos, intentar de nuevo.")
            return;
        }

        if (indicadorB.Llamadas_entrantesVehNvo === "") indicadorB.Llamadas_entrantesVehNvo = 0;

        let body = {
            indicadorBDC: indicadorB,
            agencia
        }

        if (isEditMode) {
            updateIndicadorSelected(body)
            return;
        }

        createIndicadorWritten(body);

    }

    const editIndicadorBDC = (indicadorB) => {
        setIsEditMode(true)
        setIndicadorBDC({
            Fecha: indicadorB.Fecha.substring(0, 10),
            Llamadas_entrantesVehNvo: indicadorB.Llamadas_entrantesVehNvo,
            Id: indicadorB.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#Indicador_BDC').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#Indicador_BDC').DataTable().destroy();
    }

    return (
        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">INDICADORES BDC</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createIndicadorBDC}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalIndicadorBDC
                            data={indicadorBDC}
                            onSubmit={nuevoRegistroIndicadorBDC}
                            editMode={isEditMode}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="Indicador_BDC" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%" className='text-center'>Fecha</th>
                                    <th width="60%" className='text-center'>LLamadas Entrantes Vehículos Nuevos</th>
                                    <th width="20%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((indicador) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(indicador.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{indicador.Llamadas_entrantesVehNvo}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editIndicadorBDC(indicador)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default IndicadorBDC;
