import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalIndicadorMercadotecnia } from '../modales/ModalIndicadorMercadotecnia';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';

const IndicadorMercadotecnia = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [indicadorMkt, setIndicadorMkt] = useState({
        Fecha: "",
        Lead_GrupoFelix: "",
        Lead_PaginaLocal: ""
    })
    const [isEditMode, setisEditMode] = useState(false)
    const [data, setData] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    useEffect(() => {
        getIndicadorMkt();
    }, [])


    const createIndicadortMtk = () => {
        setisEditMode(false);

        setIndicadorMkt({
            Fecha: FechaDeHoy(),
            Lead_GrupoFelix: "",
            Lead_PaginaLocal: ""
        })

        openModalReporte();
    }

    const getIndicadorMkt = async () => {
        url = Apiurl + "api/indicadorMkt/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const setValuesWrittenForm = (indicadorM) => {
        isEditMode
            ?
            setIndicadorMkt({
                Fecha: indicadorMkt.Fecha,
                Lead_GrupoFelix: indicadorMkt.Lead_GrupoFelix,
                Lead_PaginaLocal: indicadorMkt.Lead_PaginaLocal,
                Id: indicadorMkt.Id
            })
            :
            setIndicadorMkt({
                Fecha: indicadorM.Fecha,
                Lead_GrupoFelix: indicadorM.Lead_GrupoFelix,
                Lead_PaginaLocal: indicadorM.Lead_PaginaLocal
            })
    }

    const updateIndicadorSelected = async (body) => {
        url = Apiurl + "api/indicadorMkt/update"
        await axios.patch( url, body )
            .then(response => {
                if( response['data'].isUpdated ) {
                    getIndicadorMkt();
                    setisEditMode( false );
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createIndicadorWritten = async (body) => {
        url = Apiurl + "api/indicadorMkt/create"
        await axios.post(url, body)
         .then(response => {
            if ( response['data'].isCreated ) {
                getIndicadorMkt();
                toast.success("Registro completado exitosamente.")
                closeModalReporte();
            }
         })
         .catch(err => {
            toast.info("Ocurrió un error al conectarse con el servidor")
         })
    }

    const nuevoRegistroIndicadorMkt = (indicadorM) => {

        if (indicadorM.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(indicadorM);
            return;
        }

        if (
            (indicadorM.Lead_GrupoFelix.toString().substring(0, 1) === "-") ||
            (indicadorM.Lead_PaginaLocal.toString().substring(0, 1) === "-")
        ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(indicadorM);
            return;
        }

        if (indicadorM.Lead_GrupoFelix === "") indicadorM.Lead_GrupoFelix = 0;
        if (indicadorM.Lead_PaginaLocal === "") indicadorM.Lead_PaginaLocal = 0;

        let body = {
            indicadorMkt: indicadorM,
            agencia
        }

        if (isEditMode) {
            updateIndicadorSelected(body);
            return;
        }

        createIndicadorWritten(body);

    }

    const editIndicadorMkt = (indicadorM) => {
        setisEditMode(true);
        setIndicadorMkt({
            Fecha: indicadorM.Fecha.substring(0, 10),
            Lead_GrupoFelix: indicadorM.Lead_GrupoFelix,
            Lead_PaginaLocal: indicadorM.Lead_PaginaLocal,
            Id: indicadorM.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#Indicador_Mkt').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#Indicador_Mkt').DataTable().destroy();
    }

    return (
        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">MERCADOTECNIA</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createIndicadortMtk}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalIndicadorMercadotecnia
                            data={indicadorMkt}
                            onSubmit={nuevoRegistroIndicadorMkt}
                            editMode={isEditMode}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="Indicador_Mkt" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%" className='text-center'>Fecha</th>
                                    <th width="30%" className='text-center'>Lead Grupo Félix</th>
                                    <th width="30%" className='text-center'>Lead Página Local</th>
                                    <th width="20%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((indicador) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(indicador.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{indicador.Lead_GrupoFelix}</td>
                                                    <td className='text-center'>{indicador.Lead_PaginaLocal}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editIndicadorMkt(indicador)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default IndicadorMercadotecnia;
