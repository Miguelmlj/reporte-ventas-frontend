import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Modal from '../componentes/Modal';
import { useModal } from '../hooks/useModal';
import { ModalGarantiasGMPlus } from '../modales/ModalGarantiasGMPlus';
import { getEmpresa, getSucursal } from '../helpers/getEmpresaSucursal';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FechaDeHoy } from '../componentes/FechaFormatoHoy';
import { confDataTableIndicadores } from '../componentes/ConfDataTable';
import { formatDecimalNumber } from '../helpers/formatearDecimales';
import { formatoMoneda, removerComas } from '../helpers/formatoMoneda';
import axios from 'axios'
import $ from 'jquery';
import { Apiurl } from '../services/Apirest';
import { getAsesores } from '../helpers/getAsesores';
import { activos } from '../constantes/asesores';

const GarantiasGMPlus = () => {
    const [isOpenModalReporte, openModalReporte, closeModalReporte] = useModal(false);
    const [isEditMode, setIsEditMode] = useState(false)
    const [garantiasGMPlus, setGarantiasGMPlus] = useState({
        Fecha: "",
        Importe_Garantia: "",
        Asesor: "",
        Nombre_Cliente: "",
        Utilidad: "",
        Comision_Asesor: "",
        Utilidad_Neta: ""

    })
    const [data, setData] = useState([])
    const [asesores, setAsesores] = useState([])
    let agencia = { Empresa: getEmpresa(), Sucursal: getSucursal() }
    let url;

    useEffect(async () => {
        getGarantiasGMPlus()
        setAsesores(await getAsesores(activos))
    }, [])

    const createGarantiasGMPlus = () => {
        setIsEditMode(false);
        setGarantiasGMPlus({
            Fecha: FechaDeHoy(),
            Importe_Garantia: "",
            Asesor: asesores.length > 0 ? asesores[0].Nombre_Asesor : "",
            Nombre_Cliente: "",
            Utilidad: "",
            Comision_Asesor: "",
            Utilidad_Neta: ""
        })

        openModalReporte();
    }

    const getGarantiasGMPlus = async () => {
        url = Apiurl + "api/garantiasGMPlus/get"
        await axios.post(url, agencia)
            .then(response => {
                dataTableDestroy();
                setData(response['data'])
                dataTable();
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const nuevoRegistroGarantiasGMPlus = (GarantiasGM) => {

        if (GarantiasGM.Fecha === "") {
            toast.info("Favor de elegir una fecha para hacer el registro.");
            setValuesWrittenForm(GarantiasGM);
            return;
        }

        if (GarantiasGM.Nombre_Cliente === "") {
            toast.info("El campo Nombre Cliente se encuentra vacío.");
            setValuesWrittenForm(GarantiasGM);
            return;
        }

        if (GarantiasGM.Asesor === "") {
            toast.info("El campo Nombre Asesor se encuentra vacío.");
            setValuesWrittenForm(GarantiasGM);
            return;
        }

        if (
            (GarantiasGM.Importe_Garantia.toString().substring(0, 1) === "-") ||
            (GarantiasGM.Comision_Asesor.toString().substring(0, 1) === "-") ||
            (GarantiasGM.Utilidad_Neta.toString().substring(0, 1) === "-") ||
            (GarantiasGM.Utilidad.toString().substring(0, 1) === "-")
        ) {
            toast.info("No se permiten números negativos, intentar de nuevo.");
            setValuesWrittenForm(GarantiasGM);
            return;
        }

        if (GarantiasGM.Importe_Garantia === "") GarantiasGM.Importe_Garantia = 0;
        else GarantiasGM.Importe_Garantia = removerComas(GarantiasGM.Importe_Garantia)

        if (GarantiasGM.Comision_Asesor === "") GarantiasGM.Comision_Asesor = 0;
        else GarantiasGM.Comision_Asesor = removerComas(GarantiasGM.Comision_Asesor);

        if (GarantiasGM.Utilidad_Neta === "") GarantiasGM.Utilidad_Neta = 0;
        else GarantiasGM.Utilidad_Neta = removerComas(GarantiasGM.Utilidad_Neta);

        if (GarantiasGM.Utilidad === "") GarantiasGM.Utilidad = 0;
        else GarantiasGM.Utilidad = removerComas(GarantiasGM.Utilidad)

        let body = {
            garantiasGMPlus: GarantiasGM,
            agencia
        }

        if (isEditMode) {
            updateGarantiaSelected(body);
            return;
        }

        createGarantiaWritten(body);

    }

    const setValuesWrittenForm = (GarantiasGM) => {
        isEditMode
        ?
        setGarantiasGMPlus({
            Fecha: garantiasGMPlus.Fecha,
            Importe_Garantia: garantiasGMPlus.Importe_Garantia,
            Asesor: garantiasGMPlus.Asesor,
            Nombre_Cliente: garantiasGMPlus.Nombre_Cliente,
            Utilidad: garantiasGMPlus.Utilidad ,
            Comision_Asesor: garantiasGMPlus.Comision_Asesor,
            Utilidad_Neta: garantiasGMPlus.Utilidad_Neta,
            Id: garantiasGMPlus.Id
        })
        :
        setGarantiasGMPlus({
            Fecha: GarantiasGM.Fecha,
            Importe_Garantia: GarantiasGM.Importe_Garantia,
            Asesor: GarantiasGM.Asesor,
            Nombre_Cliente: GarantiasGM.Nombre_Cliente,
            Utilidad: GarantiasGM.Utilidad,
            Comision_Asesor: GarantiasGM.Comision_Asesor,
            Utilidad_Neta: GarantiasGM.Utilidad_Neta,
        })

     }

    const updateGarantiaSelected = async (body) => {
        url = Apiurl + "api/garantiasGMPlus/update"
        await axios.patch(url, body)
            .then(response => {
                if (response['data'].isUpdated) {
                    getGarantiasGMPlus();
                    setIsEditMode(false);
                    toast.success("El registro se actualizó exitosamente.")
                    closeModalReporte();
                }

            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })
    }

    const createGarantiaWritten = async (body) => {
        url = Apiurl + "api/garantiasGMPlus/create"
        await axios.post(url, body)
            .then(response => {
                if (response['data'].isCreated) {
                    getGarantiasGMPlus();
                    toast.success("Registro completado exitosamente.")
                    closeModalReporte();
                }
            })
            .catch(err => {
                toast.error("Ocurrió un error al conectarse con el servidor")
            })


    }

    const editGarantiaGMPlus = (GarantiasGM) => {

        setIsEditMode(true);
        setGarantiasGMPlus({
            Fecha: GarantiasGM.Fecha.substring(0, 10),
            Importe_Garantia: formatoMoneda(formatDecimalNumber(GarantiasGM.Importe_Garantia)),
            Asesor: GarantiasGM.Asesor,
            Nombre_Cliente: GarantiasGM.Nombre_Cliente,
            Utilidad: formatoMoneda(formatDecimalNumber(GarantiasGM.Utilidad)),
            Comision_Asesor: formatoMoneda(formatDecimalNumber(GarantiasGM.Comision_Asesor)),
            Utilidad_Neta: formatoMoneda(formatDecimalNumber(GarantiasGM.Utilidad_Neta)),
            Id: GarantiasGM.Id
        })
        openModalReporte();
    }

    const dataTable = () => {
        $(document).ready(function () {
            $('#Garantias_GMPlus').DataTable(
                confDataTableIndicadores
            );
        });
    }

    const dataTableDestroy = () => {
        $('#Garantias_GMPlus').DataTable().destroy();
    }

    return (
        <div className='content-wrapper'>
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-4">
                        <div className="col-sm-12">

                            <div className='card card-outline card-primary'>
                                <div className='card-header'>
                                    <h5 className="m-0 text-dark">GARANTÍAS GM PLUS</h5>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div style={{ marginLeft: 50 }}>

                        <button
                            type='button'
                            onClick={createGarantiasGMPlus}
                            className="btn btn-outline-info mt-4 mb-2"
                        >
                            Registrar
                        </button>
                    </div>
                </div>
            </div>


            <div className="container-fluid">
                <div className="row pl-4 ml-4 mr-4 pr-4">
                    <Modal isOpen={isOpenModalReporte} closeModal={closeModalReporte}>
                        <ModalGarantiasGMPlus
                            data={garantiasGMPlus}
                            onSubmit={nuevoRegistroGarantiasGMPlus}
                            editMode={isEditMode}
                            asesores={asesores}
                        />
                    </Modal>

                    <div className=' table-responsive mb-4'>
                        <table id="Garantias_GMPlus" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" className='text-center'>Fecha</th>
                                    <th width="10%" className='text-center'>Importe Garantía</th>
                                    <th width="20%" className='text-center'>Asesor</th>
                                    <th width="20%" className='text-center'>Nombre Cliente</th>
                                    <th width="10%" className='text-center'>Utilidad</th>
                                    <th width="15%" className='text-center'>Comisión Asesor</th>
                                    <th width="10%" className='text-center'>Utilidad Neta</th>
                                    <th width="5%" className='text-center'>Editar</th>

                                </tr>

                            </thead>

                            <tbody >
                                {
                                    data.length > 0 ?
                                        data.map((garantia) => {
                                            return (
                                                <tr>
                                                    <td className='text-center'>{reverseDate(garantia.Fecha.substring(0, 10))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(garantia.Importe_Garantia))}</td>
                                                    <td className='text-center'>{garantia.Asesor}</td>
                                                    <td className='text-center'>{garantia.Nombre_Cliente}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(garantia.Utilidad))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(garantia.Comision_Asesor))}</td>
                                                    <td className='text-center'>{formatoMoneda(formatDecimalNumber(garantia.Utilidad_Neta))}</td>

                                                    <td className='text-center'>{
                                                        <button type="button" className="btn btn-secondary" title="Editar" onClick={() => editGarantiaGMPlus(garantia)}><FontAwesomeIcon icon={faEdit} /></button>
                                                    }</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        " "
                                }
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export default GarantiasGMPlus;
