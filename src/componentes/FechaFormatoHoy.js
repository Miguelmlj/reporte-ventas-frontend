export const FechaFormatoHoy = () => {
    let Fecha = new Date();
    let Months = [
        "enero",
        "febrero",
        "marzo",
        "abril",
        "mayo",
        "junio",
        "julio",
        "agosto",
        "septiembre",
        "octubre",
        "noviembre",
        "diciembre",
      ];       

  
    let Mes = Fecha.getMonth();
    var Dia = Fecha.getDate();
    var Anio = Fecha.getFullYear();      
    
    return `${Dia.toString().length > 1? Dia: '0' + Dia}` + ` de ${Months[Mes]} de ${Anio}`;

  }
  
  export const FechaDeHoy = () => {
    let Fecha = new Date();
    let Mes = Fecha.getMonth() + 1;
    var Dia = Fecha.getDate();
    var Anio = Fecha.getFullYear();      
    
    return `${Anio}-` + `${Mes.toString().length > 1? Mes: '0' + Mes}-${Dia.toString().length > 1? Dia: '0' + Dia}`;
    
}
  
export const FechaDeHoyFormato = () => {
    let Fecha = new Date();
    let Mes = Fecha.getMonth() + 1;
    let Dia = Fecha.getDate();
    let Anio = Fecha.getFullYear();      
    
    // return `${Anio}-` + `${Mes.toString().length > 1? Mes: '0' + Mes}-${Dia.toString().length > 1? Dia: '0' + Dia}`;
    return `${Dia.toString().length > 1? Dia: '0' + Dia}-` + `${Mes.toString().length > 1? Mes: '0' + Mes}-${Anio}`;
    
}

export const FechaDeHoyConHora = () => {
  let date = new Date();
  let hours =  date.getHours();
  let minutes = date.getMinutes();
  let Mes = date.getMonth() + 1;
  let Dia = date.getDate();
  let Anio = date.getFullYear();  

  // Check whether AM or PM
  let newformat = hours >= 12 ? 'PM' : 'AM';

  // Find current hour in AM-PM Format
  hours = hours % 12;

  // To display "0" as "12"
  hours = hours ? hours : 12; 
  minutes = minutes < 10 ? '0' + minutes : minutes;

  return `${Dia.toString().length > 1? Dia: '0' + Dia}-` + `${Mes.toString().length > 1? Mes: '0' + Mes}-${Anio}. ${hours}:${minutes} ${newformat}`;
}