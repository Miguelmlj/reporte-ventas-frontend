import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { Apiurl } from '../services/Apirest';
import { get_month_name } from '../helpers/fechaHelpers';
import { formatoMoneda } from '../helpers/formatoMoneda';
import "../css/Desempeno_Asesores.css"

const Desempeno_Asesores = ({agencia}) => {
  let url;
  const endpointPVAS = "api/resumen/pvas"
  const [data, setdata] = useState([])
  const cantidad = 'cant'
  const facturado = 'fact'
  const facturacion = 'facturacion'
  const entregas = 'entregas'

useEffect(() => {
  getPVAS()
}, [agencia])

const getPVAS = async() => {
  url = Apiurl + endpointPVAS
  await axios.post(url, agencia)
    .then( response => {
      setdata(response['data']);

    })
    .catch(err => {
      console.log(err);
    })
}

const ValidationZero = (value, field) => {
  if ( field === entregas ) return value === 0 ? '-' : value;
  if ( field === facturacion ) return value === 0 ? '-' : value;
  if ( field === cantidad ) return value === 0 ? '-' : value;
  if ( field === facturado ) return value === 0 ? '-' : `$ ${formatoMoneda(value)}`

}

  return (
    <div className="container-card-asesores">

            <div className="card card-info direct-chat direct-chat-info table-responsive table-bordered">
                <div className="card-header text-center overflow-header">
                    <i className="nav-icon fas fa-file-contract mr-2" /><span className="info-box-text">FACTURACIÓN, ENTREGAS , PRODUCTOS DE VALOR AGREGADO Y ACCESORIOS</span>
                </div>
                <table className="table table-hover">
                    <thead>
                      <tr className='font-size-12 text-center'>
                        <th rowspan="3" colspan="1" className='yellow-bg-month' style={{paddingBottom:40}}>{get_month_name(agencia.Fecha)}</th>
                        <th colSpan="7" rowspan="2" className='yellow-bg-month' style={{paddingBottom:25}}>FACTURACIÓN</th>
                        <th colSpan="2" rowspan="2" className='yellow-bg-month' style={{paddingBottom:25}}>ENTREGAS</th>
                        <th colSpan="18" className='yellow-bg-month'>PRODUCTOS DE VALOR AGREGADO Y ACCESORIOS</th>
                      </tr>
                      <tr className='font-size-12 text-center'>
                          <th colspan="2" className='background-total-pvas-title'>SEG CONTADO - GMF</th>
                          <th colspan="2" className='background-total-pvas-title'>SEG CONTADO - INBROS</th>
                          <th colspan="2" className='background-total-pvas-title'>GE GMF</th>
                          <th colspan="2" className='background-total-pvas-title'>GAP</th>
                          <th colspan="2" className='background-total-pvas-title'>ONSTAR</th>
                          <th colspan="2" className='background-total-pvas-title'>CARPROTECTION</th>
                          <th colspan="2" className='background-total-pvas-title'>TOTAL PVA'S</th>
                          <th colspan="2" className='background-total-pvas-title'>ACCESORIOS</th>
                          <th colspan="2" className='background-total-pvas-title'>PRECIO FELIX</th>
                      </tr>
                        <tr className='font-size-12 text-center'>
                          {/* MES */}
                          {/* FACTURACION */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>GMF</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CONT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>BANCOS</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>SUAUTO</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>SUBT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANC</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>TOT</th>

                          {/* ENTREGAS */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>OTRO</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>TOT</th>
                          {/* SEG CONTADO - GMF */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                          {/* SEG CONTADO - INBROS */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                          {/* GE GMF */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                          {/* GAP */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                          {/* ONSTAR */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                          {/* CARPROTECTION */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                          {/* TOTAL PVA'S */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                          {/* ACCESORIOS */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                          {/* PRECIO FELIX */}
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>CANT</th>
                          <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent'>FACT</th>
                        </tr>
                    </thead>
                    <tbody>
                      {
                      data.length > 0 ?
                        data.map(asesor => {
                          return (
                            <tr className='font-size-12 text-center bg-table-row'>

                              {asesor['nombre_asesor'].map(e => {return <th className='background-total-pvas'>{e.nombreAsesor}</th>})}                              

                              {asesor['facturacion'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.gmf, facturacion)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cont, facturacion)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.bancos, facturacion)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.suauto, facturacion)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.subt, facturacion)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.canc, facturacion)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.total, facturacion)}</th>
                                  </>
                                )
                              })}

                              {
                               asesor['entregas'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent-value'>{ValidationZero(e.otro, entregas)}</th>
                                    <th className='background-total-pvas-subtitle bg-subt-subtitle bg-tot-ent-value'>{ValidationZero(e.tot, entregas)}</th>
                                  </>
                                )
                               })}

                              {asesor['seguro_gmf'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>
                                  </>
                                )
                                })}

                              {asesor['seguro_inbros'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>
                                  </>
                                )
                                })}
                              
                              {asesor['ge_gmf'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>
                                  </>
                              )
                              })}
                              
                              {asesor['gap'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>
                                  </>
                              )
                              })}

                              {asesor['onstar'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>  
                                  </>
                              )
                              })}

                              {asesor['carprotection'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>
                                  </>
                                )
                                })}
                              
                              {asesor['total_pvas'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>
                                  </>
                                )})}

                              {asesor['accesorios'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>
                                  </>
                                )
                                })}

                              {asesor['precio_felix'].map(e => {
                                return (
                                  <>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.cant, cantidad)}</th>
                                    <th className='background-total-pvas bg-subt-total-value bg-tot-ent-value'>{ValidationZero(e.fact, facturado)}</th>
                                  </>
                                )
                                })}
                              
                            </tr>
                          )
                        }) 
                        :
                        <tr>
                          <div class="d-flex align-items-center">
                            <strong>Cargando...</strong>
                            <div className="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                          </div>
                        </tr>
                      }

                    </tbody>
                </table>
            </div>
        </div>
  )
}

export default Desempeno_Asesores