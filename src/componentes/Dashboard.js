
export const DashboardData = [
    {
      title: 'Inicio',
      path: '/capturas/inicio',
      icon: "nav-icon fas fa-chart-pie",
      cName: 'nav-item'
    },
    {
      title: 'Afluencia',
      path: '/capturas/afluencia',
      icon: "nav-icon fas fa-columns",
      cName: 'nav-item'
    },
    {
      title: 'Citas',
      path: '/capturas/citas',
      icon: "nav-icon fas fa-calendar-alt",
      cName: 'nav-item'
    },
    {
      title: 'Solicitudes',
      path: '/capturas/solicitudes',
      icon: "nav-icon fas fa-edit",
      cName: 'nav-item'
    },
    {
      title: 'Indicador F&I',
      path: '/capturas/indicadorfyi',
      icon: "nav-icon fas fa-chart-bar",
      cName: 'nav-item'
    },
    {
      title: 'Leads Contactación',
      path: '/capturas/indicadorleads',
      icon: "nav-icon fas fa-chart-line",
      cName: 'nav-item'
    },
    {
      title: 'Centro de Propietario',
      path: '/capturas/centropropietario',
      icon: "nav-icon fas fa-chart-area",
      cName: 'nav-item'
    },
    {
      title: 'Leads JD Power',
      path: '/capturas/jdpower',
      icon: "nav-icon fas fa-align-left",
      cName: 'nav-item'
    },
    {
      title: 'Indicador BDC',
      path: '/capturas/indicadorbdc',
      icon: "nav-icon far fa-chart-bar",
      cName: 'nav-item'
    },
    {
      title: 'Mercadotecnia',
      path: '/capturas/indicadorMkt',
      icon: "nav-icon fas fa-file-signature",
      cName: 'nav-item'
    },
    {
      title: 'Resumen',
      path: '/capturas/resumen',
      icon: 'nav-icon fas fa-file-alt',
      cName: 'nav-item'
    },
    {
      title: 'Seguro GMF',
      path: '/capturas/seguroGMF',
      icon: "nav-icon fas fa-car-crash",
      cName: 'nav-item'
    },
    {
      title: 'Garantías GM Plus',
      path: '/capturas/garantiasGMPlus',
      icon: "nav-icon fas fa-car-side",
      cName: 'nav-item'
    },
    {
      title: 'OnStar',
      path: '/capturas/ctrlOperOnStar',
      icon: "nav-icon fas fa-star",
      cName: 'nav-item'
    },
    {
      title: 'GAP',
      path: '/capturas/ctrlOperGAP',
      icon: "nav-icon fas fa-money-check-alt",
      cName: 'nav-item'
    },
    {
      title: 'Accesorios',
      path: '/capturas/ctrlOperAccesorios',
      icon: "nav-icon fas fa-truck-loading",
      cName: 'nav-item'
    },
    {
      title: 'Precio Félix',
      path: '/capturas/ctrlOperPrecioFelix',
      icon: "nav-icon fas fa-money-bill-alt",
      cName: 'nav-item'
    },
    {
      title: 'Garanti Plus',
      path: '/capturas/ctrlOperGarantiPlus',
      icon: "nav-icon fas fa-plus-circle",
      cName: 'nav-item'
    },
    {
      title: 'Seguros Inbros',
      path: '/capturas/ctrlOperSegurosInbros',
      icon: "nav-icon 	fas fa-car-alt",
      cName: 'nav-item'
    },
    {
      title: 'Precio Félix Contado',
      path: '/capturas/ctrlOperPrecioFelixContado',
      icon: "nav-icon fas fa-money-bill-wave",
      cName: 'nav-item'
    },
    {
      title: 'Detalles Contratos GMF',
      path: '/capturas/detallesContratosGMF',
      icon: "nav-icon fas fa-file-signature",
      cName: 'nav-item'
    },
    {
      title: 'Anticipos',
      path: '/capturas/anticipos',
      icon: "nav-icon fas fa-file-invoice",
      cName: 'nav-item'
    },
    {
      title: 'Inventario Físico',
      path: '/capturas/invfisico',
      icon: "nav-icon fas fa-car",
      cName: 'nav-item'
    },
    {
      title: 'Códigos QR',
      path: '/capturas/codigosqr',
      icon: "nav-icon fas fa-qrcode",
      cName: 'nav-item'
    },
    {
      title: 'Usuarios',
      path: '/capturas/usuarios',
      icon: "nav-icon fas fa-user",
      cName: 'nav-item'
    },
    
   
  ];

