export const reverseDate = (fecha) => {
    //cortamos la cadena string
    let dia = fecha.slice(0,4)
    let mes = fecha.slice(5,7)
    let anio = fecha.slice(8,10)

    //guardamos en un array las variables fecha
    let fechanew = [anio, mes, dia]
    //unimos el array en orden día, mes, anio
    let fechanewjoin = fechanew.join('-');
    //regresamos valores ordenados
    return fechanewjoin;
}

export const reverseNormalDate = (fecha) => {
    let dia = fecha.slice(0,2);
    let mes = fecha.slice(3,5);
    let anio = fecha.slice(6,10);

    let fechanew = [anio, mes, dia]

    let fechanewjoin = fechanew.join('-');

    return fechanewjoin;
    /* console.log(fechanewjoin)

    console.log("dia", dia);
    console.log("mes", mes);
    console.log("anio", anio); */
}