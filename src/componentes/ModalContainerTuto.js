import React from 'react'
import '../css/ModalContainerTuto.css'

const ModalContainerTuto = ({children, isOpen, closeModal}) => {
  return (
    <article className={`modal1 ${isOpen && "is-open1"}`}>
        <div className='modal-container1'>
            <button className='modal-close1 btn btn-primary' onClick={closeModal}>Cerrar</button>
            {children}
        </div>
    </article>
  )
}

export default ModalContainerTuto