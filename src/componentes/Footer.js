import React from 'react'
import { FechaFormatoHoy } from './FechaFormatoHoy'

export default function Footer() {
  return (
    <footer className="main-footer">
  <strong>Sistema Capturas. </strong>
  Grupo Félix.
  <div className="float-right d-none d-sm-inline-block">
    <em>{ FechaFormatoHoy() }</em>
  </div>
</footer>
  )
}
