export const confDataTable = {
  // "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos"]],
  "lengthMenu": [[15, -1], [15, "Todos"]],
  // "lengthMenu": [[-1], ["Todos"]],
  "bVisible": false,
  retrieve: true,
  destroy: true,
  // "ordering": true,
  "order": false,
  "columnDefs": [{
    targets: "_all",
    orderable: false
  }],
  // "scrollX": true,
  "autoWidth": false,
  "responsive": true,

  "language": {
    "lengthMenu": "Mostrar _MENU_ registros",
    "zeroRecords": "No se encontraron resultados",
    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sSearch": "Buscar:",
    "oPaginate": {
      "sFirst": "Primero",
      "sLast": "Último",
      "sNext": "Siguiente",
      "sPrevious": "Anterior"
    },
    "sProcessing": "Procesando...",
  },

}

export const confDataTableInv = {
  // "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos"]],
  "lengthMenu": [[-1], ["Todos"]],
  "bVisible": false,
  // "searching": false,
  retrieve: true,
  destroy: true,
  // "ordering": true,
  // "order": [[3, "asc"]],
  "order": false,
  "columnDefs": [{
    targets: "_all",
    orderable: false
}],
  // "scrollX": true,
  "autoWidth": false,
  "responsive": true,

  "language": {
    "lengthMenu": "Mostrar _MENU_ registros",
    "zeroRecords": "No se encontraron resultados",
    // "zeroRecords": " ",
    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    // "info": " ",
    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    // "infoEmpty": " ",
    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sSearch": "Buscar:",
    "oPaginate": {
      "sFirst": "Primero",
      "sLast": "Último",
      "sNext": "Siguiente",
      "sPrevious": "Anterior"
    },
    "sProcessing": "Procesando...",
  },

}

export const confDataTableUsuarios = {
  // "lengthMenu": [[15, -1], [15, "Todos"]],
  "lengthMenu": [[15], [15]],
  "bVisible": false,
  retrieve: true,
  destroy: true,
  // "searching": false,
  // "order": [[0, "desc"]],
  "order": false,
  // "scrollX": true,
  "columnDefs": [{
    targets: "_all",
    orderable: false
}],
  "autoWidth": false,
  "responsive": true,

  "language": {
    "lengthMenu": "Mostrar _MENU_ registros",
    "zeroRecords": "No se encontraron resultados",
    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sSearch": "Buscar:",
    "oPaginate": {
      "sFirst": "Primero",
      "sLast": "Último",
      "sNext": "Siguiente",
      "sPrevious": "Anterior"
    },
    "sProcessing": "Procesando...",
  },

}

export const confDataTableIndicadores = {
  "lengthMenu": [[15, -1], [15, "Todos"]],
  // "lengthMenu": [[15], [15]],
  "bVisible": false,
  retrieve: true,
  destroy: true,
  // "searching": false,
  // "order": [[0, "desc"]],
  "order": false,
  // "scrollX": true,
  "columnDefs": [{
    targets: "_all",
    orderable: false
}],
  "autoWidth": false,
  "responsive": true,

  "language": {
    "lengthMenu": "Mostrar _MENU_ registros",
    "zeroRecords": "No se encontraron resultados",
    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sSearch": "Buscar:",
    "oPaginate": {
      "sFirst": "Primero",
      "sLast": "Último",
      "sNext": "Siguiente",
      "sPrevious": "Anterior"
    },
    "sProcessing": "Procesando...",
  },

}
