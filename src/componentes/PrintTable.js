import React, { useRef } from 'react'
import { useReactToPrint } from 'react-to-print';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPrint, faFileExcel } from '@fortawesome/free-solid-svg-icons';
import { reverseDate } from '../componentes/OrdenarFecha';
import { FechaDeHoyConHora } from './FechaFormatoHoy';
import * as XLSX from "xlsx"
import "../css/InventarioFisico.css"

class ImprimirTabla extends React.Component {
    render() {
        const Empresa = this.props.empresa;
        const Sucursal = this.props.sucursal;
        const getAgencia = () => {
            if (Empresa === '1' && Sucursal === '1') {
                return `MOCHIS`
            }
            if (Empresa === '3' && Sucursal === '1') {
                return `GUASAVE`
            }
            if (Empresa === '5' && Sucursal === '1') {
                return `CULIACÁN`
            }
            if (Empresa === '7' && Sucursal === '1') {
                return `CADILLAC`
            }
        }

        return (
            <div className='pt-4 pb-4 pl-4 pr-4'>
                <div className='row'>
                    <div className='d-flex justify-content-between'>
                        <small className='ml-2 mb-2'>{`AGENCIA: ${getAgencia()}. Fecha Inventario: ${reverseDate(this.props.fecha)}`}</small>
                        {/* <small className='ml-2 mb-2'>{`Agencia: ${getAgencia() === undefined && 'Mochis'}. Fecha Inventario: ${reverseDate(this.props.fecha)}`}</small> */}
                        <small className='ml-2 mb-2'>{FechaDeHoyConHora()}</small>
                    </div>
                </div>

                {/* Tarjeta Indicadores  col-sm-4 col-md-4*/}
                <div className="col-4">

                    <div className="card card-info direct-chat direct-chat-info">
                        <div className="card-header">
                            <i className="nav-icon fas fa-car mr-2" /><span className="info-box-text">Inventario de Vehículos</span>
                        </div>
                        <table className="table">

                            <tbody>
                                <tr style={{ fontSize: 12 }} className="border-separator-top border-separator-right-left"><th>Total de Inventario</th><td style={{ textAlign: 'left' }}>{this.props.indicadores.TotalDeInventario}</td></tr>
                                <tr style={{ fontSize: 12 }} className="border-separator-right-left"><th>En Sistema</th><td style={{ textAlign: 'left' }}>{this.props.indicadores.EnSistema}</td></tr>
                                <tr style={{ fontSize: 12 }} className="border-separator-right-left"><th>En Físico</th><td style={{ textAlign: 'left' }}>{this.props.indicadores.EnFisico}</td></tr>
                                <tr style={{ fontSize: 12 }} className="border-separator-bottom border-separator-right-left"><th>Sistema / Físico</th><td style={{ textAlign: 'left' }}>{this.props.indicadores.Sistema_Fisico}</td></tr>
                                <tr style={{ fontSize: 12 }} className="border-separator-right-left"><th>Nuevos</th><td style={{ textAlign: 'left' }}>{this.props.indicadores.Nuevos}</td></tr>
                                <tr style={{ fontSize: 12 }} className="border-separator-bottom border-separator-right-left"><th>Seminuevos</th><td style={{ textAlign: 'left' }}>{this.props.indicadores.Seminuevos}</td></tr>

                            </tbody>
                        </table>
                    </div>

                </div>
                {/* Tarjeta Indicadores */}

                <table className="table table-bordered table-striped">

                    <thead id="thead">
                        <tr style={{ fontSize: "12px" }}>
                            <th colspan="7">Inventario Sistema</th>
                            <th colspan="3">Inventario Físico</th>
                        </tr>
                        <tr style={{ fontSize: "12px" }}>

                            <th>Id Vehículo</th>
                            <th>Año</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Serie</th>
                            <th>Ubicación DMS</th>
                            <th>En Sistema</th>
                            <th>En Físico</th>
                            <th>Ubicación</th>
                            <th>QR Escrito</th>
                        </tr>
                    </thead>

                    <tbody id="tbody">
                        {
                            this.props.data.length > 0 ? this.props.data.map((inventario) => {
                                return (
                                    <tr style={{ fontSize: "12px" }}>

                                        <td>{inventario.Id_vehiculo}</td>
                                        <td>{inventario.Anio_vehi}</td>
                                        <td>{inventario.Marca}</td>
                                        <td>{inventario.Modelo}</td>
                                        <td>{inventario.Serie_sistema !== "N.E." ? inventario.Serie_sistema : inventario.Serie_fisico}</td>
                                        <td>{inventario.UbicacionDMS}</td>
                                        <td className='text-center'>

                                            <input
                                                type="checkbox"
                                                className="form-check-input"
                                                checked={inventario.Serie_sistema !== "N.E."}
                                                readOnly
                                            />

                                        </td>
                                        <td className='text-center'>
                                            <input
                                                type="checkbox"
                                                className="form-check-input"
                                                checked={inventario.Serie_fisico !== "N.E."}
                                                readOnly
                                            />

                                        </td>
                                        <td>{inventario.Ubicacion}</td>
                                        <td>{inventario.QRCapturado}</td>
                                    </tr>
                                )
                            }) : "Presionar botón Buscar para obtener el inventario"

                        }

                    </tbody>

                </table>
            </div>
        )
    }
}


export const PrintTable = (props) => {

    const Empresa = props.empresa;
    const Sucursal = props.sucursal;
    const fechaExp= FechaDeHoyConHora().split('.');
    const getAgencia = () => {
        if (Empresa === '1' && Sucursal === '1') {
            return `MOCHIS`
        }
        if (Empresa === '3' && Sucursal === '1') {
            return `GUASAVE`
        }
        if (Empresa === '5' && Sucursal === '1') {
            return `CULIACÁN`
        }
        if (Empresa === '7' && Sucursal === '1') {
            return `CADILLAC`
        }
    }

    const invResult = props.data.map((row) => {
        return {
            "Id Vehículo": row.Id_vehiculo,
            "Año": row.Anio_vehi,
            "Marca": row.Marca,
            "Modelo": row.Modelo,
            "Serie": row.Serie_sistema !== "N.E." ? row.Serie_sistema : row.Serie_fisico,
            "Ubicación DMS": row.UbicacionDMS,
            "En sistema": row.Serie_sistema !== "N.E." ? "SI" : "NO",
            "En Físico": row.Serie_fisico !== "N.E." ? "SI" : "NO",
            "Ubicación": row.Ubicacion,
            "QR Escrito": row.QRCapturado === "S" ? "SI" : "NO"
        }
    })

    const componentRef = useRef();
    const handlePrint = useReactToPrint({
        content: () => componentRef.current,
    });

    const indicators = [
        { "Indicador": "Agencia", "Valor": `${getAgencia()}`, },
        { "Indicador": "Fecha Exportación", "Valor": `${fechaExp[0]}`, },
        { "Indicador": "Fecha Inventario", "Valor": `${reverseDate(props.fecha)}`, },
        { "Indicador": "Total Inventario", "Valor": `${props.indicadores.TotalDeInventario}`, },
        { "Indicador": "En Sistema", "Valor": `${props.indicadores.EnSistema}`, },
        { "Indicador": "En Físico", "Valor": `${props.indicadores.EnFisico}`, },
        { "Indicador": "Sistema / Físico", "Valor": `${props.indicadores.Sistema_Fisico}`, },
        { "Indicador": "Nuevos", "Valor": `${props.indicadores.Nuevos}`, },
        { "Indicador": "Seminuevos", "Valor": `${props.indicadores.Seminuevos}`, },
    ]

    const exportToExcel = () => {
        //https://github.com/SheetJS/sheetjs/issues/1364
        // let ws = XLSX.utils.json_to_sheet(test)
        let Heading = [["Indicador", "Valor"]] 
        let wb = XLSX.utils.book_new()
        let ws = XLSX.utils.json_to_sheet([])
        XLSX.utils.sheet_add_aoa(ws, Heading);
        XLSX.utils.sheet_add_json(ws, indicators, {skipHeader: true });

        XLSX.utils.sheet_add_json(ws, invResult, {
            header:["Id Vehículo","Año","Marca","Modelo","Serie","Ubicación DMS","En sistema","En Físico","Ubicación","QR Escrito"],
            skipHeader: false, 
            origin: indicators.length + 2, 
        });

        XLSX.utils.book_append_sheet(wb, ws, "Inventario")
        XLSX.writeFile(wb, `${getAgencia()}-${reverseDate(props.fecha)}.xlsx`);       
    }

    return (
        <div>
            <div style={{ display: "none" }}>
                <ImprimirTabla ref={componentRef} data={props.data} empresa={props.empresa} sucursal={props.sucursal} fecha={props.fecha} indicadores={props.indicadores} />
            </div>            

            <button
                onClick={
                    handlePrint
                }
                title='Imprimir'
                type='button'
                className="btn btn-outline-dark ml-2 mb-4"
                disabled={props.data.length === 0}>
                <FontAwesomeIcon icon={faPrint} />
            </button>


            <button
                onClick={exportToExcel}
                title='Exportar a excel'
                type='button'
                className="btn btn-outline-dark ml-2 mb-4"
                disabled={props.data.length === 0}>
                <FontAwesomeIcon icon={faFileExcel} />
            </button>
        </div>
    );
}