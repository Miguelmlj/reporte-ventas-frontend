import React, { useState, useEffect } from 'react'
import {
  BrowserRouter as Router,
  Link,
  useHistory,
  NavLink
} from 'react-router-dom'
import { removerVariablesDeSesion } from '../helpers/removeSessionStorage';
import image_gf from '../assets/images/LogoGF.png'
import '../css/Navbar.css';

const Navbar = () => {
  const history = useHistory();
  const [usuario, setUsuario] = useState("");
  //Estados para manejar las opciones del sidebar - Desplegar opciones acordeon
  const [IsAfluenciaVentasCategory, setIsAfluenciaVentasCategory] = useState(false)
  const [IsAnticiposCategory, setIsAnticiposCategory] = useState(false)
  const [IsInvFisCategory, setIsInvFisCategory] = useState(false)
  const [IsGTICategory, setIsGTICategory] = useState(false)
  const [IsControlOperativoCategory, setIsControlOperativoCategory] = useState(false)
  const [IsIndicadoresCategory, setIsIndicadoresCategory] = useState(false)

  useEffect(() => {
    setUsuario(window.sessionStorage.getItem('nombre'))
  }, [usuario])

  useEffect(() => {
    resetTimer()
    readRol();
  }, [])

  const cerrarSesion = () => { 
    removerVariablesDeSesion()
    history.push("/");
  }

  let t;

  const resetTimer = () => {
    clearTimeout(t)
    t = setTimeout(cerrarSesionPorInactividad, 600000)
  }

  const cerrarSesionPorInactividad = () => {
    cerrarSesion()
  }

  const readRol = () => {

    if (
      window.sessionStorage.getItem('responsable') === "1" ||
      window.sessionStorage.getItem('responsable') === "2" ||
      window.sessionStorage.getItem('responsable') === "3" ||
      window.sessionStorage.getItem('responsable') === "4" ||
      window.sessionStorage.getItem('responsable') === "7"

    ) setIsAfluenciaVentasCategory(!IsAfluenciaVentasCategory)

    if (window.sessionStorage.getItem('responsable') === "5") setIsInvFisCategory(!IsInvFisCategory)
    if (window.sessionStorage.getItem('responsable') === "6") setIsAnticiposCategory(!IsAnticiposCategory)
    if (window.sessionStorage.getItem('responsable') === "9") setIsIndicadoresCategory(!IsIndicadoresCategory)
    if (window.sessionStorage.getItem('responsable') === "10") setIsInvFisCategory(!IsInvFisCategory)

  }

  const AfluenciaVentasCategory = () => {
    setIsAfluenciaVentasCategory(!IsAfluenciaVentasCategory)

    setIsAnticiposCategory(false)
    setIsInvFisCategory(false)
    setIsGTICategory(false)
    setIsControlOperativoCategory(false)
    setIsIndicadoresCategory(false)
  }

  const AnticiposCategory = () => {
    setIsAnticiposCategory(!IsAnticiposCategory)

    setIsInvFisCategory(false)
    setIsGTICategory(false)
    setIsControlOperativoCategory(false)
    setIsIndicadoresCategory(false)
    setIsAfluenciaVentasCategory(false)
  }

  const InvFisCategory = () => {
    setIsInvFisCategory(!IsInvFisCategory)

    setIsGTICategory(false)
    setIsControlOperativoCategory(false)
    setIsIndicadoresCategory(false)
    setIsAfluenciaVentasCategory(false)
    setIsAnticiposCategory(false)
  }
  
  const GTICategory = () => {
    setIsGTICategory(!IsGTICategory)

    setIsControlOperativoCategory(false)
    setIsIndicadoresCategory(false)
    setIsAfluenciaVentasCategory(false)
    setIsAnticiposCategory(false)
    setIsInvFisCategory(false)
  }

  const ControlOperativoCategory = () => {
    setIsControlOperativoCategory(!IsControlOperativoCategory)

    setIsIndicadoresCategory(false)
    setIsAfluenciaVentasCategory(false)
    setIsAnticiposCategory(false)
    setIsInvFisCategory(false)
    setIsGTICategory(false)
  }

  const IndicadoresCategory = () => {
    setIsIndicadoresCategory(!IsIndicadoresCategory)

    setIsAfluenciaVentasCategory(false)
    setIsAnticiposCategory(false)
    setIsInvFisCategory(false)
    setIsGTICategory(false)
    setIsControlOperativoCategory(false)
  }

  window.addEventListener('keydown', resetTimer);
  window.addEventListener('click', resetTimer);
  window.addEventListener('scroll', resetTimer);

  return (
    <>

      {/* BARRA SUPERIOR */}

      <nav className="main-header navbar navbar-expand navbar-white navbar-light sidebar-mini">

        {/* Left navbar links */}
        <ul className="navbar-nav">
          <li className={"nav-item "}>
            <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a>
          </li>
        </ul>

        {/* Right navbar links */}
        <ul className="navbar-nav ml-auto">

          <li className="nav-item">
            <span className=''>{usuario.toUpperCase()}</span>
            <button type="button" class="btn btn-secondary ml-3" onClick={cerrarSesion}>Salir<i class="fas fa-sign-out-alt ml-2"></i></button>
          </li>

        </ul>

      </nav>


      {/* Barra Lateral - Dashboard */}

      <aside className=" main-sidebar sidebar-light-primary nav-legacy elevation-4 arreglos">

        {/* Brand Logo */}
        <div className="brand-link" >
          <div className="text-center">
            <img className='profile-user-img img-fluid img-circle' src={image_gf} alt="User profile picture" />
          </div>
        </div>

        {/* Sidebar */}
        <div className="sidebar ml-1">

          {/* Sidebar Menu */}
          <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

              {/* <h4 className="nav-header" style={{ color: "gray", fontSize: 15 }}> <i className='nav-icon fas fa-tachometer-alt ml-2 mr-2'></i>Dashboard</h4> */}
              {
                /* (

                  DashboardData.map((item, index) => {
                    return (
                      <li key={index} className={item.cName}>
                        
                        <NavLink to={item.path} className="nav-link color-fuente" activeClassName="active color-fuente-selected">
                          <i className={item.icon} />
                          <p style={{fontSize:15}}>{item.title}</p>
                        </NavLink>
                        
                      </li>
                    );
                  })
                ) */

              }

              {/* <li className={IsAfluenciaVentas ? "menu-open nav-item color-fuente" : "nav-item color-fuente"}> */}
              <li className={IsAfluenciaVentasCategory ? "menu-open nav-item color-fuente" : "nav-item color-fuente"}>
                <a style={{ cursor: "pointer" }}
                  className={IsAfluenciaVentasCategory ? "nav-link active" : "nav-link bg__categorias color-fuente"}
                  onClick={AfluenciaVentasCategory}>
                  <i className="nav-icon fas fa-chart-pie" />
                  <p>
                    Afluencia de Ventas
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">

                  <li className='nav-item'>
                    <NavLink to='/capturas/inicio' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-chart-pie" />
                      <p style={{ fontSize: 15 }}>Inicio</p>
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink to='/capturas/afluencia' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-columns" />
                      <p style={{ fontSize: 15 }}>Afluencia</p>
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink to='/capturas/citas' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-calendar-alt" />
                      <p style={{ fontSize: 15 }}>Citas</p>
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink to='/capturas/solicitudes' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-edit" />
                      <p style={{ fontSize: 15 }}>Solicitudes</p>
                    </NavLink>
                  </li>

                </ul>
              </li>


              <li className={IsIndicadoresCategory ? "nav-item menu-open" : "nav-item"}>
                <a style={{ cursor: "pointer" }}
                  className={IsIndicadoresCategory ? "nav-link active" : "nav-link bg__categorias color-fuente"}
                  onClick={IndicadoresCategory}>
                  <i className="nav-icon fas fa-chart-bar" />
                  <p>
                    Indicadores
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">

                  <li className='nav-item'>
                    <NavLink to='/capturas/indicadorfyi' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-chart-bar" />
                      <p style={{ fontSize: 15 }}>Indicador F&I</p>
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink to='/capturas/indicadorleads' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-chart-line" />
                      <p style={{ fontSize: 15 }}>Leads Contactación</p>
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink to='/capturas/centropropietario' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-chart-area" />
                      <p style={{ fontSize: 15 }}>Centro de Propietario</p>
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink to='/capturas/jdpower' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-align-left" />
                      <p style={{ fontSize: 15 }}>JD Power</p>
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink to='/capturas/indicadorbdc' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon far fa-chart-bar" />
                      <p style={{ fontSize: 15 }}>Indicador BDC</p>
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink to='/capturas/indicadorMkt' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-file-signature" />
                      <p style={{ fontSize: 15 }}>Mercadotecnia</p>
                    </NavLink>
                  </li>

                </ul>
              </li>


              <li className={IsControlOperativoCategory ? "nav-item menu-open" : "nav-item"}>
                <a
                  style={{ cursor: "pointer" }}
                  className={IsControlOperativoCategory ? "nav-link active" : "nav-link bg__categorias color-fuente"}
                  onClick={ControlOperativoCategory}>
                  <i className="nav-icon fas fa-tachometer-alt" />
                  <p>
                    F&I Control Operativo
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">

                  <li className='nav-item'>
                    <NavLink to='/capturas/resumen' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-file-alt" />
                      <p style={{ fontSize: 15 }}>Resumen</p>
                    </NavLink>
                  </li>

                  <li className='nav-item'>
                    <NavLink to='/capturas/seguroGMF' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-car-crash" />
                      <p style={{ fontSize: 15 }}>Seguro GMF</p>
                    </NavLink>
                  </li>

                  <li className='nav-item'>
                    <NavLink to='/capturas/garantiasGMPlus' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-car-side" />
                      <p style={{ fontSize: 15 }}>Garantías GM Plus</p>
                    </NavLink>
                  </li>

                  <li className='nav-item'>
                    <NavLink to='/capturas/ctrlOperOnStar' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-star" />
                      <p style={{ fontSize: 15 }}>On Star</p>
                    </NavLink>
                  </li>
                  
                  <li className='nav-item'>
                    <NavLink to='/capturas/ctrlOperGAP' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-money-check-alt" />
                      <p style={{ fontSize: 15 }}>GAP</p>
                    </NavLink>
                  </li>
                  
                  <li className='nav-item'>
                    <NavLink to='/capturas/ctrlOperAccesorios' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-truck-loading" />
                      <p style={{ fontSize: 15 }}>Accesorios</p>
                    </NavLink>
                  </li>
                  
                  <li className='nav-item'>
                    <NavLink to='/capturas/ctrlOperPrecioFelix' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-money-bill-alt" />
                      <p style={{ fontSize: 15 }}>Precio Félix</p>
                    </NavLink>
                  </li>
                  
                  <li className='nav-item'>
                    <NavLink to='/capturas/ctrlOperGarantiPlus' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-plus-circle" />
                      <p style={{ fontSize: 15 }}>Garanti Plus</p>
                    </NavLink>
                  </li>

                  <li className='nav-item'>
                    <NavLink to='/capturas/ctrlOperSegurosInbros' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-car-alt" />
                      <p style={{ fontSize: 15 }}>Seguros Inbros</p>
                    </NavLink>
                  </li>
                  
                  <li className='nav-item'>
                    <NavLink to='/capturas/ctrlOperPrecioFelixContado' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-money-bill-wave" />
                      <p style={{ fontSize: 15 }}>Precio Félix Contado</p>
                    </NavLink>
                  </li>
                  
                  <li className='nav-item'>
                    <NavLink to='/capturas/detallesContratosGMF' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-file-signature" />
                      <p style={{ fontSize: 15 }}>Detalles Contratos GMF</p>
                    </NavLink>
                  </li>

                </ul>
              </li>


              <li className={IsAnticiposCategory ? "nav-item menu-open" : "nav-item"}>
                <a
                  style={{ cursor: "pointer" }}
                  className={IsAnticiposCategory ? "nav-link active" : "nav-link bg__categorias color-fuente"}
                  onClick={AnticiposCategory}>
                  <i className="nav-icon fas fa-file-invoice" />
                  <p>
                    Anticipos
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">

                  <li className='nav-item'>
                    <NavLink to='/capturas/anticipos' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-file-invoice" />
                      <p style={{ fontSize: 15 }}>Anticipos</p>
                    </NavLink>
                  </li>

                </ul>
              </li>


              <li className={IsInvFisCategory ? "nav-item menu-open" : "nav-item"}>
                <a
                  style={{ cursor: "pointer" }}
                  className={IsInvFisCategory ? "nav-link active" : "nav-link bg__categorias color-fuente"}
                  onClick={InvFisCategory}>
                  <i className="nav-icon fas fa-car" />
                  <p>
                    Inventario Físico
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">

                  <li className='nav-item'>
                    <NavLink to='/capturas/invfisico' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-car" />
                      <p style={{ fontSize: 15 }}>Inventario Físico</p>
                    </NavLink>
                  </li>

                  <li className='nav-item'>
                    <NavLink to='/capturas/codigosqr' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-qrcode" />
                      <p style={{ fontSize: 15 }}>Códigos QR</p>
                    </NavLink>
                  </li>

                </ul>
              </li>


              <li className={IsGTICategory ? "nav-item menu-open" : "nav-item"}>
                <a
                  style={{ cursor: "pointer" }}
                  className={IsGTICategory ? "nav-link active " : "nav-link bg__categorias color-fuente"}
                  onClick={GTICategory}>
                  <i className="nav-icon fas fa-user" />
                  <p>
                    Gestión TI
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">

                  <li className='nav-item'>
                    <NavLink to='/capturas/usuarios' className="nav-link color-fuente" activeClassName="bg__categorias_children">
                      <i className="nav-icon fas fa-user" />
                      <p style={{ fontSize: 15 }}>Usuarios</p>
                    </NavLink>
                  </li>


                </ul>
              </li>



            </ul>

          </nav>
          {/* /.sidebar-menu */}

        </div>
        {/* /.sidebar */}
      </aside>

    </>
  )
}

export default Navbar;
