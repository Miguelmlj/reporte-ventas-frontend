
import React, { useRef } from 'react'
import { useReactToPrint } from 'react-to-print';
import QRCode from "react-qr-code";
import '../css/Modulos.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPrint } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { Apiurl } from '../services/Apirest';

class ImprimirCodigosQR extends React.Component {
    render() {


        return (
            <div className="container-fluid">
                <div className='row'>
                {
                        this.props.data.map((valor, index) => {

                            return (
                                <div className='col-3 mt-4 mb-4'>
                                    <QRCode value={valor.vehi_serie} size={190} />
                                    <p className='mb-1 font__size'>{valor.vehi_serie}</p>
                                </div>
                            )

                        })

                    }
                </div>
            </div>
        )
    }
}

export const PrintQRS = (props) => {

    const componentRef = useRef();
    const handlePrint = useReactToPrint({
        content: () => componentRef.current
    })

    const updateVINDB = async() => {
        let url = Apiurl + "api/codigosqr/updateVehiculosSQL"

        if((!props.isVinGenerated) && (props.Responsable === "5")){ //&& rol es contador : actualizamos
            await axios.post(url, props.data);
        }

    }

    return (
        <div>
            {/* <div> */}
            <div style={{ display: "none" }}>
                <ImprimirCodigosQR ref={componentRef} data={props.data}/>
            </div>
            <button onClick={()=> {
                handlePrint()
                updateVINDB()
                }} disabled={props.data.length === 0} title='Imprimir Códigos QR' type='button' className="btn btn-outline-dark ml-2 mb-4"><FontAwesomeIcon icon={faPrint}></FontAwesomeIcon></button>

        </div>
    )
}
