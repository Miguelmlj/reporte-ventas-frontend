import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { Apiurl } from '../services/Apirest';
import { ToastContainer, toast } from 'react-toastify';
import { formatoMoneda } from '../helpers/formatoMoneda';
import "../css/Control_Operativo_FYI.css"

const Control_Operativo_FYI = ({ agencia }) => {
    let url;
    const endpointUtilidades = "api/resumen/utilidades"
    const utilidad = 'utilidad'
    const lease = 'leasing'
    const [data, setdata] = useState({
        contratos_leasing: 0,
        penetracion_gmf: 0,
        garantia_extendida_gmplus: 0,
        garantia_extendida_garantiplus: 0,
        onstar: 0,
        accesorios: 0,
        gap: 0,
        sobreprecios_gmf: 0,
        seguros_gmf: 0,
        seguros_inbros: 0,
        sobreprecios_contado: 0,
        total: 0
    })

    useEffect(async () => {
        getUtilidades();
    }, [agencia])

    const getUtilidades = async () => {
        url = Apiurl + endpointUtilidades;
        await axios.post(url, agencia)
            .then(response => {
                setdata({
                    ...data,
                    contratos_leasing: response['data'].contratos_leasing,
                    penetracion_gmf: response['data'].penetracion_gmf,
                    garantia_extendida_gmplus: response['data'].garantia_extendida_gmplus,
                    garantia_extendida_garantiplus: response['data'].garantia_extendida_garantiplus,
                    onstar: response['data'].onstar,
                    accesorios: response['data'].accesorios,
                    gap: response['data'].gap,
                    sobreprecios_gmf: response['data'].sobreprecios_gmf,
                    seguros_gmf: response['data'].seguros_gmf,
                    seguros_inbros: response['data'].seguros_inbros,
                    sobreprecios_contado: response['data'].sobreprecios_contado,
                    total: response['data'].total 
                })
            })
            .catch(err => {
                toast.error("Error al obtener las utilidades")
            })
    }

    const ValidationZero = (value, field) => {
        if ( field === utilidad) return value === 0 ? '-' : `$ ${formatoMoneda(value)}`
        if ( field === lease) return value === 0 ? '-' : value
    }

    return (
        <div className="container-card">

            <div className="card card-info direct-chat direct-chat-info">
                <div className="card-header">
                    <i className="nav-icon fas fa-file-contract mr-2" /><span className="info-box-text">UTILIDAD VENTAS, PVAS, SEGUROS Y SOBREPRECIOS</span>
                </div>
                <table className="table">
                    <thead>
                        <tr className='font-size-12'>
                            <th></th>
                            {/* <th>HOY</th> */}
                            <th className='text-align-right'></th>
                        </tr>
                    </thead>
                    <tbody>
                        {/* HOY                                                                                                                                             ACUMULADO */}
                        <tr className='font-size-12 gray-background'>
                            <th>TOTAL</th>
                            {/* <th>{"valor2"}</th> */}
                            {/* <th className='text-align-right'>{`$ ${formatoMoneda(data.total)}`}</th> */}
                            <th className='text-align-right'>{ValidationZero(data.total, utilidad)}</th>
                        </tr>


                        <tr className='font-size-12'>
                            <td>UTILIDAD GARANTÍA EXTENDIDA GMPLUS</td>
                            {/* <td>"dato2"</td> */}
                            {/* <td className='text-align-right'>{`$ ${formatoMoneda(data.garantia_extendida_gmplus)}`}</td> */}
                            <td className='text-align-right'>{ValidationZero(data.garantia_extendida_gmplus, utilidad)}</td>
                        </tr>

                        <tr className='font-size-12'>
                            <td>UTILIDAD GARANTÍA EXTENDIDA GARANTIPLUS</td>
                            {/* <td>"dato3"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.garantia_extendida_garantiplus, utilidad)}</td>
                        </tr>
                        <tr className='font-size-12'>
                            <td>UTILIDAD ONSTAR</td>
                            {/* <td>"dato3"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.onstar, utilidad)}</td>
                        </tr>
                        <tr className='font-size-12'>
                            <td>UTILIDAD ACCESORIOS</td>
                            {/* <td>"dato3"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.accesorios, utilidad)}</td>
                        </tr>
                        <tr className='font-size-12'>
                            <td>UTILIDAD GAP</td>
                            {/* <td>"dato3"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.gap, utilidad)}</td>
                        </tr>
                        <tr className='font-size-12'>
                            <td>UTILIDAD SOBREPRECIOS GMF</td>
                            {/* <td>"dato3"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.sobreprecios_gmf, utilidad)}</td>
                        </tr>
                        <tr className='font-size-12'>
                            <td>UTILIDAD SEGUROS GMF</td>
                            {/* <td>"dato3"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.seguros_gmf, utilidad)}</td>
                        </tr>
                        <tr className='font-size-12'>
                            <td>UTILIDAD SEGUROS INBROS</td>
                            {/* <td>"dato3"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.seguros_inbros, utilidad)}</td>
                        </tr>
                        <tr className='font-size-12 yellow-background'>
                            <td>UTILIDAD SOBREPRECIOS CONTADO</td>
                            {/* <td>"dato3"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.sobreprecios_contado, utilidad)}</td>
                        </tr>
                        <tr className='font-size-12 yellow-background'>
                            <td>CONTRATOS LEASING</td>
                            {/* <td>"dato1"</td> */}
                            <td className='text-align-right'>{ValidationZero(data.contratos_leasing, lease)}</td>
                        </tr>

                        <tr className='font-size-12 yellow-background'>
                            <td>PENETRACIÓN GMF</td>
                            {/* <td>"dato1"</td> */}
                            <td className='text-align-right'>{`${data.penetracion_gmf} %`}</td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <ToastContainer />
        </div>
    )
}

export default Control_Operativo_FYI