import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
// import { useEffect, useState } from "react";
import 'datatables.net'
import "datatables.net-dt/css/jquery.dataTables.min.css"

import Navbar from './componentes/Navbar';
// import './App.css'; //custom css
import Login from './pages/Login'
import Inicio from './pages/Inicio'
import Afluencia from './pages/Afluencia'
import Citas from './pages/Citas'
import Solicitudes from './pages/Solicitudes'
import Usuarios from './pages/Usuarios'
import Error404 from './pages/Error404'
import Footer from "./componentes/Footer";
import Anticipos from "./pages/Anticipos";
import CodesQR from "./pages/CodesQR";
import InventarioFisico from "./pages/InventarioFisico";
import IndicadorFYI from "./pages/IndicadorFYI";
import IndicadorLeads from "./pages/IndicadorLeads";
import IndicadorLeadsCentroProp from "./pages/IndicadorLeadsCentroProp";
import IndicadorLeadsJDPower from "./pages/IndicadorLeadsJDPower";
import IndicadorBDC from "./pages/IndicadorBDC";
import IndicadorMercadotecnia from "./pages/IndicadorMercadotecnia";
import GarantiasGMPlus from "./pages/GarantiasGMPlus";
import SeguroGMF from "./pages/SeguroGMF";
import CtrlOper_onStar from "./pages/CtrlOper_onStar";
import CtrlOper_GAP from "./pages/CtrlOper_GAP";
import CtrlOper_Accesorios from "./pages/CtrlOper_Accesorios";
import CtrlOper_PrecioFelix from "./pages/CtrlOper_PrecioFelix";
import CtrlOper_GarantiPlus from "./pages/CtrlOper_GarantiPlus";
import CtrlOper_SegurosInbros from "./pages/CtrlOper_SegurosInbros";
import CtrlOper_PrecioFelixContado from "./pages/CtrlOper_PrecioFelixContado";
import DetallesContratosGMF from "./pages/DetallesContratosGMF";
import Ctrl_Resumen from "./pages/CtrlOper_Resumen";

function App() {

  return (
    <div className="wrapper">

      <Router>

        <Switch>

          <Route exact path="/">
            <Login />
          </Route>

          <Route exact path="/capturas/inicio" render={() => {

            /*   roles o responsables
            1.- ADMINISTRADOR = 1 RESPONSABLE
            2.- HOSTESS = 2 RESPONSABLE
            3.- BDC = 3 RESPONSABLE
            4.- FYI = 4 RESPONSABLE
            */

            //si aún no hay usuario logueado se redirigirá a login
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            // return window.sessionStorage.getItem('responsable') === '1' ? <><Navbar /><Inicio /><Footer /></> : <><Navbar /><Error404 /></>
            return window.sessionStorage.getItem('inicio') === 'S' ? <><Navbar /><Inicio /><Footer /></> : <><Navbar /><Error404 /></>
          }}>

          </Route>

          <Route exact path="/capturas/anticipos" render={() => {


            //si aún no hay usuario logueado se redirigirá a login
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            // return window.sessionStorage.getItem('responsable') === '1' || window.sessionStorage.getItem('responsable') === '5' ?
            return window.sessionStorage.getItem('anticipos') === 'S' ? <><Navbar /><Anticipos /><Footer /></> : <><Navbar /><Error404 /></>
          }}>

          </Route>

          <Route exact path="/capturas/invfisico" render={() => {

            //si aún no hay usuario logueado se redirigirá a login
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            // return window.sessionStorage.getItem('responsable') === '1' ?
            return window.sessionStorage.getItem('invfisico') === 'S' ? <><Navbar /><InventarioFisico /><Footer /></> : <><Navbar /><Error404 /></>
          }}>

          </Route>

          <Route exact path="/capturas/codigosqr" render={() => {

            //si aún no hay usuario logueado se redirigirá a login
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            // return window.sessionStorage.getItem('responsable') === '1' ? <><Navbar /><CodesQR /><Footer /></> : <><Navbar /><Error404 /></>
            return window.sessionStorage.getItem('codigosqr') === 'S' ? <><Navbar /><CodesQR /><Footer /></> : <><Navbar /><Error404 /></>
          }}>

          </Route>

          <Route exact path="/capturas/afluencia" render={() => {

            //si no se ha logueado
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            // return window.sessionStorage.getItem('responsable') === '1' || window.sessionStorage.getItem('responsable') === '2' ?
            return window.sessionStorage.getItem('afluencia') === 'S' ? <><Navbar /><Afluencia /><Footer /></> : <><Navbar /><Error404 /></>
          }}>

          </Route>

          <Route exact path="/capturas/citas" render={() => {

            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            // return window.sessionStorage.getItem('responsable') === '1' || window.sessionStorage.getItem('responsable') === '3' ?
            return window.sessionStorage.getItem('citas') === 'S' ? <><Navbar /><Citas /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/solicitudes" render={() => {
            //si no se ha logueado
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            // return window.sessionStorage.getItem('responsable') === '1' || window.sessionStorage.getItem('responsable') === '4' ?
            return window.sessionStorage.getItem('solicitudes') === 'S' ?

              <><Navbar /><Solicitudes /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/indicadorfyi" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('indFYI') === 'S' ?

              <><Navbar /><IndicadorFYI /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/indicadorleads" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('indLeads') === 'S' ?

              <><Navbar /><IndicadorLeads /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/centropropietario" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('indCentroProp') === 'S' ?

              <><Navbar /><IndicadorLeadsCentroProp /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/jdpower" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('indJDPower') === 'S' ?

              <><Navbar /><IndicadorLeadsJDPower /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>
          <Route exact path="/capturas/indicadorbdc" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('indBDC') === 'S' ?

              <><Navbar /><IndicadorBDC /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>
          <Route exact path="/capturas/indicadorMkt" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('indMkt') === 'S' ?

              <><Navbar /><IndicadorMercadotecnia /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/resumen" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_Resumen') === 'S' ?

              <><Navbar /><Ctrl_Resumen /><Footer /></> : <><Navbar /><Error404 /></>

          }}>
          </Route>

          <Route exact path="/capturas/seguroGMF" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_SegGMF') === 'S' ?

              <><Navbar /><SeguroGMF /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/garantiasGMPlus" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_GarGMPlus') === 'S' ?

              <><Navbar /><GarantiasGMPlus /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/ctrlOperOnStar" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_OnStrt') === 'S' ?

              <><Navbar /><CtrlOper_onStar /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>
          
          <Route exact path="/capturas/ctrlOperGAP" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_GAP') === 'S' ?

              <><Navbar /><CtrlOper_GAP /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>
          
          <Route exact path="/capturas/ctrlOperAccesorios" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_Accsrios') === 'S' ?

              <><Navbar /><CtrlOper_Accesorios /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>
          
          <Route exact path="/capturas/ctrlOperPrecioFelix" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_PrecioFlx') === 'S' ?

              <><Navbar /><CtrlOper_PrecioFelix /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>


          <Route exact path="/capturas/ctrlOperGarantiPlus" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_GarPlus') === 'S' ?

              <><Navbar /><CtrlOper_GarantiPlus /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>
          
          <Route exact path="/capturas/ctrlOperSegurosInbros" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_SegInbros') === 'S' ?

              <><Navbar /><CtrlOper_SegurosInbros /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>
          
          <Route exact path="/capturas/ctrlOperPrecioFelixContado" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('CO_PrecioFlxCntdo') === 'S' ?

              <><Navbar /><CtrlOper_PrecioFelixContado /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>
          
          <Route exact path="/capturas/detallesContratosGMF" render={() => {
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            return window.sessionStorage.getItem('DtllsCntrtsGMF') === 'S' ?

              <><Navbar /><DetallesContratosGMF /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route exact path="/capturas/usuarios" render={() => {
            //si no se ha logueado
            if (window.sessionStorage.getItem('usuario') === null) return <Login />

            // return window.sessionStorage.getItem('responsable') === '1' ?
            return window.sessionStorage.getItem('usuarios') === 'S' ?

              <><Navbar /><Usuarios /><Footer /></> : <><Navbar /><Error404 /></>

          }}>

          </Route>

          <Route path="/error">
            <Navbar />
            <Error404 />
          </Route>

        </Switch>

      </Router>

    </div>
  );
}

export default App;
